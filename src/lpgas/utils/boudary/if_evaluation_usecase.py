from abc import ABC, abstractmethod


class IF_EvaluateUsecase(ABC):
    @abstractmethod
    def evaluate_and_store_evaluation(self) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
