from abc import ABC, abstractmethod

from lpgas.domain.dp_results import DPResults


class IF_DPResultsRepo(ABC):
    @abstractmethod
    def store(self, dp_results: DPResults) -> None:
        pass

    @abstractmethod
    def load_latest(self) -> DPResults:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
