from abc import ABC, abstractmethod


class IF_RawInputUsecase(ABC):
    @abstractmethod
    def gene_and_store_all(self) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_meter(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_lc(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_meter_usage_data(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_ncu_meter_usage_data(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_delivery_result_data(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def gene_and_store_dict_weather_data(self, enable_preprocess: bool = True) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
