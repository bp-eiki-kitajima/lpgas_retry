from abc import ABC, abstractmethod


class IF_PredictUsecase(ABC):
    @abstractmethod
    def predict_and_store_results(self) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
