import pathlib
from abc import ABC, abstractmethod
from typing import Optional

import pandas as pd

from lpgas.domain.identifier import MeterID
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


class IF_DPModel(ABC):
    @abstractmethod
    def train(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        pass

    @abstractmethod
    def predict(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: Optional[list[MeterID]],
        predict_start_dt: pd.Timestamp,
        predict_output_term: int,
    ) -> tuple[pd.DataFrame, list[MeterID]]:
        pass

    @abstractmethod
    def dump_command(self) -> None:
        pass

    @abstractmethod
    def dump_result_for_local(self, result_dir_path: pathlib.Path) -> None:
        pass
