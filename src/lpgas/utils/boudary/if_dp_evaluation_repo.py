from abc import ABC, abstractmethod

from lpgas.domain.dp_results import DPResults


class IF_DPEvaluationRepo(ABC):
    @abstractmethod
    def store(self, dp_results: DPResults) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
