from abc import ABC, abstractmethod
from typing import Optional

from lpgas.domain.delivery_result_data import DeliveryResultData
from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC
from lpgas.domain.meter.meter import Meter
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.weather.weather_data import WeatherData


class IF_RawInputRepo(ABC):
    @abstractmethod
    def gene_dict_meter(self) -> dict[MeterID, Meter]:
        pass

    @abstractmethod
    def gene_dict_lc(self) -> dict[LC_ID, LC]:
        pass

    @abstractmethod
    def gene_dict_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, MeterUsageData]:
        pass

    @abstractmethod
    def gene_dict_ncu_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, NcuMeterUsageData]:
        pass

    @abstractmethod
    def gene_dict_delivery_result_data(
        self, list_lc_id: Optional[list[MeterID]] = None
    ) -> dict[LC_ID, DeliveryResultData]:
        pass

    @abstractmethod
    def gene_dict_weather_data(
        self, list_weather_city_id: Optional[list[WeatherCityID]] = None
    ) -> dict[WeatherCityID, WeatherData]:
        pass
