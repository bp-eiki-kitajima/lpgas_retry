from abc import ABC, abstractmethod


class IF_TrainUsecase(ABC):
    @abstractmethod
    def train_and_store_model(self) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
