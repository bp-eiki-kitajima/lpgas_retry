from abc import ABC, abstractmethod

from lpgas.utils.boudary.if_dp_model import IF_DPModel


class IF_DPModelRepo(ABC):
    @abstractmethod
    def store(self, dp_model: IF_DPModel) -> None:
        pass

    @abstractmethod
    def load_latest(self) -> IF_DPModel:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
