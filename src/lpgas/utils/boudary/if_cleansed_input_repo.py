from abc import ABC, abstractmethod
from typing import Optional

from lpgas.domain.delivery_result_data import DeliveryResultData
from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC
from lpgas.domain.meter.meter import Meter
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.weather.weather_data import WeatherData


class IF_CleansedInputRepo(ABC):
    # get
    @abstractmethod
    def get_dict_meter(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, Meter]:
        pass

    @abstractmethod
    def get_dict_lc(self, list_lc_id: Optional[list[LC_ID]] = None) -> dict[LC_ID, LC]:
        pass

    @abstractmethod
    def get_dict_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, MeterUsageData]:
        pass

    @abstractmethod
    def get_dict_ncu_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, NcuMeterUsageData]:
        pass

    @abstractmethod
    def get_dict_delivery_result_data(
        self, list_meter_id: Optional[list[LC_ID]] = None
    ) -> dict[LC_ID, DeliveryResultData]:
        pass

    @abstractmethod
    def get_dict_weather_data(
        self, list_weather_city_id: Optional[list[WeatherCityID]] = None
    ) -> dict[WeatherCityID, WeatherData]:
        pass

    # store
    @abstractmethod
    def store_dict_meter(self, dict_meter: dict[MeterID, Meter]) -> None:
        pass

    @abstractmethod
    def store_dict_lc(self, dict_lc: dict[LC_ID, LC]) -> None:
        pass

    @abstractmethod
    def store_dict_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData]
    ) -> None:
        pass

    @abstractmethod
    def store_dict_ncu_meter_usage_data(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData]
    ) -> None:
        pass

    @abstractmethod
    def store_dict_delivery_result_data(
        self, dict_delivery_result_data: dict[LC_ID, DeliveryResultData]
    ) -> None:
        pass

    @abstractmethod
    def store_dict_weather_data(self, dict_weather_data: dict[WeatherCityID, WeatherData]) -> None:
        pass

    @abstractmethod
    def init_output(self) -> None:
        pass
