from abc import ABC, abstractmethod

from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


class IF_PreprocessService(ABC):
    @abstractmethod
    def apply_and_store(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        pass
