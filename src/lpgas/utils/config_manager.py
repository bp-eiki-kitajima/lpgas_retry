from typing import NamedTuple, Union, cast

from omegaconf import DictConfig, ListConfig, OmegaConf

from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_evaluation_repo import IF_DPEvaluationRepo
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.boudary.if_dp_results_repo import IF_DPResultsRepo
from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase
from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase
from lpgas.utils.boudary.if_raw_input_repo import IF_RawInputRepo
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase
from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase


class ConfigManagerUsecaseCommand(NamedTuple):
    """Usecaseクラスの引数が記載されたconfigのファイルパス"""

    raw_input_usecase_cfg_path: str
    train_usecase_cfg_path: str
    predict_usecase_cfg_path: str
    evaluate_usecase_cfg_path: str


class ConfigManagerRepoCommand(NamedTuple):
    """Repoクラスの引数が記載されたconfigのファイルパス"""

    raw_input_repo_cfg_path: str
    dp_model_repo_cfg_path: str
    cleansed_input_repo_cfg_path: str
    dp_results_repo_cfg_path: str
    dp_evaluation_repo_cfg_path: str


class ConfigManager:
    """configを解釈し, ユースケースとリポジトリの依存解決と初期化をする"""

    def __init__(
        self,
        usecase_command: ConfigManagerUsecaseCommand,
        repo_command: ConfigManagerRepoCommand,
    ) -> None:
        assert type(usecase_command) is ConfigManagerUsecaseCommand
        assert type(repo_command) is ConfigManagerRepoCommand

        self._usecase_command = usecase_command
        self._repo_command = repo_command

    def _convert_yaml(self, yaml: Union[DictConfig, ListConfig], type_name: str):
        cfg = cast(dict, OmegaConf.to_container(yaml))
        inner_cfg = cfg[type_name]
        return inner_cfg["classname"], inner_cfg["params"]

    # usecase
    def params_for_raw_input_usecase(self) -> tuple[str, dict]:
        yaml = OmegaConf.load(self._usecase_command.raw_input_usecase_cfg_path)
        return self._convert_yaml(yaml, IF_RawInputUsecase.__name__)

    def params_for_train_usecase(self):
        yaml = OmegaConf.load(self._usecase_command.train_usecase_cfg_path)
        return self._convert_yaml(yaml, IF_TrainUsecase.__name__)

    def params_for_predict_usecase(self):
        yaml = OmegaConf.load(self._usecase_command.predict_usecase_cfg_path)
        return self._convert_yaml(yaml, IF_PredictUsecase.__name__)

    def params_for_evaluate_usecase(self):
        yaml = OmegaConf.load(self._usecase_command.evaluate_usecase_cfg_path)
        return self._convert_yaml(yaml, IF_EvaluateUsecase.__name__)

    # repo
    def params_for_raw_input_repo(self):
        yaml = OmegaConf.load(self._repo_command.raw_input_repo_cfg_path)
        return self._convert_yaml(yaml, IF_RawInputRepo.__name__)

    def params_for_dp_model_repo(self):
        yaml = OmegaConf.load(self._repo_command.dp_model_repo_cfg_path)
        return self._convert_yaml(yaml, IF_DPModelRepo.__name__)

    def params_for_cleansed_input_repo(self):
        yaml = OmegaConf.load(self._repo_command.cleansed_input_repo_cfg_path)
        return self._convert_yaml(yaml, IF_CleansedInputRepo.__name__)

    def params_for_dp_results_repo(self):
        yaml = OmegaConf.load(self._repo_command.dp_results_repo_cfg_path)
        return self._convert_yaml(yaml, IF_DPResultsRepo.__name__)

    def params_for_dp_evaluation_repo(self):
        yaml = OmegaConf.load(self._repo_command.dp_evaluation_repo_cfg_path)
        return self._convert_yaml(yaml, IF_DPEvaluationRepo.__name__)
