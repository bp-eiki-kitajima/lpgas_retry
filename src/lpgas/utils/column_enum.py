from enum import IntEnum, auto


class ColumnEnum(IntEnum):
    # enterprise
    enterprise_id = auto()

    # identifier
    str_lc_id = auto()
    str_meter_id = auto()

    # meter
    customer_code = auto()
    atach_status = auto()
    ncu_id = auto()
    template_number = auto()
    open_status = auto()
    predict_target_flag = auto()
    # meter domain
    is_ncu = auto()
    enable_prediction = auto()

    # lc
    list_str_meter_id = auto()

    # cylinder
    main_cylinder_size = auto()
    main_cylinder_num = auto()
    sub_cylinder_size = auto()
    sub_cylinder_num = auto()
    str_staff_id = auto()
    str_weather_city_id = auto()

    # area
    lat = auto()
    lon = auto()
    addr = auto()

    # meter usage
    date = auto()
    time = auto()
    meter_usage_type = auto()
    accum_usage = auto()
    plug_type = auto()
    is_change = auto()
    is_read = auto()
    diff_usage = auto()
    diff_day_num = auto()
    daily_usage = auto()
    # meter usage domain
    datetime = auto()
    is_open = auto()

    # delivery result
    main_cylinder_remain_amount = auto()
    delivery_input_amount = auto()

    # weather
    ave_temp = auto()

    # features
    f_ncu_recent = auto()
    f_ncu_week = auto()
    f_ncu_ave_week = auto()
    f_ncu_dayofweek = auto()
    f_ncu_ave_dayofweek = auto()
    f_nonencu_diff_day = auto()
    f_nonencu_diff_usage = auto()
    f_nonencu_daily_usage = auto()
    f_target = auto()

    # dp model
    dp_model_classname = auto()
    dp_model_filename = auto()

    str_train_start_dt = auto()
    str_train_end_dt = auto()
    str_predict_start_dt = auto()
    train_output_term = auto()
    predict_output_term = auto()

    # ncu dp model
    ncu_raw_model = auto()
    ncu_model_recent_term = auto()
    ncu_model_week_term = auto()
    ncu_model_dayofweek_term = auto()
    ncu_model_train_sample_num = auto()
    ncu_model_val_sample_num = auto()

    # none ncu dp model
    none_ncu_model_train_sample_num = auto()
    none_ncu_model_val_sample_num = auto()
    none_ncu_model_recent_term = auto()
    none_ncu_raw_model = auto()

    # dp meter result
    c_predict_kg = auto()
    error_var = auto()
    str_last_change_dt = auto()

    # dp results with gt
    c_groundtruth_kg = auto()
    exist_groundtruth = auto()
