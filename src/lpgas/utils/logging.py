import logging


def get_lpgas_logger(module_name):
    logger = logging.getLogger(module_name)
    handler = logging.StreamHandler()
    format_str = "[%(asctime)s][%(name)s][%(levelname)s] %(message)s"
    formatter = logging.Formatter(format_str)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


def is_progress_index(
    index: int,
    length: int,
    count: int = 10,
) -> bool:
    if length > 0 and (length / count) >= 1:
        if index % int(length / count) == 0:
            return True
    return False


def get_str_progress(index: int, length: int) -> str:
    return f"[{int((index+1) * 100 / length):2}%][{(index+1)}/{length}]"
