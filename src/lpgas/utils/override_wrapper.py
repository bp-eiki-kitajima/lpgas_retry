def override(abstract_method):
    def overrider(method):
        assert method.__name__ == abstract_method.__name__
        return method

    return overrider
