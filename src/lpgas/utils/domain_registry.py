from lpgas.adapter.csv_cleansed_input_repo import Csv_CleansedInputRepo
from lpgas.adapter.dp_evaluation_repo import DPEvaluationRepo
from lpgas.adapter.dp_model_repo import DPModelRepo
from lpgas.adapter.dp_results_repo import DPResultsRepo
from lpgas.adapter.isg_raw_input_repo.isg_raw_input_repo import ISG_RawInputRepo
from lpgas.app.evaluate_usecase import EvaluateUsecase
from lpgas.app.predict_usecase import PredictUsecase
from lpgas.app.raw_input_usecase import RawInputUsecase
from lpgas.app.train_usecase import TrainUsecase
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_evaluation_repo import IF_DPEvaluationRepo
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.boudary.if_dp_results_repo import IF_DPResultsRepo
from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase
from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase
from lpgas.utils.boudary.if_raw_input_repo import IF_RawInputRepo
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase
from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase
from lpgas.utils.config_manager import ConfigManager


class DomainRegistry:
    """CongiManagerの返り値を元に抽象クラスの依存解決をする. 生成の際に引数を与えて挙動を決定させる."""

    def __init__(self, config_manager: ConfigManager) -> None:
        assert type(config_manager) is ConfigManager

        self._config_manager = config_manager

    # usecase
    def raw_input_usecase(self) -> IF_RawInputUsecase:
        classname, dict_params = self._config_manager.params_for_raw_input_usecase()
        if classname == RawInputUsecase.__name__:
            return RawInputUsecase(
                raw_input_repo=self.raw_input_repo(),
                cleansed_input_repo=self.cleansed_input_repo(),
                **dict_params,
            )
        else:
            raise ValueError

    def train_usecase(self) -> IF_TrainUsecase:
        classname, dict_params = self._config_manager.params_for_train_usecase()
        if classname == TrainUsecase.__name__:
            dp_model_repo = self.dp_model_repo()
            cleansed_input_repo = self.cleansed_input_repo()
            return TrainUsecase(
                dp_model_repo=dp_model_repo, cleansed_input_repo=cleansed_input_repo, **dict_params
            )
        else:
            raise ValueError

    def predict_usecase(self) -> IF_PredictUsecase:
        classname, dict_params = self._config_manager.params_for_predict_usecase()
        if classname == PredictUsecase.__name__:
            dp_model_repo = self.dp_model_repo()
            cleansed_input_repo = self.cleansed_input_repo()
            dp_results_repo = self.dp_results_repo()
            return PredictUsecase(
                dp_model_repo=dp_model_repo,
                cleansed_input_repo=cleansed_input_repo,
                dp_results_repo=dp_results_repo,
                **dict_params,
            )
        else:
            raise ValueError

    def evaluate_usecase(self) -> IF_EvaluateUsecase:
        classname, dict_params = self._config_manager.params_for_evaluate_usecase()
        if classname == EvaluateUsecase.__name__:
            cleansed_input_repo = self.cleansed_input_repo()
            dp_results_repo = self.dp_results_repo()
            dp_evaluation_repo = self.dp_evaluation_repo()
            return EvaluateUsecase(
                cleansed_input_repo=cleansed_input_repo,
                dp_results_repo=dp_results_repo,
                dp_evaluation_repo=dp_evaluation_repo,
                **dict_params,
            )
        else:
            raise ValueError

    # repo
    def raw_input_repo(self) -> IF_RawInputRepo:
        classname, dict_params = self._config_manager.params_for_raw_input_repo()
        if classname == ISG_RawInputRepo.__name__:
            return ISG_RawInputRepo(**dict_params)
        else:
            raise ValueError

    def dp_model_repo(self) -> IF_DPModelRepo:
        classname, dict_params = self._config_manager.params_for_dp_model_repo()
        if classname == DPModelRepo.__name__:
            return DPModelRepo(**dict_params)
        else:
            raise ValueError

    def cleansed_input_repo(self) -> IF_CleansedInputRepo:
        classname, dict_params = self._config_manager.params_for_cleansed_input_repo()
        if classname == Csv_CleansedInputRepo.__name__:
            return Csv_CleansedInputRepo(**dict_params)
        else:
            raise ValueError

    def dp_results_repo(self) -> IF_DPResultsRepo:
        classname, dict_params = self._config_manager.params_for_dp_results_repo()
        if classname == DPResultsRepo.__name__:
            return DPResultsRepo(**dict_params)
        else:
            raise ValueError

    def dp_evaluation_repo(self) -> IF_DPEvaluationRepo:
        classname, dict_params = self._config_manager.params_for_dp_evaluation_repo()
        if classname == DPEvaluationRepo.__name__:
            return DPEvaluationRepo(**dict_params)
        else:
            raise ValueError
