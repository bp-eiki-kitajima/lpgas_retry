from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_evaluation_repo import IF_DPEvaluationRepo
from lpgas.utils.boudary.if_dp_results_repo import IF_DPResultsRepo
from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class EvaluateUsecase(IF_EvaluateUsecase):
    """予測評価を得る"""

    def __init__(
        self,
        dp_results_repo: IF_DPResultsRepo,
        dp_evaluation_repo: IF_DPEvaluationRepo,
        cleansed_input_repo: IF_CleansedInputRepo,
        **kwargs,
    ) -> None:
        self._dp_results_repo = dp_results_repo
        self._dp_evaluation_repo = dp_evaluation_repo
        self._cleansed_input_repo = cleansed_input_repo

    @override(IF_EvaluateUsecase.evaluate_and_store_evaluation)
    def evaluate_and_store_evaluation(self) -> None:
        logger.info("評価開始")

        dp_results = self._dp_results_repo.load_latest()
        dp_results.attach_groundtruth(self._cleansed_input_repo)
        self._dp_evaluation_repo.store(dp_results)

    @override(IF_EvaluateUsecase.init_output)
    def init_output(self) -> None:
        logger.info("出力初期化")
        self._dp_evaluation_repo.init_output()
