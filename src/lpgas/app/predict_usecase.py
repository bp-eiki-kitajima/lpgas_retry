from typing import cast

import pandas as pd

from lpgas.domain.dp_results import DPResultsFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.boudary.if_dp_results_repo import IF_DPResultsRepo
from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class PredictUsecase(IF_PredictUsecase):
    """予測結果を得る.
    - 予測結果をPredictServiceから得る.
    - 予測結果をdp_results_repoに保存する.
    """

    def __init__(
        self,
        dp_model_repo: IF_DPModelRepo,
        dp_results_repo: IF_DPResultsRepo,
        cleansed_input_repo: IF_CleansedInputRepo,
        str_predict_start_dt: str,
        predict_output_term: int,
        **kwargs,
    ) -> None:
        self._dp_model_repo = dp_model_repo
        self._dp_results_repo = dp_results_repo
        self._cleansed_input_repo = cleansed_input_repo

        self._predict_start_dt = cast(pd.Timestamp, pd.to_datetime(str_predict_start_dt))
        self._predict_output_term = predict_output_term

    @override(IF_PredictUsecase.predict_and_store_results)
    def predict_and_store_results(self) -> None:
        logger.info("予測開始")

        dp_model = self._dp_model_repo.load_latest()
        df_dp_meter_results, list_meter_id_succedded_for_dpmodel = dp_model.predict(
            cleansed_input_repo=self._cleansed_input_repo,
            list_target_meter_id=None,
            predict_start_dt=self._predict_start_dt,
            predict_output_term=self._predict_output_term,
        )

        dp_results = DPResultsFactory().gene_dp_results(df_dp_meter_results)
        self._dp_results_repo.store(dp_results)

    @override(IF_PredictUsecase.init_output)
    def init_output(self) -> None:
        logger.info("出力初期化")
        self._dp_results_repo.init_output()
