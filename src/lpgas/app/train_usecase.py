from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class TrainUsecase(IF_TrainUsecase):
    """学習済みのDPModel保存する.
    - DPModelを初期化する.
    - DPModelを学習する.
    - 学習済みDPModelをdp_model_repoに保存する.
    """

    def __init__(
        self,
        dp_model_repo: IF_DPModelRepo,
        cleansed_input_repo: IF_CleansedInputRepo,
        **kwargs,
    ) -> None:
        self._dp_model_repo = dp_model_repo
        self._cleansed_input_repo = cleansed_input_repo
        self._dp_model = DPModelFactory().gene_from_args(**kwargs)

    @override(IF_TrainUsecase.train_and_store_model)
    def train_and_store_model(self) -> None:
        logger.info("学習開始")
        self._dp_model.train(self._cleansed_input_repo)
        self._dp_model_repo.store(self._dp_model)

    @override(IF_TrainUsecase.init_output)
    def init_output(self) -> None:
        logger.info("出力初期化")
        self._dp_model_repo.init_output()
