from lpgas.domain.preprocess.preprocess_service_factory import PreprocessServiceFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_raw_input_repo import IF_RawInputRepo
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class RawInputUsecase(IF_RawInputUsecase):
    """整形済みデータを作成し保存する.
    - 入力データを読み込んで集約のドメインオブジェクトを作成する
    - 集約のドメインオブジェクトをcsvに変換し, 保存する.
    """

    def __init__(
        self,
        raw_input_repo: IF_RawInputRepo,
        cleansed_input_repo: IF_CleansedInputRepo,
        **kwargs,
    ) -> None:
        self._raw_input_repo = raw_input_repo
        self._cleansed_input_repo = cleansed_input_repo
        self._preprocess_service = PreprocessServiceFactory().gene_from_args(**kwargs)

    @override(IF_RawInputUsecase.gene_and_store_all)
    def gene_and_store_all(self) -> None:
        logger.info("全ての集約の作成開始")

        self.gene_and_store_dict_meter(enable_preprocess=False)
        self.gene_and_store_dict_lc(enable_preprocess=False)
        self.gene_and_store_dict_meter_usage_data(enable_preprocess=False)
        self.gene_and_store_dict_ncu_meter_usage_data(enable_preprocess=False)
        self.gene_and_store_dict_delivery_result_data(enable_preprocess=False)
        self.gene_and_store_dict_weather_data(enable_preprocess=False)
        self._preprocess_service.apply_and_store(self._cleansed_input_repo)

    @override(IF_RawInputUsecase.gene_and_store_dict_meter)
    def gene_and_store_dict_meter(self, enable_preprocess: bool = True) -> None:
        logger.info("Meter集約の作成開始")
        dict_meter = self._raw_input_repo.gene_dict_meter()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_meter(dict_meter)

    @override(IF_RawInputUsecase.gene_and_store_dict_lc)
    def gene_and_store_dict_lc(self, enable_preprocess: bool = True) -> None:
        logger.info("LC集約の作成開始")
        dict_lc = self._raw_input_repo.gene_dict_lc()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_lc(dict_lc)

    @override(IF_RawInputUsecase.gene_and_store_dict_meter_usage_data)
    def gene_and_store_dict_meter_usage_data(self, enable_preprocess: bool = True) -> None:
        logger.info("MeterUsageData集約の作成開始")
        dict_meter_usage_data = self._raw_input_repo.gene_dict_meter_usage_data()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_meter_usage_data(dict_meter_usage_data)

    @override(IF_RawInputUsecase.gene_and_store_dict_ncu_meter_usage_data)
    def gene_and_store_dict_ncu_meter_usage_data(self, enable_preprocess: bool = True) -> None:
        logger.info("NcuMeterUsageData集約の作成開始")
        dict_ncu_meter_usage_data = self._raw_input_repo.gene_dict_ncu_meter_usage_data()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_ncu_meter_usage_data(dict_ncu_meter_usage_data)

    @override(IF_RawInputUsecase.gene_and_store_dict_delivery_result_data)
    def gene_and_store_dict_delivery_result_data(self, enable_preprocess: bool = True) -> None:
        logger.info("DeliveryResultData集約の作成開始")
        dict_delivery_result_data = self._raw_input_repo.gene_dict_delivery_result_data()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_delivery_result_data(dict_delivery_result_data)

    @override(IF_RawInputUsecase.gene_and_store_dict_weather_data)
    def gene_and_store_dict_weather_data(self, enable_preprocess: bool = True) -> None:
        logger.info("WeatherData集約の作成開始")
        dict_weather_data = self._raw_input_repo.gene_dict_weather_data()

        if enable_preprocess:
            self._preprocess_service.apply_and_store(self._cleansed_input_repo)
        else:
            self._cleansed_input_repo.store_dict_weather_data(dict_weather_data)

    @override(IF_RawInputUsecase.init_output)
    def init_output(self) -> None:
        logger.info("出力初期化")
        self._cleansed_input_repo.init_output()
