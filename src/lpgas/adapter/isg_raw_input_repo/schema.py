import pandas as pd
import pandera

from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum


class Schema_For_ISG_RawInputRepo:

    # raw
    def validate_df_lc_list_raw(self, df: pd.DataFrame) -> pd.DataFrame:

        _columns = {
            "LC": ColumnEnum.str_lc_id.name,
            "lat": ColumnEnum.lat.name,
            "lon": ColumnEnum.lon.name,
            "addr": ColumnEnum.addr.name,
            "主容容量": ColumnEnum.main_cylinder_size.name,
            "主容本数": ColumnEnum.main_cylinder_num.name,
            "予備容量": ColumnEnum.sub_cylinder_size.name,
            "予備本数": ColumnEnum.sub_cylinder_num.name,
            "staff": ColumnEnum.str_staff_id.name,
            "天候都市ID": ColumnEnum.str_weather_city_id.name,
        }

        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String),
                ColumnEnum.lat.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.lon.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.addr.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.main_cylinder_size.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.main_cylinder_num.name: pandera.Column(pandera.Int, nullable=True),
                ColumnEnum.sub_cylinder_size.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.sub_cylinder_num.name: pandera.Column(pandera.Int, nullable=True),
                ColumnEnum.str_staff_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.str_weather_city_id.name: pandera.Column(pandera.String, nullable=True),
            },
            coerce=True,
        )

        df = df.dropna(how="any", subset=[ColumnEnum.str_lc_id.name])
        df = df.sort_values([ColumnEnum.str_lc_id.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_joined_meter_raw(self, df: pd.DataFrame) -> pd.DataFrame:
        _columns = {
            "■供給ＬＣ": ColumnEnum.str_lc_id.name,
            "メーター": ColumnEnum.str_meter_id.name,
            "需要家ｺｰﾄﾞ": ColumnEnum.customer_code.name,
            "現場ステータス": ColumnEnum.atach_status.name,
            "取付NCU ID（QR）": ColumnEnum.ncu_id.name,
            "ﾃﾝﾌﾟﾚｰﾄ番号": ColumnEnum.template_number.name,
            "開栓状態": ColumnEnum.open_status.name,
            "予測対象フラグ": ColumnEnum.predict_target_flag.name,
        }

        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String),
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.customer_code.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.atach_status.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.ncu_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.template_number.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.open_status.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.predict_target_flag.name: pandera.Column(pandera.Bool, nullable=True),
            },
            coerce=True,
        )

        df = df.dropna(how="any", subset=[ColumnEnum.str_meter_id.name, ColumnEnum.str_lc_id.name])
        df = df.sort_values([ColumnEnum.str_lc_id.name, ColumnEnum.str_meter_id.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_meter_read_raw(self, df: pd.DataFrame) -> pd.DataFrame:

        _columns = {
            "販売事業者": ColumnEnum.enterprise_id.name,
            "供給ＬＣｺｰﾄﾞ": ColumnEnum.str_lc_id.name,
            "ﾒｰﾀｰｺｰﾄﾞ 枝番": ColumnEnum.str_meter_id.name,
            "指針日": ColumnEnum.date.name,
            "指針時刻": ColumnEnum.time.name,
            "指針区分": ColumnEnum.meter_usage_type.name,
            "指針": ColumnEnum.accum_usage.name,
            "栓区分": ColumnEnum.plug_type.name,
            "ボンベ交換": ColumnEnum.is_change.name,
        }

        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.enterprise_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.date.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.time.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.meter_usage_type.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.plug_type.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.is_change.name: pandera.Column(pandera.Bool, nullable=True),
            },
            coerce=True,
        )
        df = df.dropna(how="any", subset=[ColumnEnum.str_meter_id.name])
        df = df.sort_values(
            [
                ColumnEnum.str_lc_id.name,
                ColumnEnum.str_meter_id.name,
                ColumnEnum.date.name,
                ColumnEnum.time.name,
            ]
        )
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_meter_ncu_raw(self, df: pd.DataFrame) -> pd.DataFrame:

        _columns = {
            "取付NCU ID（QR）": ColumnEnum.ncu_id.name,
            "供給ＬＣコード": ColumnEnum.str_lc_id.name,
            "メーターID": ColumnEnum.str_meter_id.name,
            "年月日": ColumnEnum.date.name,
            "時": ColumnEnum.time.name,
            "検針値": ColumnEnum.accum_usage.name,
        }
        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.ncu_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.date.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.time.name: pandera.Column(pandera.String, nullable=True),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
            },
            coerce=True,
        )

        df = df.dropna(how="any", subset=[ColumnEnum.str_meter_id.name])
        df = df.sort_values(
            [
                ColumnEnum.str_lc_id.name,
                ColumnEnum.str_meter_id.name,
                ColumnEnum.date.name,
                ColumnEnum.time.name,
            ]
        )
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_delivery_result_raw(self, df: pd.DataFrame) -> pd.DataFrame:
        _columns = {
            "供給ＬＣｺｰﾄﾞ": ColumnEnum.str_lc_id.name,
            "配送実績日": ColumnEnum.date.name,
            "配送時指針": ColumnEnum.accum_usage.name,
            "主容器残ガス(KG)": ColumnEnum.main_cylinder_remain_amount.name,
            "納入量合計(KG)": ColumnEnum.delivery_input_amount.name,
            "配送実績時間": ColumnEnum.time.name,
        }
        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String),
                ColumnEnum.date.name: pandera.Column(pandera.String),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.main_cylinder_remain_amount.name: pandera.Column(
                    pandera.Float, nullable=True
                ),
                ColumnEnum.delivery_input_amount.name: pandera.Column(
                    pandera.Float, nullable=True
                ),
                ColumnEnum.time.name: pandera.Column(pandera.String, nullable=True),
            },
            coerce=True,
        )

        df = df.dropna(how="any", subset=[ColumnEnum.str_lc_id.name, ColumnEnum.date.name])
        df = df.sort_values(
            [
                ColumnEnum.str_lc_id.name,
                ColumnEnum.date.name,
                ColumnEnum.time.name,
            ]
        )
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_weather_raw(self, df: pd.DataFrame) -> pd.DataFrame:
        _columns = {
            "年月日": ColumnEnum.date.name,
            "平均気温(℃)": ColumnEnum.ave_temp.name,
            "天候都市ID": ColumnEnum.str_weather_city_id.name,
        }
        df = df.rename(columns=_columns)

        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.date.name: pandera.Column(pandera.String),
                ColumnEnum.ave_temp.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.str_weather_city_id.name: pandera.Column(pandera.String),
            },
            coerce=True,
        )

        df = df.dropna(
            how="any",
            subset=[
                ColumnEnum.date.name,
                ColumnEnum.str_weather_city_id.name,
            ],
        )
        df = df.sort_values(
            [
                ColumnEnum.str_weather_city_id.name,
                ColumnEnum.date.name,
            ]
        )
        df = df.reset_index(drop=True)
        return schema.validate(df)

    # processed
    def validate_df_lc_and_meter(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(
                    pandera.String, nullable=False, unique=True
                ),
                ColumnEnum.list_str_meter_id.name: pandera.Column(pandera.String, nullable=False),
            },
            strict=True,
            coerce=True,
        )
        df = df.dropna(how="any")
        df = df.sort_values(
            [
                ColumnEnum.str_lc_id.name,
            ]
        )
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_meter_attr(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_meter(df)

    def validate_df_lc_attr(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_lc(df)

    def validate_df_meter_usage_value(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_meter_usage_data(df)

    def validate_df_ncu_meter_usage_value(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_ncu_meter_usage_data(df)

    def validate_df_delivery_result_value(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_delivery_result_data(df)

    def validate_df_weather(self, df: pd.DataFrame) -> pd.DataFrame:
        return Schema_For_Domain().validate_df_weather_data(df)
