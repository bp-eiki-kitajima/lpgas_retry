import pathlib
from datetime import datetime
from typing import Optional

import numpy as np
import pandas as pd

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory
from lpgas.utils.boudary.if_raw_input_repo import IF_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class ISG_RawInputRepo(IF_RawInputRepo):
    def __init__(
        self,
        lc_list_csv_path: str,
        joined_meter_csv_path: str,
        meter_read_csv_path: str,
        meter_ncu_csv_path: str,
        delivery_result_csv_path: str,
        weather_csv_path: str,
        **kwargs,
    ) -> None:
        self._lc_list_csv_path = pathlib.Path(lc_list_csv_path)
        self._joined_meter_csv_path = pathlib.Path(joined_meter_csv_path)
        self._meter_read_csv_path = pathlib.Path(meter_read_csv_path)
        self._meter_ncu_csv_path = pathlib.Path(meter_ncu_csv_path)
        self._delivery_result_csv_path = pathlib.Path(delivery_result_csv_path)
        self._weather_csv_path = pathlib.Path(weather_csv_path)

        self._init_df()

    def _init_df(self):
        self._df_lc_list_raw: Optional[pd.DataFrame] = None
        self._df_joined_meter_raw: Optional[pd.DataFrame] = None
        self._df_meter_read_raw: Optional[pd.DataFrame] = None
        self._df_meter_ncu_raw: Optional[pd.DataFrame] = None
        self._df_delivery_result_raw: Optional[pd.DataFrame] = None
        self._df_weather_raw: Optional[pd.DataFrame] = None

    def _get_lc_list_csv_path(self) -> pathlib.Path:
        return self._lc_list_csv_path

    def _get_joined_meter_csv_path(self) -> pathlib.Path:
        return self._joined_meter_csv_path

    def _get_meter_read_csv_path(self) -> pathlib.Path:
        return self._meter_read_csv_path

    def _get_meter_ncu_csv_path(self) -> pathlib.Path:
        return self._meter_ncu_csv_path

    def _get_delivery_result_csv_path(self) -> pathlib.Path:
        return self._delivery_result_csv_path

    def _get_weather_csv_path(self) -> pathlib.Path:
        return self._weather_csv_path

    def _load_lc_list_raw(self) -> pd.DataFrame:
        logger.info("入力データ(lc_list)の読込開始")

        if self._df_lc_list_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_lc_list_raw

        df_lc_list_raw = pd.read_csv(self._get_lc_list_csv_path())
        self._df_lc_list_raw = Schema_For_ISG_RawInputRepo().validate_df_lc_list_raw(
            df_lc_list_raw
        )
        return self._df_lc_list_raw.copy()

    def _load_joined_meter_raw(self) -> pd.DataFrame:
        logger.info("入力データ(joined_meter)の読込開始")
        if self._df_joined_meter_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_joined_meter_raw

        df_joined_meter_raw = pd.read_csv(self._get_joined_meter_csv_path())
        self._df_joined_meter_raw = Schema_For_ISG_RawInputRepo().validate_df_joined_meter_raw(
            df_joined_meter_raw
        )
        return self._df_joined_meter_raw.copy()

    def _load_meter_read_raw(self) -> pd.DataFrame:
        logger.info("入力データ(meter_read)の読込開始")

        if self._df_meter_read_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_meter_read_raw

        df_meter_read_raw = pd.read_csv(self._get_meter_read_csv_path())
        self._df_meter_read_raw = Schema_For_ISG_RawInputRepo().validate_df_meter_read_raw(
            df_meter_read_raw
        )

        return self._df_meter_read_raw.copy()

    def _load_meter_ncu_raw(self) -> pd.DataFrame:
        logger.info("入力データ(meter_ncu)の読込開始")

        if self._df_meter_ncu_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_meter_ncu_raw

        df_meter_ncu_raw = pd.read_csv(self._get_meter_ncu_csv_path())
        self._df_meter_ncu_raw = Schema_For_ISG_RawInputRepo().validate_df_meter_ncu_raw(
            df_meter_ncu_raw
        )
        return self._df_meter_ncu_raw.copy()

    def _load_delivery_result_raw(self) -> pd.DataFrame:
        logger.info("入力データ(delivery_result)の読込開始")
        if self._df_delivery_result_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_delivery_result_raw

        df_delivery_result_raw = pd.read_csv(self._get_delivery_result_csv_path())
        self._df_delivery_result_raw = (
            Schema_For_ISG_RawInputRepo().validate_df_delivery_result_raw(df_delivery_result_raw)
        )
        return self._df_delivery_result_raw.copy()

    def _load_weather_raw(self) -> pd.DataFrame:
        logger.info("入力データ(weather)の読込開始")
        if self._df_weather_raw is not None:
            logger.info("読込済みより, 読込スキップ")
            return self._df_weather_raw

        df_weather_raw = pd.read_csv(self._get_weather_csv_path())
        self._df_weather_raw = Schema_For_ISG_RawInputRepo().validate_df_weather_raw(
            df_weather_raw
        )
        return self._df_weather_raw.copy()

    def _get_df_lc_and_meter(self) -> pd.DataFrame:
        """str_meter_idとstr_lc_idが列のdfを返す.
        str_lc_idがnanならその行は無視.
        str_meter_idがnanならその行は無視.
        """

        df_joined_meter_raw = self._load_joined_meter_raw()

        list_df_joined_meter_raw = [
            (str_lc_id, df_grouped)
            for str_lc_id, df_grouped in df_joined_meter_raw.groupby(ColumnEnum.str_lc_id.name)
        ]

        dict_str_list_meter_id = {}
        for str_lc_id, df_grouped in list_df_joined_meter_raw:
            try:
                list_str_meter_id = list(df_grouped[ColumnEnum.str_meter_id.name].unique())
                str_list_meter_id = " ".join(list_str_meter_id)
                dict_str_list_meter_id[str_lc_id] = [str_list_meter_id]
            except TypeError:
                continue

        df_lc_and_meter = pd.DataFrame.from_dict(
            dict_str_list_meter_id, orient="index"
        ).reset_index()

        df_lc_and_meter.columns = [
            ColumnEnum.str_lc_id.name,
            ColumnEnum.list_str_meter_id.name,
        ]

        return Schema_For_ISG_RawInputRepo().validate_df_lc_and_meter(df_lc_and_meter)

    def _get_df_meter_attr(self) -> pd.DataFrame:
        """メータの属性情報を取り出す.
        ncu_idがstrの時のみ, is_ncu=True.
        template_numberがstrでなければ, "0"に置換.
        open_statusは常に, is_open=True.
        predict_target_flagがFalseでなければ, enable_prediction=True.
        """
        df_joined_meter_raw = self._load_joined_meter_raw()
        df_meter_attr = pd.DataFrame()

        df_meter_attr[ColumnEnum.str_meter_id.name] = df_joined_meter_raw[
            ColumnEnum.str_meter_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        df_meter_attr[ColumnEnum.str_lc_id.name] = df_joined_meter_raw[
            ColumnEnum.str_lc_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        df_meter_attr[ColumnEnum.is_ncu.name] = df_joined_meter_raw[ColumnEnum.ncu_id.name].apply(
            lambda x: True if type(x) is str else False
        )

        df_meter_attr[ColumnEnum.template_number.name] = df_joined_meter_raw[
            ColumnEnum.template_number.name
        ].apply(lambda x: x if type(x) is str else "0")

        df_meter_attr[ColumnEnum.enable_prediction.name] = df_joined_meter_raw[
            ColumnEnum.predict_target_flag.name
        ].apply(lambda x: True if x is not False else False)

        return Schema_For_ISG_RawInputRepo().validate_df_meter_attr(df_meter_attr)

    def _get_df_lc_attr(self) -> pd.DataFrame:
        """LCの属性情報を取り出す."""

        df_lc_list_raw = self._load_lc_list_raw()
        df_lc_and_meter = self._get_df_lc_and_meter()

        df_lc_attr = pd.merge(
            df_lc_list_raw, df_lc_and_meter, on=ColumnEnum.str_lc_id.name, how="left"
        )

        df_lc_attr[ColumnEnum.str_lc_id.name] = df_lc_attr[ColumnEnum.str_lc_id.name].apply(
            lambda x: x if type(x) is str else np.nan
        )

        df_lc_attr[ColumnEnum.list_str_meter_id.name] = df_lc_attr[
            ColumnEnum.list_str_meter_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        # TODO バリデーション気が向いたら追加する

        return Schema_For_ISG_RawInputRepo().validate_df_lc_attr(df_lc_attr)

    def _get_df_meter_usage_value(self) -> pd.DataFrame:
        """MeterUsageData構築に必要なdfを返す.
        strでないmeter_idは無視.
        検針日時をtimestampに変換できない場合, np.nanに置換.
        plug_typeが"1"でないなら, is_openはFalse.
        is_changeがFalseでないなら, True.
        meter_usage_value列以外のnp.nanを含む行は削除.
        """
        logger.info("MeterUsageData作成に必要なDFを作成開始")

        df_meter_read_raw = self._load_meter_read_raw()
        df_meter_usage_value = pd.DataFrame()

        df_meter_usage_value[ColumnEnum.str_meter_id.name] = df_meter_read_raw[
            ColumnEnum.str_meter_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        """
        df_meter_usage_value[ColumnEnum.datetime.name] = df_meter_read_raw.apply(
            lambda ser: self._get_datetime(ser), axis=1
        )
        """
        df_meter_usage_value[ColumnEnum.datetime.name] = self._get_ser_of_serftime(
            df_meter_read_raw
        )

        df_meter_usage_value[ColumnEnum.accum_usage.name] = df_meter_read_raw[
            ColumnEnum.accum_usage.name
        ].apply(lambda x: x if type(x) is float else np.nan)

        df_meter_usage_value[ColumnEnum.is_open.name] = df_meter_read_raw[
            ColumnEnum.plug_type.name
        ].apply(lambda x: True if (x != "0") else False)

        df_meter_usage_value[ColumnEnum.is_change.name] = df_meter_read_raw[
            ColumnEnum.is_change.name
        ].apply(lambda x: True if x is not False else False)

        df_meter_usage_value[ColumnEnum.is_read.name] = df_meter_read_raw[
            ColumnEnum.accum_usage.name
        ].apply(lambda x: True if type(x) is float else False)

        return Schema_For_ISG_RawInputRepo().validate_df_meter_usage_value(df_meter_usage_value)

    def _get_df_ncu_meter_usage_value(self) -> pd.DataFrame:
        """NcuMeterUsageData構築に必要なdfを返す.
        strでないmeter_idは無視.
        検針日時をtimestampに変換できない場合, np.nanに置換.
        meter_reading_value列以外のnp.nanを含む行は削除.
        """

        df_meter_ncu_raw = self._load_meter_ncu_raw()
        df_ncu_meter_usage_value = pd.DataFrame()

        df_ncu_meter_usage_value[ColumnEnum.str_meter_id.name] = df_meter_ncu_raw[
            ColumnEnum.str_meter_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        """
        df_ncu_meter_usage_value[ColumnEnum.datetime.name] = df_meter_ncu_raw.apply(
            lambda ser: self._get_datetime(ser), axis=1
        )
        """

        df_ncu_meter_usage_value[ColumnEnum.datetime.name] = self._get_ser_of_serftime(
            df_meter_ncu_raw
        )

        df_ncu_meter_usage_value[ColumnEnum.accum_usage.name] = df_meter_ncu_raw[
            ColumnEnum.accum_usage.name
        ].apply(lambda x: x if type(x) is float else np.nan)

        return Schema_For_ISG_RawInputRepo().validate_df_ncu_meter_usage_value(
            df_ncu_meter_usage_value
        )

    def _get_df_delivery_result(self) -> pd.DataFrame:
        """DeliveryResultDataの生成に必要なdfを返す.
        無効なstr_lc_idの行は無視.
        無効なdatetimeの行は無視.
        無効なmeter_usage_valueの行はnanで埋める.
        無効なmain_cylinder_remain_amountの行は無視.
        無効なdelivery_amountはLCのmain_cylinder_sizeで埋める
        0のdelivery_amountは無視.
        """
        df_delivery_result_raw = self._load_delivery_result_raw()
        df_delivery_result_value = pd.DataFrame()

        df_delivery_result_value[ColumnEnum.str_lc_id.name] = df_delivery_result_raw[
            ColumnEnum.str_lc_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        """
        df_delivery_result_value[ColumnEnum.datetime.name] = df_delivery_result_raw.apply(
            lambda ser: self._get_datetime(ser), axis=1
        )
        """
        df_delivery_result_value[ColumnEnum.datetime.name] = self._get_ser_of_serftime(
            df_delivery_result_raw
        )

        df_delivery_result_value[ColumnEnum.accum_usage.name] = df_delivery_result_raw[
            ColumnEnum.accum_usage.name
        ].apply(lambda x: x if type(x) is float else np.nan)

        df_delivery_result_value[
            ColumnEnum.main_cylinder_remain_amount.name
        ] = df_delivery_result_raw[ColumnEnum.main_cylinder_remain_amount.name].apply(
            lambda x: x if type(x) is float else np.nan
        )

        def get_delivery_input_amount(value):
            if type(value) is float:
                if value > 0:
                    return value
            return np.nan

        df_delivery_result_value[ColumnEnum.delivery_input_amount.name] = df_delivery_result_raw[
            ColumnEnum.delivery_input_amount.name
        ].apply(lambda x: get_delivery_input_amount(x))

        def is_change(value):
            if type(value) is float:
                if value > 0:
                    return True
            return False

        df_delivery_result_value[ColumnEnum.is_change.name] = df_delivery_result_raw[
            ColumnEnum.delivery_input_amount.name
        ].apply(lambda x: is_change(x))

        def is_read(value):
            if type(value) is float:
                if value > 0:
                    return True
            return False

        df_delivery_result_value[ColumnEnum.is_read.name] = df_delivery_result_raw[
            ColumnEnum.accum_usage.name
        ].apply(lambda x: is_read(x))

        return Schema_For_ISG_RawInputRepo().validate_df_delivery_result_value(
            df_delivery_result_value
        )

    def _get_df_weather(self) -> pd.DataFrame:
        """生の天候情報を前処理して取り出す."""
        df_weather_raw = self._load_weather_raw()
        df_weather = pd.DataFrame()

        df_weather[ColumnEnum.datetime.name] = self._get_serftime_of_weather(df_weather_raw)

        df_weather[ColumnEnum.ave_temp.name] = df_weather_raw[ColumnEnum.ave_temp.name].apply(
            lambda x: x if type(x) is float else np.nan
        )

        df_weather[ColumnEnum.str_weather_city_id.name] = df_weather_raw[
            ColumnEnum.str_weather_city_id.name
        ].apply(lambda x: x if type(x) is str else np.nan)

        # TODO 前処理に移動する
        # 欠損した日付を埋める
        def fill_missing_dt(df: pd.DataFrame):
            df_datetime = pd.to_datetime(df[ColumnEnum.datetime.name]).dt.date
            list_filled_dt = [
                timestamp.strftime("%Y/%m/%d %H:%M:%S")
                for timestamp in pd.date_range(df_datetime.min(), df_datetime.max(), freq="D")
            ]

            narray_str_weather_city_id, narray_filled_dt = np.meshgrid(
                list(df[ColumnEnum.str_weather_city_id.name].unique()), list_filled_dt
            )
            df_filled_dt = pd.DataFrame(
                {
                    ColumnEnum.str_weather_city_id.name: narray_str_weather_city_id.ravel(),
                    ColumnEnum.datetime.name: narray_filled_dt.ravel(),
                }
            )

            return pd.merge(df_filled_dt, df, how="left")

        df_weather = fill_missing_dt(df_weather)

        # nanのave_tempを埋める
        med_ave_temp = df_weather[ColumnEnum.ave_temp.name].median()
        med_ave_temp = round(med_ave_temp, 1)
        df_weather[ColumnEnum.ave_temp.name] = df_weather[ColumnEnum.ave_temp.name].fillna(
            med_ave_temp
        )

        return Schema_For_ISG_RawInputRepo().validate_df_weather(df_weather)

    @override(IF_RawInputRepo.gene_dict_meter)
    def gene_dict_meter(self) -> dict[MeterID, Meter]:

        # meterと属性の関係がvalidation済のdf
        df_meter_attr = self._get_df_meter_attr()

        return MeterFactory().gene_dict_from_df(df_meter_attr)

    @override(IF_RawInputRepo.gene_dict_lc)
    def gene_dict_lc(self) -> dict[LC_ID, LC]:
        df_lc_attr = self._get_df_lc_attr()

        return LCFactory().gene_dict_from_df(df_lc_attr)

    @override(IF_RawInputRepo.gene_dict_meter_usage_data)
    def gene_dict_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, MeterUsageData]:
        """MeterUsageDataを構築し返す.
        validate済のmeterのみ対象.
        list_meter_idが指定されている場合, list_meterに含まれるmeterで構築する.
        list_meter_idがNoneなら, 全てのmeter分を構築する.
        """

        def filter_df_meter_usage_value(
            list_meter_id: Optional[list[MeterID]], df_meter_usage_value: pd.DataFrame
        ):
            if list_meter_id is None:
                return df_meter_usage_value
            else:
                list_str_meter_id = [meter_id.id for meter_id in set(list_meter_id)]
                return df_meter_usage_value[
                    df_meter_usage_value[ColumnEnum.str_meter_id.name].isin(list_str_meter_id)
                ]

        df_meter_usage_value = self._get_df_meter_usage_value()

        df_meter_usage_value_filtered = filter_df_meter_usage_value(
            list_meter_id, df_meter_usage_value
        )

        return MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_value_filtered)

    @override(IF_RawInputRepo.gene_dict_ncu_meter_usage_data)
    def gene_dict_ncu_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, NcuMeterUsageData]:
        """NcuMeterUsageDataを構築し返す.
        validate済のmeterのみ対象.
        list_meter_idがNoneなら, 全てのmeter分を構築する.
        """

        def filter_df_ncu_meter_usage_value(
            list_meter_id: Optional[list[MeterID]], df_ncu_meter_usage_value: pd.DataFrame
        ):
            if list_meter_id is None:
                return df_ncu_meter_usage_value
            else:
                list_str_meter_id = [meter_id.id for meter_id in set(list_meter_id)]
                return df_ncu_meter_usage_value[
                    df_ncu_meter_usage_value[ColumnEnum.str_meter_id.name].isin(list_str_meter_id)
                ]

        df_ncu_meter_usage_value = self._get_df_ncu_meter_usage_value()
        df_ncu_meter_usage_value_filtered = filter_df_ncu_meter_usage_value(
            list_meter_id, df_ncu_meter_usage_value
        )
        return NcuMeterUsageDataFactory().gene_dict_from_df(df_ncu_meter_usage_value_filtered)

    @override(IF_RawInputRepo.gene_dict_delivery_result_data)
    def gene_dict_delivery_result_data(
        self, list_lc_id: Optional[list[LC_ID]] = None
    ) -> dict[LC_ID, DeliveryResultData]:
        """DeliveryResultDataを構築し返す.
        validate済のmeterのみ対象.
        list_meter_idがNoneなら, 全てのmeter分を構築する.
        """

        def filter_df_delivery_result(
            list_lc_id: Optional[list[LC_ID]], df_delivery_result: pd.DataFrame
        ):
            if list_lc_id is None:
                return df_delivery_result
            else:
                list_str_lc_id = [lc_id.id for lc_id in set(list_lc_id)]
                return df_delivery_result[
                    df_delivery_result[ColumnEnum.str_lc_id.name].isin(list_str_lc_id)
                ]

        df_delivery_result = self._get_df_delivery_result()
        df_delivery_result_filtered = filter_df_delivery_result(list_lc_id, df_delivery_result)

        return DeliveryResultDataFactory().gene_dict_from_df(df_delivery_result_filtered)

    @override(IF_RawInputRepo.gene_dict_weather_data)
    def gene_dict_weather_data(
        self, list_weather_city_id: Optional[list[WeatherCityID]] = None
    ) -> dict[WeatherCityID, WeatherData]:
        def filter_df_weather_data(
            list_weather_city_id: Optional[list[WeatherCityID]], df_weather: pd.DataFrame
        ):
            if list_weather_city_id is None:
                return df_weather
            else:
                list_str_weather_city_id = [
                    weather_city_id.id for weather_city_id in set(list_weather_city_id)
                ]
                return df_weather[
                    df_weather[ColumnEnum.str_weather_city_id.name].isin(list_str_weather_city_id)
                ]

        df_weather = self._get_df_weather()
        df_weather_filtered = filter_df_weather_data(list_weather_city_id, df_weather)

        return WeatherDataFactory().gene_dict_from_df(df_weather_filtered)

    def _get_strftime(self, x, list_input_format, output_format):

        for dt_format in list_input_format:
            try:
                return datetime.strptime(x, dt_format).strftime(output_format)
            except ValueError:
                continue
        return np.nan

    def _get_ser_of_serftime(self, df: pd.DataFrame):
        ser_date = df[ColumnEnum.date.name]
        ser_time = df[ColumnEnum.time.name].apply(lambda x: x.zfill(4))

        ser_datetime = ser_date + " " + ser_time
        list_input_format = ["%Y/%m/%d %H%M", "%Y-%m-%d %H%M"]
        output_format = "%Y/%m/%d %H:%M:%S"
        for dt_format in list_input_format:
            try:
                return pd.to_datetime(ser_datetime, format=dt_format).dt.strftime(output_format)
            except ValueError:
                continue

        return ser_datetime.apply(
            lambda x: self._get_strftime(x, list_input_format, output_format)
        )

    def _get_serftime_of_weather(self, df: pd.DataFrame):
        ser_datetime = df[ColumnEnum.date.name]

        list_input_format = ["%Y/%m/%d", "%Y-%m-%d"]
        output_format = "%Y/%m/%d %H:%M:%S"
        for dt_format in list_input_format:
            try:
                return pd.to_datetime(ser_datetime, format=dt_format).dt.strftime(output_format)
            except ValueError:
                continue

        return ser_datetime.apply(
            lambda x: self._get_strftime(x, list_input_format, output_format)
        )
