import pathlib
from typing import Optional

import pandas as pd

from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class Csv_CleansedInputRepo(IF_CleansedInputRepo):
    def __init__(
        self,
        meter_path: str,
        lc_path: str,
        meter_usage_data_path: str,
        ncu_meter_usage_data_path: str,
        delivery_result_data_path: str,
        weather_data_path: str,
        **kwargs,
    ) -> None:
        self._meter_path = pathlib.Path(meter_path)
        self._lc_path = pathlib.Path(lc_path)
        self._meter_usage_data_path = pathlib.Path(meter_usage_data_path)
        self._ncu_meter_usage_data_path = pathlib.Path(ncu_meter_usage_data_path)
        self._delivery_result_data_path = pathlib.Path(delivery_result_data_path)
        self._weather_data_path = pathlib.Path(weather_data_path)

        self._init_df()

    def _init_df(self):
        self._dict_meter: Optional[dict[MeterID, Meter]] = None
        self._dict_lc: Optional[dict[LC_ID, LC]] = None
        self._dict_meter_usage_data: Optional[dict[MeterID, MeterUsageData]] = None
        self._dict_ncu_meter_usage_data: Optional[dict[MeterID, NcuMeterUsageData]] = None
        self._dict_delivery_result_data: Optional[dict[LC_ID, DeliveryResultData]] = None
        self._dict_weather_data: Optional[dict[WeatherCityID, WeatherData]] = None

    def _get_meter_path(self) -> pathlib.Path:
        return self._meter_path

    def _get_lc_path(self) -> pathlib.Path:
        return self._lc_path

    def _get_meter_usage_data_path(self) -> pathlib.Path:
        return self._meter_usage_data_path

    def _get_ncu_meter_usage_data_path(self) -> pathlib.Path:
        return self._ncu_meter_usage_data_path

    def _get_delivery_result_data_path(self) -> pathlib.Path:
        return self._delivery_result_data_path

    def _get_weather_data_path(self) -> pathlib.Path:
        return self._weather_data_path

    def _load_dict_meter(self) -> dict[MeterID, Meter]:

        if self._dict_meter is None:
            df_meter = pd.read_csv(self._get_meter_path(), encoding="utf_8_sig")
            self._dict_meter = MeterFactory().gene_dict_from_df(df_meter=df_meter)
        return self._dict_meter

    def _load_dict_lc(self) -> dict[LC_ID, LC]:

        if self._dict_lc is None:
            df_lc = pd.read_csv(self._get_lc_path(), encoding="utf_8_sig")
            self._dict_lc = LCFactory().gene_dict_from_df(df_lc=df_lc)
        return self._dict_lc

    def _load_dict_meter_usage_data(self) -> dict[MeterID, MeterUsageData]:

        if self._dict_meter_usage_data is None:
            df_meter_usage_data = pd.read_csv(
                self._get_meter_usage_data_path(), encoding="utf_8_sig"
            )
            self._dict_meter_usage_data = MeterUsageDataFactory().gene_dict_from_df(
                df_meter_usage_data
            )
        return self._dict_meter_usage_data

    def _load_dict_ncu_meter_usage_data(self) -> dict[MeterID, NcuMeterUsageData]:

        if self._dict_ncu_meter_usage_data is None:
            df_ncu_meter_usage_data = pd.read_csv(
                self._get_ncu_meter_usage_data_path(), encoding="utf_8_sig"
            )
            self._dict_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dict_from_df(
                df_ncu_meter_usage_data
            )

        return self._dict_ncu_meter_usage_data

    def _load_dict_delivery_result_data(self) -> dict[LC_ID, DeliveryResultData]:

        if self._dict_delivery_result_data is None:
            df_delivery_result_data = pd.read_csv(
                self._get_delivery_result_data_path(), encoding="utf_8_sig"
            )
            self._dict_delivery_result_data = DeliveryResultDataFactory().gene_dict_from_df(
                df_delivery_result_data
            )

        return self._dict_delivery_result_data

    def _load_dict_weather_data(self) -> dict[WeatherCityID, WeatherData]:

        if self._dict_weather_data is None:
            df_weather_data = pd.read_csv(self._get_weather_data_path(), encoding="utf_8_sig")
            self._dict_weather_data = WeatherDataFactory().gene_dict_from_df(df_weather_data)

        return self._dict_weather_data

    # get
    @override(IF_CleansedInputRepo.get_dict_meter)
    def get_dict_meter(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, Meter]:

        dict_meter = self._load_dict_meter()
        if list_meter_id is None:
            return dict_meter
        else:
            set_meter_id = set(list_meter_id)
            return {
                meter_id: meter
                for meter_id, meter in dict_meter.items()
                if meter.meter_id in set_meter_id
            }

    @override(IF_CleansedInputRepo.get_dict_lc)
    def get_dict_lc(self, list_lc_id: Optional[list[LC_ID]] = None) -> dict[LC_ID, LC]:

        dict_lc = self._load_dict_lc()
        if list_lc_id is None:
            return dict_lc
        else:
            set_lc_id = set(list_lc_id)
            return {lc_id: lc for lc_id, lc in dict_lc.items() if lc.lc_id in set_lc_id}

    @override(IF_CleansedInputRepo.get_dict_meter_usage_data)
    def get_dict_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, MeterUsageData]:

        dict_meter_usage_data = self._load_dict_meter_usage_data()
        if list_meter_id is None:
            return dict_meter_usage_data
        else:
            set_meter_id = set(list_meter_id)
            return {
                meter_id: meter_usage_data
                for meter_id, meter_usage_data in dict_meter_usage_data.items()
                if meter_usage_data.meter_id in set_meter_id
            }

    @override(IF_CleansedInputRepo.get_dict_ncu_meter_usage_data)
    def get_dict_ncu_meter_usage_data(
        self, list_meter_id: Optional[list[MeterID]] = None
    ) -> dict[MeterID, NcuMeterUsageData]:

        dict_ncu_meter_usage_data = self._load_dict_ncu_meter_usage_data()
        if list_meter_id is None:
            return dict_ncu_meter_usage_data
        else:
            set_meter_id = set(list_meter_id)
            return {
                meter_id: ncu_meter_usage_data
                for meter_id, ncu_meter_usage_data in dict_ncu_meter_usage_data.items()
                if ncu_meter_usage_data.meter_id in set_meter_id
            }

    @override(IF_CleansedInputRepo.get_dict_delivery_result_data)
    def get_dict_delivery_result_data(
        self, list_lc_id: Optional[list[LC_ID]] = None
    ) -> dict[LC_ID, DeliveryResultData]:

        dict_delivery_result_data = self._load_dict_delivery_result_data()
        if list_lc_id is None:
            return dict_delivery_result_data
        else:
            set_lc_id = set(list_lc_id)
            return {
                lc_id: delivery_result_data
                for lc_id, delivery_result_data in dict_delivery_result_data.items()
                if delivery_result_data.lc_id in set_lc_id
            }

    @override(IF_CleansedInputRepo.get_dict_weather_data)
    def get_dict_weather_data(
        self, list_weather_city_id: Optional[list[WeatherCityID]] = None
    ) -> dict[WeatherCityID, WeatherData]:

        dict_weather_data = self._load_dict_weather_data()
        if list_weather_city_id is None:
            return dict_weather_data
        else:
            set_weather_city_id = set(list_weather_city_id)
            return {
                weather_city_id: weather_data
                for weather_city_id, weather_data in dict_weather_data.items()
                if weather_data.weather_city_id in set_weather_city_id
            }

    # store
    def _store_csv(self, p: pathlib.Path, df: pd.DataFrame):
        p.parent.mkdir(parents=True, exist_ok=True)
        df.to_csv(p, index=False, encoding="utf_8_sig")

    @override(IF_CleansedInputRepo.store_dict_meter)
    def store_dict_meter(self, dict_meter: dict[MeterID, Meter]) -> None:
        logger.info("Meter集約保存")

        df_meter = MeterFactory().dump_df_from_dict(dict_meter=dict_meter)
        self._store_csv(self._get_meter_path(), df_meter)

    @override(IF_CleansedInputRepo.store_dict_lc)
    def store_dict_lc(self, dict_lc: dict[LC_ID, LC]) -> None:
        logger.info("LC集約保存")

        df_lc = LCFactory().dump_df_from_dict(dict_lc)
        self._store_csv(self._get_lc_path(), df_lc)

    @override(IF_CleansedInputRepo.store_dict_meter_usage_data)
    def store_dict_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData]
    ) -> None:
        logger.info("MeterUsageData集約保存")

        df_meter_usage_data = MeterUsageDataFactory().dump_df_from_dict(dict_meter_usage_data)
        self._store_csv(self._get_meter_usage_data_path(), df_meter_usage_data)

    @override(IF_CleansedInputRepo.store_dict_ncu_meter_usage_data)
    def store_dict_ncu_meter_usage_data(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData]
    ) -> None:
        logger.info("NcuMeterUsageData集約保存")

        df_ncu_meter_usage_data = NcuMeterUsageDataFactory().dump_df_from_dict(
            dict_ncu_meter_usage_data
        )
        self._store_csv(self._get_ncu_meter_usage_data_path(), df_ncu_meter_usage_data)

    @override(IF_CleansedInputRepo.store_dict_delivery_result_data)
    def store_dict_delivery_result_data(
        self, dict_delivery_result_data: dict[LC_ID, DeliveryResultData]
    ) -> None:
        logger.info("DeliveryResultData集約保存")

        df_delivery_result_data = DeliveryResultDataFactory().dump_df_from_dict(
            dict_delivery_result_data
        )
        self._store_csv(self._get_delivery_result_data_path(), df_delivery_result_data)

    @override(IF_CleansedInputRepo.store_dict_weather_data)
    def store_dict_weather_data(self, dict_weather_data: dict[WeatherCityID, WeatherData]) -> None:
        logger.info("WeatherData集約保存")

        df_weather_data = WeatherDataFactory().dump_df_from_dict(dict_weather_data)
        self._store_csv(self._get_weather_data_path(), df_weather_data)

    @override(IF_CleansedInputRepo.init_output)
    def init_output(self) -> None:
        logger.info("Repo初期化")

        self._get_meter_path().unlink(missing_ok=True)
        self._get_lc_path().unlink(missing_ok=True)
        self._get_meter_usage_data_path().unlink(missing_ok=True)
        self._get_ncu_meter_usage_data_path().unlink(missing_ok=True)
        self._get_delivery_result_data_path().unlink(missing_ok=True)
        self._get_weather_data_path().unlink(missing_ok=True)
