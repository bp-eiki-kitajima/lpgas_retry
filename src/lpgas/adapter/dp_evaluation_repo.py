import pathlib

import pandas as pd

from lpgas.domain.dp_results import DPResults
from lpgas.utils.boudary.if_dp_evaluation_repo import IF_DPEvaluationRepo
from lpgas.utils.override_wrapper import override


class DPEvaluationRepo(IF_DPEvaluationRepo):
    def __init__(
        self,
        dp_evaluation_dir_path: str,
        dp_meter_results_filename: str,
        dp_lc_results_filename: str,
        **kwargs,
    ) -> None:
        self._p_dp_evaluation_dir = pathlib.Path(dp_evaluation_dir_path)
        self._p_dp_meter_results = self._p_dp_evaluation_dir / dp_meter_results_filename
        self._p_dp_lc_results = self._p_dp_evaluation_dir / dp_lc_results_filename

    def _get_latest_dp_meter_results_path(self) -> pathlib.Path:
        return self._p_dp_meter_results

    def _get_latest_dp_lc_results_path(self) -> pathlib.Path:
        return self._p_dp_lc_results

    def _store_df_dp_meter_results(self, df_dp_meter_results: pd.DataFrame) -> None:
        p_meter_results = self._get_latest_dp_meter_results_path()
        p_meter_results.parent.mkdir(parents=True, exist_ok=True)
        df_dp_meter_results.to_csv(p_meter_results, index=False, encoding="utf_8_sig")

    def _store_df_dp_lc_results(self, df_dp_lc_results: pd.DataFrame) -> None:
        p_lc_results = self._get_latest_dp_lc_results_path()
        p_lc_results.parent.mkdir(parents=True, exist_ok=True)
        df_dp_lc_results.to_csv(p_lc_results, index=False, encoding="utf_8_sig")

    @override(IF_DPEvaluationRepo.store)
    def store(self, dp_results: DPResults) -> None:
        self._store_df_dp_meter_results(dp_results.dump_df_dp_lc_results_with_groundtruth())
        self._store_df_dp_lc_results(dp_results.dump_df_dp_lc_results_with_groundtruth())

    @override(IF_DPEvaluationRepo.init_output)
    def init_output(self) -> None:
        self._get_latest_dp_lc_results_path().unlink(missing_ok=True)
        self._get_latest_dp_meter_results_path().unlink(missing_ok=True)
