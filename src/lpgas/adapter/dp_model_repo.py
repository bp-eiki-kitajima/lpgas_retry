import pathlib
import pickle
import shutil
from dataclasses import asdict

import pandas as pd

from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.utils.boudary.if_dp_model import IF_DPModel
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class DPModelRepo(IF_DPModelRepo):
    def __init__(
        self,
        dp_model_dir_path: str,
        dp_model_result_dirname: str,
        dp_model_filename: str,
        dp_model_tag_filename: str,
        **kwargs,
    ) -> None:
        self._dp_model_filename = dp_model_filename
        self._dp_model_tag_filename = dp_model_tag_filename
        self._dp_model_result_dirname = dp_model_result_dirname

        self._p_dp_model_dir = pathlib.Path(dp_model_dir_path)
        self._p_dp_model = self._p_dp_model_dir / dp_model_filename
        self._p_dp_model_tag = self._p_dp_model_dir / dp_model_tag_filename
        self._p_dp_model_result_dir = self._p_dp_model_dir / dp_model_result_dirname

    def _get_dp_model_dir_path(self) -> pathlib.Path:
        return self._p_dp_model_dir

    def _get_latest_model_path(self) -> pathlib.Path:
        return self._p_dp_model

    def _get_latest_model_tag_path(self) -> pathlib.Path:
        return self._p_dp_model_tag

    def _get_latest_model_result_dir_path(self) -> pathlib.Path:
        return self._p_dp_model_result_dir

    def _store_command(self, dp_model: IF_DPModel) -> None:
        command = dp_model.dump_command()

        p_model = self._get_latest_model_path()
        p_model.parent.mkdir(parents=True, exist_ok=True)
        with open(p_model, "wb") as pf:
            pickle.dump(asdict(command), file=pf)

    def _store_tag(self, dp_model: IF_DPModel) -> None:
        command = dp_model.dump_command()

        dict_tag = {
            ColumnEnum.dp_model_classname.name: type(dp_model).__name__,
            ColumnEnum.dp_model_filename.name: self._dp_model_filename,
        } | asdict(command)

        ser_tag = pd.Series(dict_tag)
        p_tag = self._get_latest_model_tag_path()
        p_tag.parent.mkdir(parents=True, exist_ok=True)
        ser_tag.to_csv(p_tag, encoding="utf_8_sig")

    def _store_result(self, dp_model: IF_DPModel) -> None:
        p_result_dir = self._get_latest_model_result_dir_path()
        p_result_dir.mkdir(parents=True, exist_ok=True)

        dp_model.dump_result_for_local(self._p_dp_model_result_dir)

    @override(IF_DPModelRepo.store)
    def store(self, dp_model: IF_DPModel) -> None:
        logger.info("DPモデル集約保存")

        # TODO 一つのモデルしか扱わない仕様になっている.
        self._store_command(dp_model)
        self._store_tag(dp_model)
        self._store_result(dp_model)

    @override(IF_DPModelRepo.load_latest)
    def load_latest(self) -> IF_DPModel:
        logger.info("最新のDPモデル読込")

        p_tag = self._get_latest_model_tag_path()
        df_tag = pd.read_csv(p_tag, index_col=0).squeeze("columns")

        dp_model_classname = df_tag[ColumnEnum.dp_model_classname.name]
        p_model = self._get_dp_model_dir_path() / df_tag[ColumnEnum.dp_model_filename.name]

        kwargs = {}
        with open(p_model, "rb") as pf:
            kwargs = pickle.load(pf)

        return DPModelFactory().gene_from_args(dp_model_classname=dp_model_classname, **kwargs)

    @override(IF_DPModelRepo.init_output)
    def init_output(self) -> None:
        logger.info("Repo初期化")

        self._get_latest_model_path().unlink(missing_ok=True)
        self._get_latest_model_tag_path().unlink(missing_ok=True)
        if self._get_latest_model_result_dir_path().exists():
            shutil.rmtree(self._get_latest_model_result_dir_path())
