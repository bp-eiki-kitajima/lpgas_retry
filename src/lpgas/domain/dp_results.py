from typing import Any, Optional, cast

import pandas as pd

from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class DPResults:
    def __init__(self, df_dp_meter_results: pd.DataFrame) -> None:
        self._df_dp_meter_results = df_dp_meter_results
        self._df_dp_meter_results_with_gt: Optional[pd.DataFrame] = None
        self._df_dp_lc_results_with_gt: Optional[pd.DataFrame] = None

    def dump_df_dp_meter_results(self) -> pd.DataFrame:
        return self._df_dp_meter_results.copy()

    def dump_df_dp_lc_results_results(self) -> pd.DataFrame:
        logger.info("メータ単位からLC単位の予測結果の作成開始")

        list_df_dp_meter_results_grouped = [
            (str_lc_id, output_term, df_grouped)
            for (str_lc_id, output_term), df_grouped in self._df_dp_meter_results.groupby(
                [ColumnEnum.str_lc_id.name, ColumnEnum.train_output_term.name]
            )
        ]

        length = len(list_df_dp_meter_results_grouped)

        list_df_dp_lc_results: list[Any] = []
        for prog_i, (str_lc_id, output_term, df_grouped) in enumerate(
            list_df_dp_meter_results_grouped
        ):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            list_str_meter_ids = df_grouped[ColumnEnum.str_meter_id.name].unique()
            str_list_meter_id = " ".join(list_str_meter_ids)

            str_predict_start_dt = df_grouped[ColumnEnum.str_predict_start_dt.name].iloc[0]
            c_predict_kg = df_grouped[ColumnEnum.c_predict_kg.name].sum()
            error_var = df_grouped[ColumnEnum.error_var.name].sum()
            str_last_change_dt = df_grouped[ColumnEnum.str_last_change_dt.name].iloc[0]

            list_df_a_dp_lc_results: list[Any] = []
            list_df_a_dp_lc_results.append(str_lc_id)
            list_df_a_dp_lc_results.append(str_list_meter_id)
            list_df_a_dp_lc_results.append(str_predict_start_dt)
            list_df_a_dp_lc_results.append(output_term)
            list_df_a_dp_lc_results.append(c_predict_kg)
            list_df_a_dp_lc_results.append(error_var)
            list_df_a_dp_lc_results.append(str_last_change_dt)
            list_df_dp_lc_results.append(list_df_a_dp_lc_results)

        df_dp_lc_results = pd.DataFrame(
            list_df_dp_lc_results,
            columns=[
                ColumnEnum.str_lc_id.name,
                ColumnEnum.list_str_meter_id.name,
                ColumnEnum.str_predict_start_dt.name,
                ColumnEnum.train_output_term.name,
                ColumnEnum.c_predict_kg.name,
                ColumnEnum.error_var.name,
                ColumnEnum.str_last_change_dt.name,
            ],
        )

        df_dp_lc_results = cast(pd.DataFrame, df_dp_lc_results)
        return df_dp_lc_results.copy()

    def attach_groundtruth(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        self._attach_groundtruth_to_df_dp_meter_results(cleansed_input_repo)
        self._attach_groundtruth_to_df_dp_lc_results(cleansed_input_repo)

    def _attach_groundtruth_to_df_dp_meter_results(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> None:
        # TODO 仮実装 実績計算する処理を追加する
        self._df_dp_meter_results_with_gt = self.dump_df_dp_meter_results()
        self._df_dp_meter_results_with_gt[ColumnEnum.c_groundtruth_kg.name] = 999
        self._df_dp_meter_results_with_gt[ColumnEnum.exist_groundtruth.name] = True

    def _attach_groundtruth_to_df_dp_lc_results(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> None:
        # TODO 仮実装 実績計算する処理を追加する
        self._df_dp_lc_results_with_gt = self.dump_df_dp_lc_results_results()
        self._df_dp_lc_results_with_gt[ColumnEnum.c_groundtruth_kg.name] = 999
        self._df_dp_lc_results_with_gt[ColumnEnum.exist_groundtruth.name] = True

    def dump_df_dp_meter_results_with_groundtruth(self) -> pd.DataFrame:
        if self._df_dp_meter_results_with_gt is None:
            raise ValueError

        return self._df_dp_meter_results_with_gt.copy()

    def dump_df_dp_lc_results_with_groundtruth(self) -> pd.DataFrame:
        if self._df_dp_lc_results_with_gt is None:
            raise ValueError

        return self._df_dp_lc_results_with_gt.copy()


class DPResultsFactory:
    def gene_dp_results(self, df_dp_meter_results: pd.DataFrame) -> DPResults:
        df_dp_meter_results = Schema_For_Domain().validate_df_dp_meter_results(df_dp_meter_results)
        return DPResults(df_dp_meter_results)

    def gene_dummy_dp_results(self) -> DPResults:
        df_dp_meter_results = pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: ["dummy_m1", "dummy_m1", "dummy_m2", "dummy_m2"],
                ColumnEnum.str_lc_id.name: ["dummy_l1", "dummy_l1", "dummy_l1", "dummy_l1"],
                ColumnEnum.str_predict_start_dt.name: [
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                ],
                ColumnEnum.train_output_term.name: [0, 1, 0, 1],
                ColumnEnum.c_predict_kg.name: [10, 20, 100, 200],
                ColumnEnum.error_var.name: [0.5, 0.5, 0.5, 0.5],
                ColumnEnum.str_last_change_dt.name: [
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                ],
            }
        )
        return self.gene_dp_results(df_dp_meter_results)
