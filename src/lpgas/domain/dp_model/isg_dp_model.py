import pathlib
from dataclasses import dataclass, replace
from typing import Optional

import pandas as pd

from lpgas.domain.dp_model.dp_model_command import ISG_DPModelCommand
from lpgas.domain.dp_model.ncu_dp_model import NCU_DPModel
from lpgas.domain.dp_model.none_ncu_dp_model import NoneNCU_DPModel
from lpgas.domain.identifier import MeterID
from lpgas.domain.meter.meter_service import MeterService
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_model import IF_DPModel
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


@dataclass
class ISG_DPModel(IF_DPModel):
    ncu_dpmodel: NCU_DPModel
    none_ncu_dpmodel: NoneNCU_DPModel
    command: ISG_DPModelCommand

    @override(IF_DPModel.train)
    def train(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        self.ncu_dpmodel.train(cleansed_input_repo)
        self.none_ncu_dpmodel.train(cleansed_input_repo)

    @override(IF_DPModel.predict)
    def predict(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: Optional[list[MeterID]],
        predict_start_dt: pd.Timestamp,
        predict_output_term: int,
    ) -> tuple[pd.DataFrame, list[MeterID]]:
        logger.info("ISGモデル予測開始")

        if list_target_meter_id is None:
            list_target_meter_id = list(cleansed_input_repo.get_dict_meter().keys())

        (
            df_dp_meter_results_for_ncu_dpmodel,
            list_meter_id_successed_for_ncu_dpmodel,
        ) = self._predict_ncu_model(
            cleansed_input_repo=cleansed_input_repo,
            list_target_meter_id=list_target_meter_id,
            predict_start_dt=predict_start_dt,
            predict_output_term=predict_output_term,
        )

        (
            df_dp_meter_results_for_none_ncu_dpmodel,
            list_meter_id_successed_for_none_ncu_dpmodel,
        ) = self._predict_none_ncu_model(
            cleansed_input_repo=cleansed_input_repo,
            list_target_meter_id=list_target_meter_id,
            list_meter_id_successed_for_ncu_dpmodel=list_meter_id_successed_for_ncu_dpmodel,
            predict_start_dt=predict_start_dt,
            predict_output_term=predict_output_term,
        )
        # 残りを実装する

        df_dp_meter_results_for_isg_dpmodel = pd.concat(
            [df_dp_meter_results_for_ncu_dpmodel, df_dp_meter_results_for_none_ncu_dpmodel]
        )

        list_meter_id_successed_for_isg_dpmodel = list(
            set(list_meter_id_successed_for_ncu_dpmodel)
            | set(list_meter_id_successed_for_none_ncu_dpmodel)
        )

        df_dp_meter_results_for_isg_dpmodel = df_dp_meter_results_for_isg_dpmodel.reset_index(
            drop=True
        )
        df_dp_meter_results_for_isg_dpmodel = Schema_For_Domain().validate_df_dp_meter_results(
            df_dp_meter_results_for_isg_dpmodel
        )
        return df_dp_meter_results_for_isg_dpmodel, list_meter_id_successed_for_isg_dpmodel

    @override(IF_DPModel.dump_command)
    def dump_command(self) -> ISG_DPModelCommand:
        command = replace(self.command)
        ncu_linear_model_command = self.ncu_dpmodel.dump_command()
        none_ncu_linear_model_command = self.none_ncu_dpmodel.dump_command()

        command.ncu_raw_model = ncu_linear_model_command.ncu_raw_model
        command.none_ncu_raw_model = none_ncu_linear_model_command.none_ncu_raw_model

        return command

    @override(IF_DPModel.dump_result_for_local)
    def dump_result_for_local(self, result_dir_path: pathlib.Path) -> None:
        self.ncu_dpmodel.dump_result_for_local(result_dir_path)
        self.none_ncu_dpmodel.dump_result_for_local(result_dir_path)

    def _predict_ncu_model(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: Optional[list[MeterID]],
        predict_start_dt: pd.Timestamp,
        predict_output_term: int,
    ) -> tuple[pd.DataFrame, list[MeterID]]:

        list_meter_id_for_ncu_dpmodel = MeterService().get_list_meter_id_with_ncu(
            cleansed_input_repo=cleansed_input_repo, list_target_meter_id=list_target_meter_id
        )

        (
            df_dp_meter_results_for_ncu_dpmodel,
            list_meter_id_successed_for_ncu_dpmodel,
        ) = self.ncu_dpmodel.predict(
            cleansed_input_repo=cleansed_input_repo,
            list_target_meter_id=list_meter_id_for_ncu_dpmodel,
            predict_start_dt=predict_start_dt,
            predict_output_term=predict_output_term,
        )

        return df_dp_meter_results_for_ncu_dpmodel, list_meter_id_successed_for_ncu_dpmodel

    def _predict_none_ncu_model(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: list[MeterID],
        list_meter_id_successed_for_ncu_dpmodel: list[MeterID],
        predict_start_dt: pd.Timestamp,
        predict_output_term: int,
    ) -> tuple[pd.DataFrame, list[MeterID]]:

        list_meter_id_for_none_ncu_dpmodel = list(
            set(list_target_meter_id) - set(list_meter_id_successed_for_ncu_dpmodel)
        )

        (
            df_dp_meter_results_for_none_ncu_dpmodel,
            list_meter_id_successed_for_none_ncu_dpmodel,
        ) = self.none_ncu_dpmodel.predict(
            cleansed_input_repo=cleansed_input_repo,
            list_target_meter_id=list_meter_id_for_none_ncu_dpmodel,
            predict_start_dt=predict_start_dt,
            predict_output_term=predict_output_term,
        )
        return (
            df_dp_meter_results_for_none_ncu_dpmodel,
            list_meter_id_successed_for_none_ncu_dpmodel,
        )
