from lpgas.domain.dp_model.isg_dp_model import ISG_DPModel, ISG_DPModelCommand
from lpgas.domain.dp_model.ncu_dp_model import NCU_DPModel, NCU_DPModelCommand
from lpgas.domain.dp_model.none_ncu_dp_model import NoneNCU_DPModel, NoneNCU_DPModelCommand
from lpgas.utils.boudary.if_dp_model import IF_DPModel


class DPModelFactory:
    def gene_from_args(self, dp_model_classname: str, **kwargs) -> IF_DPModel:
        if dp_model_classname == ISG_DPModel.__name__:
            return self._gene_isg_dpmodel_from_args(**kwargs)
        elif dp_model_classname == NCU_DPModel.__name__:
            return self._gene_ncu_dpmodel_from_args(**kwargs)
        elif dp_model_classname == NoneNCU_DPModel.__name__:
            return self._gene_none_ncu_dpmodel_from_args(**kwargs)
        else:
            raise ValueError

    # isg
    def _gene_isg_dpmodel_from_args(self, **kwargs) -> ISG_DPModel:
        isg_dpmodel_command = ISG_DPModelCommand.initialize(kwargs)
        return self._gene_isg_dpmodel(isg_dpmodel_command)

    def _gene_isg_dpmodel(self, isg_dpmodel_command: ISG_DPModelCommand) -> ISG_DPModel:
        ncu_dpmodel_command = isg_dpmodel_command.gene_ncu_dpmodel_command()
        none_ncu_dpmodel_command = isg_dpmodel_command.gene_none_ncu_dpmodel_command()

        ncu_dpmodel = self._gene_ncu_dpmodel(ncu_dpmodel_command)
        none_ncu_dpmodel = self._gene_none_ncu_dpmodel(none_ncu_dpmodel_command)

        return ISG_DPModel(
            ncu_dpmodel=ncu_dpmodel,
            none_ncu_dpmodel=none_ncu_dpmodel,
            command=isg_dpmodel_command,
        )

    # ncu
    def _gene_ncu_dpmodel_from_args(self, **kwargs) -> NCU_DPModel:
        ncu_dpmodel_command = NCU_DPModelCommand.initialize(kwargs)
        return self._gene_ncu_dpmodel(ncu_dpmodel_command)

    def _gene_ncu_dpmodel(self, ncu_dpmodel_command: NCU_DPModelCommand) -> NCU_DPModel:
        return NCU_DPModel(command=ncu_dpmodel_command)

    # none ncu
    def _gene_none_ncu_dpmodel_from_args(self, **kwargs) -> NoneNCU_DPModel:
        none_ncu_dpmodel_command = NoneNCU_DPModelCommand.initialize(kwargs)
        return self._gene_none_ncu_dpmodel(none_ncu_dpmodel_command)

    def _gene_none_ncu_dpmodel(
        self, none_ncu_dpmodel_command: NoneNCU_DPModelCommand
    ) -> NoneNCU_DPModel:
        return NoneNCU_DPModel(command=none_ncu_dpmodel_command)

    # dummy
    def gene_dummy_isg_dp_model(self) -> ISG_DPModel:
        isg_dpmodel_command = ISG_DPModelCommand(
            str_train_end_dt="2021-1-1",
            train_output_term=10,
            ncu_model_recent_term=6,
            ncu_model_week_term=4,
            ncu_model_dayofweek_term=4,
            ncu_model_train_sample_num=3,
            ncu_model_val_sample_num=2,
            none_ncu_model_recent_term=6,
            none_ncu_model_train_sample_num=3,
            none_ncu_model_val_sample_num=2,
            ncu_raw_model=None,
            none_ncu_raw_model=None,
        )

        return self._gene_isg_dpmodel(isg_dpmodel_command)

    def gene_dummy_ncu_dpmodel(self) -> NCU_DPModel:
        ncu_dpmodel_command = NCU_DPModelCommand(
            str_train_end_dt="2021-1-1",
            ncu_model_recent_term=6,
            ncu_model_week_term=4,
            ncu_model_dayofweek_term=4,
            ncu_model_train_sample_num=3,
            ncu_model_val_sample_num=2,
            train_output_term=10,
            ncu_raw_model=None,
        )

        return self._gene_ncu_dpmodel(ncu_dpmodel_command)

    def gene_dummy_none_ncu_dpmodel(self) -> NoneNCU_DPModel:
        none_ncu_dpmodel_command = NoneNCU_DPModelCommand(
            str_train_end_dt="2021-1-1",
            train_output_term=10,
            none_ncu_model_recent_term=6,
            none_ncu_model_train_sample_num=3,
            none_ncu_model_val_sample_num=2,
            none_ncu_raw_model=None,
        )

        return self._gene_none_ncu_dpmodel(none_ncu_dpmodel_command)
