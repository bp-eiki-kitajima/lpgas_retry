from dataclasses import dataclass, fields
from typing import Optional

import pandas as pd
from lightgbm import Booster

# TODO dataclass -> basemodelの方がいいかも


@dataclass
class NCU_DPModelCommand:
    # common
    str_train_end_dt: str
    train_output_term: int
    # ncu
    ncu_model_recent_term: int
    ncu_model_week_term: int
    ncu_model_dayofweek_term: int
    ncu_model_train_sample_num: int
    ncu_model_val_sample_num: int
    ncu_raw_model: Optional[Booster] = None

    @classmethod
    def initialize(cls, kwargs: dict):
        class_fields = {f.name for f in fields(cls)}
        return cls(**{k: v for k, v in kwargs.items() if k in class_fields})

    def get_train_end_dt(self) -> pd.Timestamp:
        return pd.to_datetime(self.str_train_end_dt)


@dataclass
class NoneNCU_DPModelCommand:
    # common
    str_train_end_dt: str
    train_output_term: int
    # none ncu
    none_ncu_model_recent_term: int
    none_ncu_model_train_sample_num: int
    none_ncu_model_val_sample_num: int
    none_ncu_raw_model: Optional[Booster] = None

    @classmethod
    def initialize(cls, kwargs: dict):
        class_fields = {f.name for f in fields(cls)}
        return cls(**{k: v for k, v in kwargs.items() if k in class_fields})

    def get_train_end_dt(self) -> pd.Timestamp:
        return pd.to_datetime(self.str_train_end_dt)


@dataclass
class ISG_DPModelCommand:
    # common
    str_train_end_dt: str
    train_output_term: int

    # ncu
    ncu_model_recent_term: int
    ncu_model_week_term: int
    ncu_model_dayofweek_term: int
    ncu_model_train_sample_num: int
    ncu_model_val_sample_num: int
    # none ncu
    none_ncu_model_recent_term: int
    none_ncu_model_train_sample_num: int
    none_ncu_model_val_sample_num: int

    ncu_raw_model: Optional[Booster] = None
    none_ncu_raw_model: Optional[Booster] = None

    @classmethod
    def initialize(cls, kwargs: dict):
        class_fields = {f.name for f in fields(cls)}
        return cls(**{k: v for k, v in kwargs.items() if k in class_fields})

    def gene_ncu_dpmodel_command(self) -> NCU_DPModelCommand:
        return NCU_DPModelCommand.initialize(self.__dict__)

    def gene_none_ncu_dpmodel_command(self) -> NoneNCU_DPModelCommand:
        return NoneNCU_DPModelCommand.initialize(self.__dict__)
