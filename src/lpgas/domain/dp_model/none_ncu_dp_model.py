import pathlib
from dataclasses import dataclass, field, replace
from typing import Any, Optional

import lightgbm as lgb
import pandas as pd
import plotly.express as px

from lpgas.domain.dp_model.dp_model_command import NoneNCU_DPModelCommand
from lpgas.domain.feature.none_ncu_feature_service import NoneNCU_FeatureService
from lpgas.domain.gas_density import GasDensityService
from lpgas.domain.identifier import MeterID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_model import IF_DPModel
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


@dataclass
class NoneNCU_DPModel(IF_DPModel):
    command: NoneNCU_DPModelCommand
    _df_learning_curve: pd.DataFrame = field(
        default_factory=pd.DataFrame, init=False, repr=False, compare=False
    )
    _df_feature_importance: pd.DataFrame = field(
        default_factory=pd.DataFrame, init=False, repr=False, compare=False
    )

    @override(IF_DPModel.train)
    def train(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        logger.info("非NCUモデル学習開始")

        (
            df_train,
            df_val,
            list_feature_name,
            target_name,
        ) = NoneNCU_FeatureService().gene_train_val_feature(
            cleansed_input_repo=cleansed_input_repo,
            recent_term=self.command.none_ncu_model_recent_term,
            train_sample_num=self.command.none_ncu_model_train_sample_num,
            val_sample_num=self.command.none_ncu_model_val_sample_num,
            end_dt=self.command.get_train_end_dt(),
        )

        train_data = lgb.Dataset(df_train[list_feature_name], label=df_train[target_name])
        val_data = lgb.Dataset(
            df_val[list_feature_name], label=df_val[target_name], reference=train_data
        )

        try:
            (
                none_ncu_raw_model,
                df_learning_curve,
                df_feature_importance,
            ) = self._train_none_ncu_raw_model(train_data, val_data)
        except Exception:
            logger.info("NoneNCUモデル学習失敗")
            return

        self.command.none_ncu_raw_model = none_ncu_raw_model
        self._df_learning_curve = df_learning_curve
        self._df_feature_importance = df_feature_importance

    @override(IF_DPModel.predict)
    def predict(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: Optional[list[MeterID]],
        predict_start_dt: pd.Timestamp,
        predict_output_term: int,
    ) -> tuple[pd.DataFrame, list[MeterID]]:
        logger.info("非NCUモデル予測開始")

        if self.command.none_ncu_raw_model is None:
            logger.warning("非NCUモデルが存在しないため予測失敗")
            raise ValueError

        dict_meter = cleansed_input_repo.get_dict_meter(list_target_meter_id)
        (
            dict_gas_density_attached_lc_id,
            default_gas_density,
        ) = GasDensityService().get_dict_gas_density_attached_lc_id(
            cleansed_input_repo, predict_start_dt
        )

        length = len(dict_meter)
        list_dp_meter_results = []
        list_meter_id_successed = []
        for prog_i, (meter_id, meter) in enumerate(dict_meter.items()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            try:
                gas_density = dict_gas_density_attached_lc_id[meter.lc_id]
            except KeyError:
                logger.warning(f"meter:{meter_id.id} 産気率取得失敗したため, デフォルトの産気率を用いる")
                gas_density = default_gas_density

            try:
                (
                    df_test,
                    list_feature_name,
                    target_name,
                ) = NoneNCU_FeatureService().gene_test_feature(
                    cleansed_input_repo=cleansed_input_repo,
                    recent_term=self.command.none_ncu_model_recent_term,
                    end_dt=predict_start_dt,
                    list_target_meter_id=[meter_id],
                )
            except Exception as e:
                logger.warning(f"meter:{meter_id.id} 特徴量作成に失敗したため, 予測スキップ. {e}")
                continue

            try:
                list_y_hat = self.command.none_ncu_raw_model.predict(df_test[list_feature_name])
            except Exception as e:
                logger.warning(f"meter:{meter_id.id} 予測値の出力に失敗したため, 予測スキップ. {e}")
                continue

            for i in range(predict_output_term):
                # predict_target_dt = predict_start_dt + pd.Timedelta(days=i)

                y_hat_kg = gas_density.convert_m3_to_kg(list_y_hat[0])
                error_var = y_hat_kg * 0.01  # TODO 仮実装

                list_a_dp_meter_results = []
                list_a_dp_meter_results.append(meter_id.id)
                list_a_dp_meter_results.append(meter.lc_id.id)
                list_a_dp_meter_results.append(str(predict_start_dt))
                list_a_dp_meter_results.append(i)
                list_a_dp_meter_results.append(y_hat_kg * i)
                list_a_dp_meter_results.append(error_var)
                list_a_dp_meter_results.append("1900/1/1")
                list_dp_meter_results.append(list_a_dp_meter_results)
            list_meter_id_successed.append(meter.meter_id)

        df_dp_meter_results = pd.DataFrame(
            list_dp_meter_results,
            columns=[
                ColumnEnum.str_meter_id.name,
                ColumnEnum.str_lc_id.name,
                ColumnEnum.str_predict_start_dt.name,
                ColumnEnum.train_output_term.name,
                ColumnEnum.c_predict_kg.name,
                ColumnEnum.error_var.name,
                ColumnEnum.str_last_change_dt.name,
            ],
        )

        df_dp_meter_results = Schema_For_Domain().validate_df_dp_meter_results(
            df_dp_meter_results, enable_groundtruth=False
        )

        return df_dp_meter_results, list_meter_id_successed

    @override(IF_DPModel.dump_command)
    def dump_command(self) -> NoneNCU_DPModelCommand:
        command = replace(self.command)
        return command

    @override(IF_DPModel.dump_result_for_local)
    def dump_result_for_local(self, result_dir_path: pathlib.Path) -> None:
        model = self.command.none_ncu_raw_model
        if model is None:
            logger.warning("モデルが存在しないのでスキップ")
            return

        try:
            fig = px.line(self._df_learning_curve, markers=True)
            fig.write_html(result_dir_path / "none_ncu_model_learning_curve.html")

        except Exception:
            logger.warning("学習曲線のプロット出力に失敗")

        try:
            fig = px.bar(self._df_feature_importance, barmode="group")
            fig.write_html(result_dir_path / "none_ncu_model_feature_importance.html")
        except Exception:
            logger.warning("特徴量重要度のプロット出力に失敗")

    def _train_none_ncu_raw_model(
        self, train_data: lgb.Dataset, val_data: lgb.Dataset
    ) -> tuple[lgb.Booster, pd.DataFrame, pd.DataFrame]:
        params = {"objective": "regression", "metric": "mae"}
        eval_result: Any = dict()

        none_ncu_raw_model = lgb.train(
            params,
            train_data,
            valid_sets=[train_data, val_data],
            valid_names=["train", "val"],
            num_boost_round=10000,
            callbacks=[
                lgb.callback.record_evaluation(eval_result),
                lgb.callback.early_stopping(10),
            ],
        )

        df_learning_curve = pd.DataFrame()
        df_learning_curve["train"] = eval_result["train"]["l1"]
        df_learning_curve["val"] = eval_result["val"]["l1"]

        df_feature_importance = pd.DataFrame(
            [
                none_ncu_raw_model.feature_importance("split"),
                none_ncu_raw_model.feature_importance("gain"),
            ],
            columns=none_ncu_raw_model.feature_name(),
            index=["split", "gain"],
        ).T

        return (
            none_ncu_raw_model,
            df_learning_curve,
            df_feature_importance,
        )
