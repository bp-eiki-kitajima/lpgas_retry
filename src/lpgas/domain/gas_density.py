import pandas as pd

from lpgas.domain.identifier import LC_ID
from lpgas.domain.weather.weather_data import WeatherData
from lpgas.domain.weather.weather_data_service import WeatherDataService
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


class GasDensity:
    def __init__(self, weather_data: WeatherData, target_dt: pd.Timestamp) -> None:
        self._T = weather_data.get_ave_temp(target_dt)

    def _cal_gas_densities(
        self,
    ) -> float:

        # TODO config等で外部入力するようにする
        propane = 0.99
        butane = 0.01
        vap_rate_propane = 0.49618
        vap_rate_butane = 0.35044
        press = 0.9731
        abs_T = 273

        return (
            (vap_rate_propane * propane + vap_rate_butane * butane)
            * press
            * ((abs_T + self._T - 4) / abs_T)
        )

    def convert_m3_to_kg(
        self,
        value: float,
    ) -> float:
        """m3単位のvalueをkg単位に変換する"""
        gas_density = self._cal_gas_densities()
        return round(value / gas_density, 3)


class GasDensityService:
    def get_dict_gas_density_attached_lc_id(
        self, cleansed_input_repo: IF_CleansedInputRepo, predict_start_dt: pd.Timestamp
    ) -> tuple[dict[LC_ID, GasDensity], GasDensity]:
        dict_weather_data_attached_lc_id = (
            WeatherDataService().get_dict_weather_data_attached_lc_id(cleansed_input_repo)
        )
        default_weather_data = list(dict_weather_data_attached_lc_id.values())[0]

        dict_gas_density_attached_lc_id = {
            lc_id: GasDensity(weather_data, predict_start_dt)
            for lc_id, weather_data in dict_weather_data_attached_lc_id.items()
        }
        default_gas_density = GasDensity(default_weather_data, predict_start_dt)

        return dict_gas_density_attached_lc_id, default_gas_density
