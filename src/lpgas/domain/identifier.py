from pydantic import BaseModel, Field


class MeterID(BaseModel):
    id: str = Field(..., min_length=1, max_length=20)

    class Config:
        frozen = True


class LC_ID(BaseModel):
    id: str = Field(..., min_length=1, max_length=20)

    class Config:
        frozen = True


class StaffID(BaseModel):
    id: str = Field(..., min_length=1, max_length=20)

    class Config:
        frozen = True


class WeatherCityID(BaseModel):
    id: str = Field(..., min_length=1, max_length=20)

    class Config:
        frozen = True
