from typing import cast

import numpy as np
import pandas as pd

from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID, MeterID
from lpgas.domain.lc import LC
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_preprocess_service import IF_PreprocessService
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger
from lpgas.utils.override_wrapper import override

logger = get_lpgas_logger(__name__)


class ISG_PreprocessService(IF_PreprocessService):
    def __init__(self, **args) -> None:
        pass

    @override(IF_PreprocessService.apply_and_store)
    def apply_and_store(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        logger.info("前処理開始")

        self._apply_to_delivery_result_data_and_store(cleansed_input_repo)
        self._apply_to_meter_usage_data_and_store(cleansed_input_repo)
        self._apply_to_ncu_meter_usage_data_and_store(cleansed_input_repo)

    def _apply_to_meter_usage_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> None:
        logger.info("MeterUsageDataの前処理開始")

        try:
            dict_delivery_result_data = cleansed_input_repo.get_dict_delivery_result_data()
            dict_lc = cleansed_input_repo.get_dict_lc()
            dict_meter = cleansed_input_repo.get_dict_meter()
            dict_meter_usage_data = cleansed_input_repo.get_dict_meter_usage_data()
        except FileNotFoundError:
            logger.warning("入力データの読込に失敗したためスキップ")
            return

        # TODO 仮実装　後で直す
        dict_meter_usage_data_applied = self._round_meter_usage_data(dict_meter_usage_data)
        dict_meter_usage_data_applied = self._merge_from_delivery_result_data(
            dict_meter_usage_data_applied, dict_delivery_result_data, dict_lc, dict_meter
        )
        # TODO 重複処理 追加する
        dict_meter_usage_data_applied = self._interpolate_meter_usage_data(
            dict_meter_usage_data_applied
        )
        dict_meter_usage_data_applied = self._fix_anomaly_meter_usage_data(
            dict_meter_usage_data_applied
        )

        cleansed_input_repo.store_dict_meter_usage_data(dict_meter_usage_data_applied)

    def _apply_to_ncu_meter_usage_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> None:
        """端数処理, 異常値処理"""
        logger.info("NcuMeterUsageDataの前処理開始")

        try:
            dict_ncu_meter_usage_data = cleansed_input_repo.get_dict_ncu_meter_usage_data()
        except FileNotFoundError:
            logger.warning("入力データの読込に失敗したためスキップ")
            return

        dict_ncu_meter_usage_data = self._round_ncu_meter_usage_data(dict_ncu_meter_usage_data)
        dict_ncu_meter_usage_data = self._fix_anomaly_ncu_meter_usage_data(
            dict_ncu_meter_usage_data
        )

        cleansed_input_repo.store_dict_ncu_meter_usage_data(dict_ncu_meter_usage_data)

    def _apply_to_delivery_result_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> None:
        """配送実績の前処理 + 保存"""
        logger.info("DeliveryResultDataの前処理開始")

        try:
            dict_delivery_result_data = cleansed_input_repo.get_dict_delivery_result_data()
            dict_lc = cleansed_input_repo.get_dict_lc()
        except FileNotFoundError:
            logger.warning("入力データの読込に失敗したためスキップ")
            return

        dict_delivery_result_data = self._replace_main_cylinder_remain_amount(
            dict_delivery_result_data, dict_lc
        )

        dict_delivery_result_data = self._round_delivery_result(dict_delivery_result_data)

        cleansed_input_repo.store_dict_delivery_result_data(dict_delivery_result_data)

    def _replace_main_cylinder_remain_amount(
        self, dict_delivery_result_data: dict[LC_ID, DeliveryResultData], dict_lc: dict[LC_ID, LC]
    ) -> dict[LC_ID, DeliveryResultData]:
        """予備系ないLCの主容器残ガスを置き換え
        | -    | -           | 1 | 2 | 3 | 4 |
        |------|-------------|---|---|---|---|
        | 条件 | lc集約に存在 | o | o | o | x |
        | -    | lcの予備系が0 | o | o | x | - |
        | -    | 配送時に交換 | o | x | - | - |
        | 結果 | 置換        | o | x | x | x |
        """
        logger.info("予備系ないLCの主容器残ガス置き換え処理開始")

        def replace_if_need(
            delivery_result_data: DeliveryResultData, dict_lc: dict[LC_ID, LC]
        ) -> pd.DataFrame:
            lc_id = delivery_result_data.lc_id

            # 配送実績にあるLCがLC集約に存在しない場合, スキップ
            if (lc := dict_lc.get(lc_id)) is None:
                return delivery_result_data.df_a_delivery_result_data

            # LCの予備系の容量が0でない場合, スキップ
            if lc.sub_cylinder_unit.get_sum_size() > 0:
                return delivery_result_data.df_a_delivery_result_data

            main_cylinder_sum_size = lc.main_cylinder_unit.get_sum_size()
            df = delivery_result_data.df_a_delivery_result_data
            for index, ser in df.iterrows():
                # 交換した実績の場合, 置換
                if ser[ColumnEnum.is_change.name]:
                    df.loc[
                        index, ColumnEnum.main_cylinder_remain_amount.name
                    ] = main_cylinder_sum_size
                    logger.warning(f"lc:{lc_id.id}, 予備系ないLCの主容器残ガス置き換え")
            return df

        list_df_a_delivery_result_data = []
        for lc_id, delivery_result_data in dict_delivery_result_data.items():
            df_a_delivery_result_data = replace_if_need(delivery_result_data, dict_lc)
            list_df_a_delivery_result_data.append(df_a_delivery_result_data)

        return DeliveryResultDataFactory().gene_dict_from_list_df(list_df_a_delivery_result_data)

    def _round_delivery_result(
        self, dict_delivery_result_data: dict[LC_ID, DeliveryResultData]
    ) -> dict[LC_ID, DeliveryResultData]:
        """配送実績の丸め込み"""
        logger.info("丸め込み処理開始")

        list_df_a_delivery_result_data = []
        for lc_id, delivery_result_data in dict_delivery_result_data.items():
            df_a_delivery_result_data = delivery_result_data.df_a_delivery_result_data
            df_a_delivery_result_data[ColumnEnum.accum_usage.name] = df_a_delivery_result_data[
                ColumnEnum.accum_usage.name
            ].round(1)
            list_df_a_delivery_result_data.append(df_a_delivery_result_data)

        return DeliveryResultDataFactory().gene_dict_from_list_df(list_df_a_delivery_result_data)

    def _round_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData]
    ) -> dict[MeterID, MeterUsageData]:
        """検針値の丸め込み"""
        logger.info("丸め込み処理開始")

        list_df_a_meter_usage_data = []
        for lc_id, meter_usage_data in dict_meter_usage_data.items():
            df_a_meter_usage_data = meter_usage_data.df_a_meter_usage_data
            df_a_meter_usage_data[ColumnEnum.accum_usage.name] = df_a_meter_usage_data[
                ColumnEnum.accum_usage.name
            ].round(1)
            list_df_a_meter_usage_data.append(df_a_meter_usage_data)

        return MeterUsageDataFactory().gene_dict_from_list_df(list_df_a_meter_usage_data)

    def _interpolate_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData]
    ) -> dict[MeterID, MeterUsageData]:
        """手動検針値の線形補完
        | -      | -      | 1          | 2          | 3                |
        | ------ | ------ | ---------- | ---------- | ---------------- |
        | 条件   | 前欠損 | o          | -          | -                |
        | -      | 中欠損 | -          | -          | o                |
        | -      | 後欠損 | -          | o          | -                |
        | 期待値 | 前外挿 | 前端と同値 | -          | -                |
        | -      | 中外挿 | -          | -          | 時系列で線形補間 |
        | -      | 後外挿 | -          | 後端と同値 | -                |
        """
        logger.info("線形補完処理開始")

        list_df_a_meter_usage_data = []
        # TOOD 進捗表示いれる
        for lc_id, meter_usage_data in dict_meter_usage_data.items():
            df_a_meter_usage_data = meter_usage_data.df_a_meter_usage_data
            df_a_meter_usage_data = df_a_meter_usage_data.reset_index(drop=True)

            df_work = df_a_meter_usage_data[
                [ColumnEnum.datetime.name, ColumnEnum.accum_usage.name]
            ]
            df_work = df_work.set_index(ColumnEnum.datetime.name)
            df_work[ColumnEnum.accum_usage.name] = cast(
                pd.Series,
                df_work[ColumnEnum.accum_usage.name].interpolate(
                    method="time", limit_direction="both"
                ),
            ).round(1)

            df_a_meter_usage_data[ColumnEnum.accum_usage.name] = df_work.reset_index()[
                ColumnEnum.accum_usage.name
            ]
            list_df_a_meter_usage_data.append(df_a_meter_usage_data)

        return MeterUsageDataFactory().gene_dict_from_list_df(list_df_a_meter_usage_data)

    def _round_ncu_meter_usage_data(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData]
    ) -> dict[MeterID, NcuMeterUsageData]:
        """NCU検針値の丸め込み"""
        logger.info("丸め込み処理開始")

        list_df_a_ncu_meter_usage_data = []
        for lc_id, ncu_meter_usage_data in dict_ncu_meter_usage_data.items():
            df_a_ncu_meter_usage_data = ncu_meter_usage_data.df_a_ncu_meter_usage_data
            df_a_ncu_meter_usage_data[ColumnEnum.accum_usage.name] = df_a_ncu_meter_usage_data[
                ColumnEnum.accum_usage.name
            ].round(1)
            list_df_a_ncu_meter_usage_data.append(df_a_ncu_meter_usage_data)

        return NcuMeterUsageDataFactory().gene_dict_from_list_df(list_df_a_ncu_meter_usage_data)

    def _fix_anomaly_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData]
    ) -> dict[MeterID, MeterUsageData]:
        logger.info("異常値処理開始")
        """検針値の異常値処理"""
        # TODO  仮実装
        return dict_meter_usage_data

    def _fix_anomaly_ncu_meter_usage_data(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData]
    ) -> dict[MeterID, NcuMeterUsageData]:
        """NCU検針値の異常値処理"""
        logger.info("異常値処理開始")
        # TODO  仮実装
        return dict_ncu_meter_usage_data

    def _merge_from_delivery_result_data(
        self,
        dict_meter_usage_data: dict[MeterID, MeterUsageData],
        dict_delivery_result_data: dict[LC_ID, DeliveryResultData],
        dict_lc: dict[LC_ID, LC],
        dict_meter: dict[MeterID, Meter],
    ) -> dict[MeterID, MeterUsageData]:
        """手動検針値と配送実績のマージ
        集合住宅はnanをマージ
        """
        logger.info("検針実績と配送実績のマージ処理開始")

        def _add_meter_id(
            dict_meter: dict[MeterID, Meter], df_delivery_result_data_for_merge: pd.DataFrame
        ) -> pd.DataFrame:
            """配送実績にMeter_IDを付与させる"""
            logger.info("配送実績にMeterID付与開始")

            df_meter = MeterFactory().dump_df_from_dict(dict_meter)
            df_meter = df_meter.loc[:, [ColumnEnum.str_meter_id.name, ColumnEnum.str_lc_id.name]]
            df_delivery_result_data_for_merge = pd.merge(
                df_delivery_result_data_for_merge,
                df_meter,
                how="left",
                on=[ColumnEnum.str_lc_id.name],
            )
            return df_delivery_result_data_for_merge

        def _insert_na_to_apartment(
            dict_lc: dict[LC_ID, LC],
            df_delivery_result_data_for_merge: pd.DataFrame,
        ) -> pd.DataFrame:
            """集合住宅LCの配送実績の検針値をnaに置き換える"""
            logger.info("集合住宅LCのna置換処理開始")

            set_str_lc_id_of_apartment = set(
                lc_id.id for lc_id, lc in dict_lc.items() if lc.is_apartment
            )
            idx_has_apartment = df_delivery_result_data_for_merge[ColumnEnum.str_lc_id.name].isin(
                set_str_lc_id_of_apartment
            )
            df_delivery_result_data_for_merge.loc[
                idx_has_apartment, ColumnEnum.accum_usage.name
            ] = np.nan

            return df_delivery_result_data_for_merge

        def _make_df_delivery_result_data_for_merge(
            dict_lc: dict[LC_ID, LC],
            dict_meter: dict[MeterID, Meter],
            df_meter_usage_data: pd.DataFrame,
            dict_delivery_result_data: dict[LC_ID, DeliveryResultData],
        ) -> pd.DataFrame:
            """マージ用の配送実績を作成する"""
            logger.info("マージ用の配送実績を作成開始")

            df_delivery_result_data = DeliveryResultDataFactory().dump_df_from_dict(
                dict_delivery_result_data
            )
            df_delivery_result_data_for_merge = _add_meter_id(dict_meter, df_delivery_result_data)
            df_delivery_result_data_for_merge = _insert_na_to_apartment(
                dict_lc, df_delivery_result_data_for_merge
            )
            df_delivery_result_data_for_merge[ColumnEnum.is_open.name] = np.nan
            df_delivery_result_data_for_merge = df_delivery_result_data_for_merge.loc[
                :, df_meter_usage_data.columns
            ]
            return df_delivery_result_data_for_merge

        def _make_df_meter_usage_data_merged(
            df_meter_usage_data: pd.DataFrame,
            df_delivery_result_data_for_merge: pd.DataFrame,
        ) -> pd.DataFrame:
            """検針実績と配送実績をマージする"""
            df_meter_usage_data_merged = pd.merge(
                df_meter_usage_data,
                df_delivery_result_data_for_merge,
                how="outer",
                on=list(df_delivery_result_data_for_merge.columns),
            )

            df_meter_usage_data_merged = df_meter_usage_data_merged.sort_values(
                [ColumnEnum.str_meter_id.name, ColumnEnum.datetime.name]
            ).reset_index(drop=True)
            df_meter_usage_data_merged[ColumnEnum.is_open.name] = df_meter_usage_data_merged[
                ColumnEnum.is_open.name
            ].fillna(method="ffill")
            return df_meter_usage_data_merged

        df_meter_usage_data = MeterUsageDataFactory().dump_df_from_dict(dict_meter_usage_data)
        df_delivery_result_data_for_merge = _make_df_delivery_result_data_for_merge(
            dict_lc, dict_meter, df_meter_usage_data, dict_delivery_result_data
        )

        df_meter_usage_data_merged = _make_df_meter_usage_data_merged(
            df_meter_usage_data, df_delivery_result_data_for_merge
        )

        return MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_data_merged)
