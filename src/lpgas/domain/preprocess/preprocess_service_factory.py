from lpgas.domain.preprocess.isg_preprocess_service import ISG_PreprocessService
from lpgas.utils.boudary.if_preprocess_service import IF_PreprocessService


class PreprocessServiceFactory:
    def gene_from_args(self, preprocess_service_classname: str, **kwargs) -> IF_PreprocessService:
        if preprocess_service_classname == ISG_PreprocessService.__name__:
            return self._gene_isg_preprocess_service_from_args(**kwargs)
        else:
            raise ValueError

    def _gene_isg_preprocess_service_from_args(self, **kwargs) -> ISG_PreprocessService:
        return ISG_PreprocessService(**kwargs)
