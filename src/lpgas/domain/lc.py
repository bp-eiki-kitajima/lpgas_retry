from typing import Optional, cast

import pandas as pd
from pydantic import BaseModel, ValidationError, root_validator, validator

from lpgas.domain.identifier import LC_ID, MeterID, StaffID, WeatherCityID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class Area(BaseModel):
    class Config:
        allow_mutation = False

    lat: float
    lon: float
    addr: str

    @validator("lat")
    def check_lat(cls, v: float):
        if 0 <= v <= 90:
            return v
        raise ValueError

    @validator("lon")
    def check_lon(cls, v: float):
        if 0 <= v <= 180:
            return v
        raise ValueError


class CylinderUnit(BaseModel):
    class Config:
        allow_mutation = False

    cylinder_size: float
    cylinder_num: float

    @validator("cylinder_size", "cylinder_num")
    def check_value(cls, v: float):
        if v >= 0:
            return v
        raise ValueError

    def get_sum_size(self) -> float:
        return self.cylinder_num * self.cylinder_size


class LC(BaseModel):
    class Config:
        allow_mutation = False

    lc_id: LC_ID
    list_meter_id: list[MeterID]
    area: Area
    main_cylinder_unit: CylinderUnit
    sub_cylinder_unit: CylinderUnit
    staff_id: StaffID
    weather_city_id: WeatherCityID

    # the provided default
    is_apartment: bool = False

    @root_validator
    def check_cylinder(cls, v):
        list_meter_id: list[MeterID] = v.get("list_meter_id")
        main_cylinder_unit: CylinderUnit = v.get("main_cylinder_unit")
        sub_cylinder_unit: CylinderUnit = v.get("sub_cylinder_unit")
        sum_size = main_cylinder_unit.get_sum_size() + sub_cylinder_unit.get_sum_size()

        if len(list_meter_id) <= 0:
            raise ValueError

        if main_cylinder_unit.get_sum_size() <= 0:
            raise ValueError

        if sum_size <= 0:
            raise ValueError
        return v

    @validator("is_apartment", always=True)
    def set_apartment(cls, v, values):
        list_meter_id = values.get("list_meter_id")
        if len(list_meter_id) > 1:
            return True
        else:
            return False


class LCFactory:
    def gene_from_args(
        self,
        str_lc_id: str,
        list_str_meter_id: list[str],
        lat: float,
        lon: float,
        addr: str,
        main_cylinder_size: float,
        main_cylinder_num: int,
        sub_cylinder_size: float,
        sub_cylinder_num: int,
        str_staff_id: str,
        str_weather_city_id: str,
    ) -> LC:

        lc_id = LC_ID(id=str_lc_id)
        list_meter_id = [MeterID(id=str_meter_id) for str_meter_id in list_str_meter_id]
        area = Area(lat=lat, lon=lon, addr=addr)
        main_cylinder_unit = CylinderUnit(
            cylinder_size=main_cylinder_size, cylinder_num=main_cylinder_num
        )
        sub_cylinder_unit = CylinderUnit(
            cylinder_size=sub_cylinder_size, cylinder_num=sub_cylinder_num
        )
        staff_id = StaffID(id=str_staff_id)
        weather_city_id = WeatherCityID(id=str_weather_city_id)

        return LC(
            lc_id=lc_id,
            list_meter_id=list_meter_id,
            area=area,
            main_cylinder_unit=main_cylinder_unit,
            sub_cylinder_unit=sub_cylinder_unit,
            staff_id=staff_id,
            weather_city_id=weather_city_id,
        )

    def gene_dict_from_df(self, df_lc: pd.DataFrame) -> dict[LC_ID, LC]:
        logger.info("DFからLC作成開始")

        df_lc = Schema_For_Domain().validate_df_lc(df_lc)
        length = len(df_lc)

        dict_lc: dict[LC_ID, LC] = {}
        for prog_i, (index, series) in enumerate(df_lc.iterrows()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            str_lc_id = series[ColumnEnum.str_lc_id.name]
            list_str_meter_ids = (series[ColumnEnum.list_str_meter_id.name]).split(" ")
            lat = series[ColumnEnum.lat.name]
            lon = series[ColumnEnum.lon.name]
            addr = series[ColumnEnum.addr.name]
            main_cylinder_size = series[ColumnEnum.main_cylinder_size.name]
            main_cylinder_num = series[ColumnEnum.main_cylinder_num.name]
            sub_cylinder_size = series[ColumnEnum.sub_cylinder_size.name]
            sub_cylinder_num = series[ColumnEnum.sub_cylinder_num.name]
            str_staff_id = series[ColumnEnum.str_staff_id.name]
            str_weather_city_id = series[ColumnEnum.str_weather_city_id.name]

            try:
                lc = self.gene_from_args(
                    str_lc_id=str_lc_id,
                    list_str_meter_id=list_str_meter_ids,
                    lat=lat,
                    lon=lon,
                    addr=addr,
                    main_cylinder_size=main_cylinder_size,
                    main_cylinder_num=main_cylinder_num,
                    sub_cylinder_size=sub_cylinder_size,
                    sub_cylinder_num=sub_cylinder_num,
                    str_staff_id=str_staff_id,
                    str_weather_city_id=str_weather_city_id,
                )
                dict_lc[lc.lc_id] = lc
            except ValidationError:
                logger.warning(f"lc_{str_lc_id}の生成に失敗")
                continue
        return dict_lc

    def gene_dummy(self, str_lc_id: Optional[str]) -> LC:
        if str_lc_id is None:
            str_lc_id = "dummy_l1"

        return self.gene_from_args(
            str_lc_id=str_lc_id,
            list_str_meter_id=["dummy_m1", "dummy_m2"],
            lat=30.0,
            lon=130.0,
            addr="dummy_addr",
            main_cylinder_size=30.0,
            main_cylinder_num=2,
            sub_cylinder_size=30.0,
            sub_cylinder_num=2,
            str_staff_id="1",
            str_weather_city_id="1",
        )

    def gene_dummy_dict(self, sample_num: int = 3) -> dict[LC_ID, LC]:
        dict_lc: dict[LC_ID, LC] = {}
        list_str_lc_ids = [f"dummy_l{i+1}" for i in range(sample_num)]
        for str_lc_id in list_str_lc_ids:
            dummy_lc = self.gene_dummy(str_lc_id)
            dict_lc[dummy_lc.lc_id] = dummy_lc
        return dict_lc

    def dump_df(self, lc: LC, enable_schema_validate: bool = True) -> pd.DataFrame:
        str_lc_id = lc.lc_id.id
        list_str_meter_id = [meter_id.id for meter_id in lc.list_meter_id]
        str_list_meter_id = " ".join(list_str_meter_id)
        area = lc.area
        main_cylinder_unit = lc.main_cylinder_unit
        sub_cylinder_unit = lc.sub_cylinder_unit
        str_staff_id = lc.staff_id.id
        str_weather_city_id = lc.weather_city_id.id

        df_lc = pd.DataFrame(
            [
                {
                    ColumnEnum.str_lc_id.name: str_lc_id,
                    ColumnEnum.list_str_meter_id.name: str_list_meter_id,
                    ColumnEnum.lat.name: area.lat,
                    ColumnEnum.lon.name: area.lon,
                    ColumnEnum.addr.name: area.addr,
                    ColumnEnum.main_cylinder_size.name: main_cylinder_unit.cylinder_size,
                    ColumnEnum.main_cylinder_num.name: main_cylinder_unit.cylinder_num,
                    ColumnEnum.sub_cylinder_size.name: sub_cylinder_unit.cylinder_size,
                    ColumnEnum.sub_cylinder_num.name: sub_cylinder_unit.cylinder_num,
                    ColumnEnum.str_staff_id.name: str_staff_id,
                    ColumnEnum.str_weather_city_id.name: str_weather_city_id,
                }
            ]
        )

        if enable_schema_validate:
            return Schema_For_Domain().validate_df_lc(df_lc)
        return df_lc.copy()

    def dump_df_from_dict(self, dict_lc: dict[LC_ID, LC]) -> pd.DataFrame:
        logger.info("dictからDF作成開始")

        list_df_lc = []
        for lc_id, lc in dict_lc.items():
            df_one_lc = self.dump_df(lc, False)
            list_df_lc.append(df_one_lc)
        df_lc = pd.concat(list_df_lc, axis=0, ignore_index=True)
        return Schema_For_Domain().validate_df_lc(cast(pd.DataFrame, df_lc))
