import pandas as pd
import pandera

from lpgas.utils.column_enum import ColumnEnum


class Schema_For_Domain:
    def validate_df_meter(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_meter_id.name: pandera.Column(
                    pandera.String,
                    unique=True,
                ),
                ColumnEnum.str_lc_id.name: pandera.Column(
                    pandera.String,
                ),
                ColumnEnum.enable_prediction.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_ncu.name: pandera.Column(pandera.Bool),
                ColumnEnum.template_number.name: pandera.Column(
                    pandera.String,
                ),
            },
            strict=True,
            coerce=True,
            checks=[
                pandera.Check(lambda df: df.shape[0] > 0),
            ],
        )

        df = df.dropna(how="any")
        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.str_lc_id.name])
        df = df.reset_index(drop=True)
        df = schema.validate(df)
        return df

    def validate_df_lc(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(
                    pandera.String,
                    unique=True,
                ),
                ColumnEnum.list_str_meter_id.name: pandera.Column(
                    pandera.String,
                ),
                ColumnEnum.lat.name: pandera.Column(
                    pandera.Float,
                ),
                ColumnEnum.lon.name: pandera.Column(
                    pandera.Float,
                ),
                ColumnEnum.addr.name: pandera.Column(
                    pandera.String,
                ),
                ColumnEnum.main_cylinder_size.name: pandera.Column(
                    pandera.Float,
                ),
                ColumnEnum.main_cylinder_num.name: pandera.Column(
                    pandera.Int,
                ),
                ColumnEnum.sub_cylinder_size.name: pandera.Column(
                    pandera.Float,
                ),
                ColumnEnum.sub_cylinder_num.name: pandera.Column(
                    pandera.Int,
                ),
                ColumnEnum.str_staff_id.name: pandera.Column(
                    pandera.String,
                ),
                ColumnEnum.str_weather_city_id.name: pandera.Column(
                    pandera.String,
                ),
            },
            strict=True,
            coerce=True,
            checks=[
                pandera.Check(lambda df: df.shape[0] > 0),
            ],
        )
        df = df.dropna(how="any")
        df = df.sort_values([ColumnEnum.str_lc_id.name])
        df = df.reset_index(drop=True)
        df = schema.validate(df)

        return df

    def validate_df_a_meter_usage_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int),
            columns={
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(
                    pandera.Timestamp,
                ),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.is_open.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_change.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_read.name: pandera.Column(pandera.Bool),
            },
            strict=True,
            coerce=True,
            checks=[
                pandera.Check(lambda df: df.shape[0] > 0),
            ],
        )
        df = df.dropna(
            subset=[
                ColumnEnum.str_meter_id.name,
                ColumnEnum.datetime.name,
                ColumnEnum.is_open.name,
                ColumnEnum.is_change.name,
                ColumnEnum.is_read.name,
            ],
            how="any",
        )
        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        df = schema.validate(df)
        return df

    def validate_df_meter_usage_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int),
            columns={
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(
                    pandera.Timestamp,
                ),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.is_open.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_change.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_read.name: pandera.Column(pandera.Bool),
            },
            strict=True,
            coerce=True,
            checks=pandera.Check(lambda df: df.shape[0] > 0),
        )

        df = df.dropna(
            subset=[
                ColumnEnum.str_meter_id.name,
                ColumnEnum.datetime.name,
                ColumnEnum.is_open.name,
                ColumnEnum.is_change.name,
                ColumnEnum.is_read.name,
            ],
            how="any",
        )
        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_a_ncu_meter_usage_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(
                    pandera.Timestamp,
                ),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
            },
            strict=True,
            coerce=True,
            checks=[
                pandera.Check(lambda df: df.shape[0] > 0),
            ],
        )
        df = df.dropna(
            subset=[
                ColumnEnum.str_meter_id.name,
                ColumnEnum.datetime.name,
            ],
            how="any",
        )
        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        df = schema.validate(df)
        return df

    def validate_df_ncu_meter_usage_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int),
            columns={
                ColumnEnum.str_meter_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(
                    pandera.Timestamp,
                ),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
            },
            strict=True,
            coerce=True,
            checks=pandera.Check(lambda df: df.shape[0] > 0),
        )

        df = df.dropna(
            subset=[
                ColumnEnum.str_meter_id.name,
                ColumnEnum.datetime.name,
            ],
            how="any",
        )

        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)

        return schema.validate(df)

    def validate_df_a_delivery_result_data(self, df: pd.DataFrame) -> pd.DataFrame:

        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(pandera.DateTime),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.main_cylinder_remain_amount.name: pandera.Column(
                    pandera.Float, nullable=True
                ),
                ColumnEnum.delivery_input_amount.name: pandera.Column(pandera.Float),
                ColumnEnum.is_change.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_read.name: pandera.Column(pandera.Bool),
            },
            strict=True,
            coerce=True,
            checks=[
                pandera.Check(lambda df: df.shape[0] > 0),
            ],
        )

        df = df.dropna(
            subset=[
                ColumnEnum.str_lc_id.name,
                ColumnEnum.datetime.name,
                ColumnEnum.delivery_input_amount.name,
                ColumnEnum.is_change.name,
                ColumnEnum.is_read.name,
            ],
            how="any",
        )
        df = df.sort_values([ColumnEnum.str_lc_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        df = schema.validate(df)
        return df

    def validate_df_delivery_result_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns={
                ColumnEnum.str_lc_id.name: pandera.Column(pandera.String),
                ColumnEnum.datetime.name: pandera.Column(pandera.DateTime),
                ColumnEnum.accum_usage.name: pandera.Column(pandera.Float, nullable=True),
                ColumnEnum.main_cylinder_remain_amount.name: pandera.Column(pandera.Float),
                ColumnEnum.delivery_input_amount.name: pandera.Column(pandera.Float),
                ColumnEnum.is_change.name: pandera.Column(pandera.Bool),
                ColumnEnum.is_read.name: pandera.Column(pandera.Bool),
            },
            strict=True,
            coerce=True,
            checks=pandera.Check(lambda df: df.shape[0] > 0),
        )

        df = df.dropna(
            subset=[
                ColumnEnum.str_lc_id.name,
                ColumnEnum.datetime.name,
                ColumnEnum.main_cylinder_remain_amount.name,
                ColumnEnum.delivery_input_amount.name,
                ColumnEnum.is_change.name,
                ColumnEnum.is_read.name,
            ],
            how="any",
        )
        df = df.sort_values([ColumnEnum.str_lc_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_a_weather_data(
        self, df: pd.DataFrame, enable_schema_validate: bool = True
    ) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.datetime.name: pandera.Column(pandera.DateTime),
                ColumnEnum.ave_temp.name: pandera.Column(pandera.Float),
                ColumnEnum.str_weather_city_id.name: pandera.Column(pandera.String),
            },
            coerce=True,
        )

        df = df.dropna(how="any").reset_index(drop=True)
        if enable_schema_validate:
            return schema.validate(df)
        return df

    def validate_df_weather_data(self, df: pd.DataFrame) -> pd.DataFrame:
        schema = pandera.DataFrameSchema(
            columns={
                ColumnEnum.datetime.name: pandera.Column(pandera.DateTime),
                ColumnEnum.ave_temp.name: pandera.Column(pandera.Float),
                ColumnEnum.str_weather_city_id.name: pandera.Column(pandera.String),
            },
            coerce=True,
        )

        df = df.dropna(how="any")
        df = df.sort_values([ColumnEnum.str_weather_city_id.name, ColumnEnum.datetime.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_dp_meter_results(
        self, df: pd.DataFrame, enable_groundtruth: bool = False
    ) -> pd.DataFrame:

        _columns = {
            ColumnEnum.str_meter_id.name: pandera.Column(
                pandera.String,
            ),
            ColumnEnum.str_lc_id.name: pandera.Column(
                pandera.String,
            ),
            ColumnEnum.str_predict_start_dt.name: pandera.Column(
                pandera.String,
            ),
            ColumnEnum.train_output_term.name: pandera.Column(pandera.Int),
            ColumnEnum.c_predict_kg.name: pandera.Column(pandera.Float),
            ColumnEnum.error_var.name: pandera.Column(pandera.Float),
            ColumnEnum.str_last_change_dt.name: pandera.Column(
                pandera.String,
            ),
        }
        if enable_groundtruth:
            _columns = _columns | {
                ColumnEnum.c_groundtruth_kg.name: pandera.Column(pandera.Float),
                ColumnEnum.exist_groundtruth.name: pandera.Column(pandera.Bool),
            }

        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns=_columns,
            strict=True,
            coerce=True,
            checks=pandera.Check(lambda df: df.shape[0] > 0),
        )
        df = df.sort_values([ColumnEnum.str_meter_id.name, ColumnEnum.train_output_term.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)

    def validate_df_dp_lc_results(
        self, df: pd.DataFrame, enable_groundtruth: bool = False
    ) -> pd.DataFrame:

        _columns = {
            ColumnEnum.str_lc_id.name: pandera.Column(
                pandera.String,
            ),
            ColumnEnum.list_str_meter_id.name: pandera.Column(pandera.String),
            ColumnEnum.str_predict_start_dt.name: pandera.Column(
                pandera.String,
            ),
            ColumnEnum.train_output_term.name: pandera.Column(pandera.Int),
            ColumnEnum.c_predict_kg.name: pandera.Column(pandera.Float),
            ColumnEnum.error_var.name: pandera.Column(pandera.Float),
            ColumnEnum.str_last_change_dt.name: pandera.Column(
                pandera.String,
            ),
        }
        if enable_groundtruth:
            _columns = _columns | {
                ColumnEnum.c_groundtruth_kg.name: pandera.Column(pandera.Float),
                ColumnEnum.exist_groundtruth.name: pandera.Column(pandera.Bool),
            }

        schema = pandera.DataFrameSchema(
            index=pandera.Index(pandera.Int, allow_duplicates=False),
            columns=_columns,
            strict=True,
            coerce=True,
            checks=pandera.Check(lambda df: df.shape[0] > 0),
        )

        df = df.sort_values([ColumnEnum.str_lc_id.name, ColumnEnum.train_output_term.name])
        df = df.reset_index(drop=True)
        return schema.validate(df)
