from lpgas.domain.identifier import MeterID
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.logging import get_lpgas_logger

logger = get_lpgas_logger(__name__)


class DataConsistencyService:
    """データ整合性に関する処理をするサービス"""

    def take_consistency_and_store(self, cleansed_input_repo: IF_CleansedInputRepo) -> None:
        # TODO 消すか実装するか要相談
        pass

        logger.info("集約の整合性処理開始")

        self._take_consistency_for_meter_and_store(cleansed_input_repo)
        self._take_consistency_for_meter_usage_data_and_store(cleansed_input_repo)
        self._take_consistency_for_ncu_meter_usage_data_and_store(cleansed_input_repo)
        self._take_consistency_for_delivery_result_data_and_store(cleansed_input_repo)
        self._take_consistency_for_weather_data_and_store(cleansed_input_repo)

    def _take_consistency_for_meter_and_store(self, cleansed_input_repo: IF_CleansedInputRepo):
        logger.info("Meter集約の整合性処理開始")

        all_meter_id_in_lc = self._get_all_meter_id_in_lc(cleansed_input_repo)
        dict_meter_filtered = cleansed_input_repo.get_dict_meter(all_meter_id_in_lc)
        cleansed_input_repo.store_dict_meter(dict_meter_filtered)

    def _take_consistency_for_meter_usage_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ):
        logger.info("MeterUsageData集約の整合性処理開始")

        all_meter_id_in_lc = self._get_all_meter_id_in_lc(cleansed_input_repo)
        dict_meter_usage_data_filtered = cleansed_input_repo.get_dict_meter_usage_data(
            all_meter_id_in_lc
        )
        cleansed_input_repo.store_dict_meter_usage_data(dict_meter_usage_data_filtered)

    def _take_consistency_for_ncu_meter_usage_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ):
        logger.info("NcuMeterUsageData集約の整合性処理開始")

        all_meter_id_in_lc = self._get_all_meter_id_in_lc(cleansed_input_repo)
        dict_ncu_meter_usage_data_filtered = cleansed_input_repo.get_dict_ncu_meter_usage_data(
            all_meter_id_in_lc
        )
        cleansed_input_repo.store_dict_ncu_meter_usage_data(dict_ncu_meter_usage_data_filtered)

    def _take_consistency_for_delivery_result_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ):
        logger.info("DeliveryResultData集約の整合性処理開始")

        all_lc_id = self._get_all_lc_id(cleansed_input_repo)
        dict_delivery_result_data_filtered = cleansed_input_repo.get_dict_delivery_result_data(
            all_lc_id
        )
        cleansed_input_repo.store_dict_delivery_result_data(dict_delivery_result_data_filtered)

    def _take_consistency_for_weather_data_and_store(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ):
        logger.info("WeatherData集約の整合性処理開始")

        all_weather_city_id = self._get_all_weather_city_id(cleansed_input_repo)
        dict_weather_data_filtered = cleansed_input_repo.get_dict_weather_data(all_weather_city_id)
        cleansed_input_repo.store_dict_weather_data(dict_weather_data_filtered)

    def _get_all_meter_id_in_lc(self, cleansed_input_repo: IF_CleansedInputRepo) -> list[MeterID]:
        dict_lc = cleansed_input_repo.get_dict_lc()
        all_meter_id_in_lc: list[MeterID] = []
        for lc_id, lc in dict_lc.items():
            list_meter_id = lc.list_meter_id
            all_meter_id_in_lc.extend(list_meter_id)
        return all_meter_id_in_lc

    def _get_all_lc_id(self, cleansed_input_repo: IF_CleansedInputRepo):
        dict_lc = cleansed_input_repo.get_dict_lc()
        all_lc_id = [lc_id for lc_id, lc in dict_lc.items()]
        return all_lc_id

    def _get_all_weather_city_id(self, cleansed_input_repo: IF_CleansedInputRepo):
        dict_lc = cleansed_input_repo.get_dict_lc()
        all_weather_city_id_in_lc = [lc.weather_city_id for lc_id, lc in dict_lc.items()]
        return all_weather_city_id_in_lc
