from typing import Any, Optional, cast

import pandas as pd
from pydantic import BaseModel, ValidationError, validator

from lpgas.domain.identifier import WeatherCityID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class WeatherData(BaseModel):
    """あるWeatherCityIDに紐づく全ての天候データを表す"""

    class Config:
        allow_mutation = False
        arbitrary_types_allowed = True

    weather_city_id: WeatherCityID
    df_a_weather_data: pd.DataFrame

    @validator("df_a_weather_data")
    def check_missing_date(cls, v):
        def cal_diff_day(df: pd.DataFrame) -> int:
            df_datetime = df[ColumnEnum.datetime.name].dt.date
            diff_day: int = (df_datetime.max() - df_datetime.min()).days
            return int(diff_day + 1)

        diff_day = cal_diff_day(v)
        length = v.shape[0]
        assert diff_day == length
        return v

    def get_ave_temp(self, target_dt: pd.Timestamp) -> float:
        df = self.df_a_weather_data
        df_filtered = df.loc[df[ColumnEnum.datetime.name] == target_dt, ColumnEnum.ave_temp.name]
        return df_filtered.values[0]

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, WeatherData):
            list_bool = []
            for self_kv, other_kv in zip(self.dict().items(), other.dict().items()):
                if isinstance(self_kv[1], pd.DataFrame):
                    list_bool.append(self_kv[1].equals(other_kv[1]))
                else:
                    list_bool.append(self_kv == other_kv)
            return all(list_bool)
        else:
            return self.dict() == other


class WeatherDataFactory:
    def gene_from_args(
        self,
        str_weather_city_id: str,
        df_a_weather_data: pd.DataFrame,
        enable_schema_validate: bool = True,
    ) -> WeatherData:
        df_a_weather_data = Schema_For_Domain().validate_df_a_weather_data(
            df_a_weather_data, enable_schema_validate
        )

        return WeatherData(
            weather_city_id=WeatherCityID(id=str_weather_city_id),
            df_a_weather_data=df_a_weather_data,
        )

    def gene_dict_from_df(self, df_weather_data: pd.DataFrame) -> dict[WeatherCityID, WeatherData]:
        logger.info("DFからWeatherData作成開始")

        df_weather_data = Schema_For_Domain().validate_df_weather_data(df_weather_data)

        list_df_weather_data_grouped = [
            (str_weather_city_id, df)
            for str_weather_city_id, df in df_weather_data.groupby(
                ColumnEnum.str_weather_city_id.name
            )
        ]

        length = len(list_df_weather_data_grouped)

        dict_weather_data: dict[WeatherCityID, WeatherData] = {}
        for prog_i, (str_weather_city_id, df_grouped) in enumerate(list_df_weather_data_grouped):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            try:
                weather_data = self.gene_from_args(str_weather_city_id, df_grouped, False)
                dict_weather_data[weather_data.weather_city_id] = weather_data
            except ValidationError:
                logger.warning(f"weather_city_id: {str_weather_city_id} WeatherData作成失敗")
                continue
        return dict_weather_data

    def gene_dummy(self, str_weather_city_id: Optional[str] = None) -> WeatherData:
        if str_weather_city_id is None:
            str_weather_city_id = "dummy_w1"

        df_dummy = pd.DataFrame(
            {
                ColumnEnum.str_weather_city_id.name: [
                    str_weather_city_id,
                    str_weather_city_id,
                    str_weather_city_id,
                ],
                ColumnEnum.datetime.name: [
                    pd.Timestamp(year=2020, month=1, day=1),
                    pd.Timestamp(year=2020, month=1, day=2),
                    pd.Timestamp(year=2020, month=1, day=3),
                ],
                ColumnEnum.ave_temp.name: [0.5, 0.4, 0.6],
            }
        )

        return self.gene_from_args(
            str_weather_city_id=str_weather_city_id, df_a_weather_data=df_dummy
        )

    def gene_dummy_dict(self, sample_num: int = 3) -> dict[WeatherCityID, WeatherData]:
        dict_weather_data: dict[WeatherCityID, WeatherData] = {}
        list_str_weather_city_id = [f"dummy_w{i+1}" for i in range(sample_num)]
        for str_weather_city_id in list_str_weather_city_id:
            dummy_weather_data = self.gene_dummy(str_weather_city_id)
            dict_weather_data[dummy_weather_data.weather_city_id] = dummy_weather_data
        return dict_weather_data

    def dump_df(
        self, weather_data: WeatherData, enable_schema_validate: bool = True
    ) -> pd.DataFrame:
        df = weather_data.df_a_weather_data
        if enable_schema_validate:
            return Schema_For_Domain().validate_df_a_weather_data(df, enable_schema_validate)
        return df.copy()

    def dump_df_from_dict(
        self, dict_weather_data: dict[WeatherCityID, WeatherData]
    ) -> pd.DataFrame:
        logger.info("dictからDF作成開始")

        list_df = []
        for weather_city_id, weather_data in dict_weather_data.items():
            list_df.append(self.dump_df(weather_data, False))
        df_weather_data = pd.concat(list_df, axis=0, ignore_index=True)
        return Schema_For_Domain().validate_df_weather_data(cast(pd.DataFrame, df_weather_data))
