from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.meter.meter_service import MeterService
from lpgas.domain.weather.weather_data import WeatherData
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


class WeatherDataService:
    def get_weather_data_from_lc_id(
        self, cleansed_input_repo: IF_CleansedInputRepo, lc_id: LC_ID
    ) -> WeatherData:
        lc = cleansed_input_repo.get_dict_lc()[lc_id]
        weather_city_id = lc.weather_city_id
        return cleansed_input_repo.get_dict_weather_data()[weather_city_id]

    def get_dict_weather_data_from_list_lc_id(
        self, cleansed_input_repo: IF_CleansedInputRepo, list_lc_id: list[LC_ID]
    ) -> dict[WeatherCityID, WeatherData]:
        dict_lc = cleansed_input_repo.get_dict_lc(list_lc_id)
        return cleansed_input_repo.get_dict_weather_data(
            [lc.weather_city_id for lc_id, lc in dict_lc.items()]
        )

    def get_weather_data_from_meter_id(
        self, cleansed_input_repo: IF_CleansedInputRepo, meter_id: MeterID
    ) -> WeatherData:
        meter = cleansed_input_repo.get_dict_meter()[meter_id]
        lc_id = meter.lc_id
        return self.get_weather_data_from_lc_id(
            cleansed_input_repo=cleansed_input_repo, lc_id=lc_id
        )

    def get_dict_weather_data_from_list_meter_id(
        self, cleansed_input_repo: IF_CleansedInputRepo, list_meter_id: list[MeterID]
    ) -> dict[WeatherCityID, WeatherData]:
        dict_meter = cleansed_input_repo.get_dict_meter(list_meter_id)
        list_lc_id = MeterService().get_list_lc_id_that_meter_has(
            cleansed_input_repo, list(dict_meter.keys())
        )

        return self.get_dict_weather_data_from_list_lc_id(cleansed_input_repo, list_lc_id)

    def get_dict_weather_data_attached_lc_id(
        self, cleansed_input_repo: IF_CleansedInputRepo
    ) -> dict[LC_ID, WeatherData]:

        dict_lc = cleansed_input_repo.get_dict_lc()
        dict_weather_data = cleansed_input_repo.get_dict_weather_data()

        dict_weather_data_attached_lc_id: dict[LC_ID, WeatherData] = {}
        for lc_id, lc in dict_lc.items():
            weather_city_id = lc.weather_city_id
            try:
                weather_data = dict_weather_data[weather_city_id]
            except KeyError:
                # TODO log追加
                continue
            dict_weather_data_attached_lc_id[lc_id] = weather_data

        return dict_weather_data_attached_lc_id
