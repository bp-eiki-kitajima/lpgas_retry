from typing import Optional, cast

import pandas as pd
from pydantic import ValidationError

from lpgas.domain.identifier import MeterID
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class NcuMeterUsageDataFactory:
    def gene_from_args(
        self,
        str_meter_id: str,
        df_a_ncu_meter_usage_data: pd.DataFrame,
        enable_schema_validate=True,
    ) -> NcuMeterUsageData:

        if enable_schema_validate:
            df_a_ncu_meter_usage_data = Schema_For_Domain().validate_df_a_ncu_meter_usage_data(
                df_a_ncu_meter_usage_data
            )

        return NcuMeterUsageData(
            meter_id=MeterID(id=str_meter_id),
            df_a_ncu_meter_usage_data=df_a_ncu_meter_usage_data,
        )

    def gene_dict_from_df(
        self, df_ncu_meter_usage_data: pd.DataFrame
    ) -> dict[MeterID, NcuMeterUsageData]:
        logger.info("DFからNcuMeterUsageData作成開始")

        df_ncu_meter_usage_data = Schema_For_Domain().validate_df_ncu_meter_usage_data(
            df_ncu_meter_usage_data
        )

        list_df_ncu_meter_usage_value_grouped = [
            (str_meter_id, df)
            for str_meter_id, df in df_ncu_meter_usage_data.groupby(ColumnEnum.str_meter_id.name)
        ]

        length = len(list_df_ncu_meter_usage_value_grouped)

        dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData] = {}
        for prog_i, (str_meter_id, df_grouped) in enumerate(list_df_ncu_meter_usage_value_grouped):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            try:
                ncu_meter_usage_data = self.gene_from_args(str_meter_id, df_grouped, False)
                dict_ncu_meter_usage_data[ncu_meter_usage_data.meter_id] = ncu_meter_usage_data
            except ValidationError:
                logger.warning(f"meter:{str_meter_id} NcuMeterUsageData作成失敗")
                continue
        return dict_ncu_meter_usage_data

    def gene_dict_from_list_df(
        self, list_df_ncu_meter_usage_data: list[pd.DataFrame]
    ) -> dict[MeterID, NcuMeterUsageData]:
        df_ncu_meter_usage_data = cast(
            pd.DataFrame, pd.concat(list_df_ncu_meter_usage_data, ignore_index=True)
        )
        df_ncu_meter_usage_data = Schema_For_Domain().validate_df_ncu_meter_usage_data(
            df_ncu_meter_usage_data
        )
        return NcuMeterUsageDataFactory().gene_dict_from_df(df_ncu_meter_usage_data)

    def gene_dummy(
        self,
        str_meter_id: Optional[str] = None,
        reading_num: int = 10,
        start_dt: pd.Timestamp = cast(
            pd.Timestamp, pd.Timestamp(year=2020, month=1, day=1, hour=0)
        ),
    ) -> NcuMeterUsageData:
        if str_meter_id is None:
            str_meter_id = "dummy_m1"

        df_a_dummy = pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [str_meter_id for _ in range(reading_num)],
                ColumnEnum.datetime.name: [
                    start_dt + cast(pd.Timedelta, pd.Timedelta(days=i)) for i in range(reading_num)
                ],
                ColumnEnum.accum_usage.name: [10.0 + i for i in range(reading_num)],
            }
        )

        return self.gene_from_args(str_meter_id=str_meter_id, df_a_ncu_meter_usage_data=df_a_dummy)

    def gene_dummy_dict(
        self,
        meter_num: int = 3,
        reading_num: int = 10,
        start_dt: pd.Timestamp = cast(
            pd.Timestamp, pd.Timestamp(year=2020, month=1, day=1, hour=0)
        ),
    ) -> dict[MeterID, NcuMeterUsageData]:
        dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData] = {}
        list_str_meter_ids = [f"dummy_m{i+1}" for i in range(meter_num)]
        for str_meter_id in list_str_meter_ids:
            dummy_meter = self.gene_dummy(str_meter_id, reading_num, start_dt)
            dict_ncu_meter_usage_data[dummy_meter.meter_id] = dummy_meter
        return dict_ncu_meter_usage_data

    def dump_df(
        self, ncu_meter_usage_data: NcuMeterUsageData, enable_schema_validate: bool = True
    ) -> pd.DataFrame:
        df = ncu_meter_usage_data.df_a_ncu_meter_usage_data
        if enable_schema_validate:
            return Schema_For_Domain().validate_df_a_ncu_meter_usage_data(
                ncu_meter_usage_data.df_a_ncu_meter_usage_data
            )
        return df.copy()

    def dump_df_from_dict(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData]
    ) -> pd.DataFrame:
        logger.info("dictからDF作成開始")

        list_df = []
        for meter_id, ncu_meter_usage_data in dict_ncu_meter_usage_data.items():
            list_df.append(self.dump_df(ncu_meter_usage_data, False))
        df_ncu_meter_usage_data = pd.concat(list_df, axis=0, ignore_index=True)
        return Schema_For_Domain().validate_df_ncu_meter_usage_data(
            cast(pd.DataFrame, df_ncu_meter_usage_data)
        )
