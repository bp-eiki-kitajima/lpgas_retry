from typing import Any, cast

import pandas as pd
from pydantic import BaseModel, validator

from lpgas.domain.identifier import MeterID
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger

logger = get_lpgas_logger(__name__)


class NcuMeterUsageData(BaseModel):
    """あるMeter_IDに紐づく全てのNcu検針値を表す"""

    class Config:
        allow_mutation = False
        arbitrary_types_allowed = True

    meter_id: MeterID
    df_a_ncu_meter_usage_data: pd.DataFrame

    # post init
    start_dt: pd.Timestamp = cast(pd.Timestamp, pd.Timestamp(1900, 1, 1))
    end_dt: pd.Timestamp = cast(pd.Timestamp, pd.Timestamp(1900, 1, 1))

    @validator("start_dt", always=True)
    def set_start_dt(cls, v, values):
        df_a_ncu_meter_usage_data = values.get("df_a_ncu_meter_usage_data")
        return df_a_ncu_meter_usage_data[ColumnEnum.datetime.name].min()

    @validator("end_dt", always=True)
    def set_end_dt(cls, v, values):
        df_a_ncu_meter_usage_data = values.get("df_a_ncu_meter_usage_data")
        return df_a_ncu_meter_usage_data[ColumnEnum.datetime.name].max()

    def is_target_dt_within_range(self, target_dt: pd.Timestamp) -> bool:
        if self.start_dt <= target_dt <= self.end_dt:
            return True
        return False

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, NcuMeterUsageData):
            list_bool = []
            for self_kv, other_kv in zip(self.dict().items(), other.dict().items()):
                if isinstance(self_kv[1], pd.DataFrame):
                    list_bool.append(self_kv[1].equals(other_kv[1]))
                else:
                    list_bool.append(self_kv == other_kv)
            return all(list_bool)
        else:
            return self.dict() == other
