from typing import Any, Optional, cast

import pandas as pd
from pydantic import BaseModel, ValidationError

from lpgas.domain.identifier import LC_ID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class DeliveryResultData(BaseModel):
    """あるLC_IDに紐づく全ての配送実績を表す"""

    class Config:
        allow_mutation = False
        arbitrary_types_allowed = True

    lc_id: LC_ID
    df_a_delivery_result_data: pd.DataFrame

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, DeliveryResultData):
            list_bool = []
            for self_kv, other_kv in zip(self.dict().items(), other.dict().items()):
                if isinstance(self_kv[1], pd.DataFrame):
                    list_bool.append(self_kv[1].equals(other_kv[1]))
                else:
                    list_bool.append(self_kv == other_kv)
            return all(list_bool)
        else:
            return self.dict() == other


class DeliveryResultDataFactory:
    def gene_from_args(
        self,
        str_lc_id: str,
        df_a_delivery_result_data: pd.DataFrame,
        enable_validate: bool = True,
    ) -> DeliveryResultData:
        if enable_validate:
            df_a_delivery_result_data = Schema_For_Domain().validate_df_a_delivery_result_data(
                df_a_delivery_result_data
            )

        return DeliveryResultData(
            lc_id=LC_ID(id=str_lc_id), df_a_delivery_result_data=df_a_delivery_result_data
        )

    def gene_dict_from_df(
        self, df_delivery_result_data: pd.DataFrame
    ) -> dict[LC_ID, DeliveryResultData]:
        df_delivery_result_data = Schema_For_Domain().validate_df_delivery_result_data(
            df_delivery_result_data
        )

        list_df_delivery_result_grouped = [
            (str_lc_id, df)
            for str_lc_id, df in df_delivery_result_data.groupby(ColumnEnum.str_lc_id.name)
        ]

        length = len(list_df_delivery_result_grouped)
        dict_delivery_result_data: dict[LC_ID, DeliveryResultData] = {}
        for prog_i, (str_lc_id, grouped) in enumerate(list_df_delivery_result_grouped):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            try:
                delivery_result_data = DeliveryResultDataFactory().gene_from_args(
                    str_lc_id=str_lc_id,
                    df_a_delivery_result_data=grouped,
                    enable_validate=False,
                )
                dict_delivery_result_data[delivery_result_data.lc_id] = delivery_result_data
            except ValidationError:
                continue

        return dict_delivery_result_data

    def gene_dict_from_list_df(
        self, list_df_a_delivery_result_data: list[pd.DataFrame]
    ) -> dict[LC_ID, DeliveryResultData]:
        df_delivery_result_data = cast(
            pd.DataFrame, pd.concat(list_df_a_delivery_result_data, ignore_index=True)
        )
        df_delivery_result_data = Schema_For_Domain().validate_df_delivery_result_data(
            df_delivery_result_data
        )
        return DeliveryResultDataFactory().gene_dict_from_df(df_delivery_result_data)

    def gene_dummy(self, str_lc_id: Optional[str] = None):
        if str_lc_id is None:
            str_lc_id = "dummy_l1"

        df_a_dummy = pd.DataFrame(
            {
                ColumnEnum.str_lc_id.name: [str_lc_id, str_lc_id, str_lc_id],
                ColumnEnum.datetime.name: [
                    pd.Timestamp(year=2019, month=1, day=1, hour=9),
                    pd.Timestamp(year=2019, month=2, day=1, hour=9),
                    pd.Timestamp(year=2019, month=3, day=1, hour=9),
                ],
                ColumnEnum.accum_usage.name: [100.0, 110.0, 120.0],
                ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0],
                ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0],
                ColumnEnum.is_change.name: [True, True, True],
                ColumnEnum.is_read.name: [True, True, True],
            },
        )

        return self.gene_from_args(str_lc_id=str_lc_id, df_a_delivery_result_data=df_a_dummy)

    def gene_dummy_dict(self, sample_num: int = 3) -> dict[LC_ID, DeliveryResultData]:
        dict_delivery_result_data: dict[LC_ID, DeliveryResultData] = {}
        list_str_lc_id = [f"dummy_l{i+1}" for i in range(sample_num)]
        for str_lc_id in list_str_lc_id:
            dummy_delivery_result_data = self.gene_dummy(str_lc_id)
            dict_delivery_result_data[
                dummy_delivery_result_data.lc_id
            ] = dummy_delivery_result_data

        return dict_delivery_result_data

    def dump_df(
        self, delivery_result_data: DeliveryResultData, enable_schema_validate: bool = True
    ) -> pd.DataFrame:
        df = delivery_result_data.df_a_delivery_result_data
        if enable_schema_validate:
            return Schema_For_Domain().validate_df_a_delivery_result_data(df)
        return df.copy()

    def dump_df_from_dict(
        self, dict_delivery_result_data: dict[LC_ID, DeliveryResultData]
    ) -> pd.DataFrame:

        list_df = []
        for lc_id, delivery_result_data in dict_delivery_result_data.items():
            list_df.append(self.dump_df(delivery_result_data, False))
        df_delivery_result_data = pd.concat(list_df, axis=0, ignore_index=True)
        return Schema_For_Domain().validate_df_delivery_result_data(
            cast(pd.DataFrame, df_delivery_result_data)
        )
