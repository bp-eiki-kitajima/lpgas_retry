from typing import Optional, cast

import pandas as pd
from pydantic import BaseModel, Field, ValidationError

from lpgas.domain.identifier import LC_ID, MeterID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class Meter(BaseModel):
    class Config:
        allow_mutation = False

    meter_id: MeterID
    lc_id: LC_ID
    enable_prediction: bool
    is_ncu: bool
    template_number: str = Field(..., min_length=1, max_length=20)


class MeterFactory:
    def gene_from_args(
        self,
        str_meter_id: str,
        str_lc_id: str,
        enable_prediction: bool,
        is_ncu: bool,
        template_number: str,
    ) -> Meter:

        return Meter(
            meter_id=MeterID(id=str_meter_id),
            lc_id=LC_ID(id=str_lc_id),
            enable_prediction=enable_prediction,
            is_ncu=is_ncu,
            template_number=template_number,
        )

    def gene_dict_from_df(self, df_meter: pd.DataFrame) -> dict[MeterID, Meter]:
        logger.info("DFからMeter作成開始")

        df_meter = Schema_For_Domain().validate_df_meter(df_meter)
        length = len(df_meter)

        dict_meter: dict[MeterID, Meter] = {}
        for prog_i, (index, ser_row) in enumerate(df_meter.iterrows()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            try:
                meter = self.gene_from_args(
                    str_meter_id=ser_row[ColumnEnum.str_meter_id.name],
                    str_lc_id=ser_row[ColumnEnum.str_lc_id.name],
                    enable_prediction=ser_row[ColumnEnum.enable_prediction.name],
                    is_ncu=ser_row[ColumnEnum.is_ncu.name],
                    template_number=ser_row[ColumnEnum.template_number.name],
                )
                dict_meter[meter.meter_id] = meter
            except ValidationError:
                continue
        return dict_meter

    def gene_dummy(self, str_meter_id: Optional[str] = None) -> Meter:
        if str_meter_id is None:
            str_meter_id = "dummy_m1"
        return self.gene_from_args(
            str_meter_id=str_meter_id,
            str_lc_id="dummy_l1",
            enable_prediction=True,
            is_ncu=True,
            template_number="dummy",
        )

    def gene_dummy_dict(self, sample_num: int = 3) -> dict[MeterID, Meter]:
        dict_meter = {}
        list_str_meter_ids = [f"dummy_m{i+1}" for i in range(sample_num)]
        for str_meter_id in list_str_meter_ids:
            dummy_meter = self.gene_dummy(str_meter_id)
            dict_meter[dummy_meter.meter_id] = dummy_meter
        return dict_meter

    def dump_df(self, meter: Meter, enable_schema_validate: bool = True) -> pd.DataFrame:
        str_meter_id = meter.meter_id.id
        str_lc_id = meter.lc_id.id
        enable_prediction = meter.enable_prediction
        is_ncu = meter.is_ncu
        template_number = meter.template_number

        df_meter = pd.DataFrame(
            [
                {
                    ColumnEnum.str_meter_id.name: str_meter_id,
                    ColumnEnum.str_lc_id.name: str_lc_id,
                    ColumnEnum.enable_prediction.name: enable_prediction,
                    ColumnEnum.is_ncu.name: is_ncu,
                    ColumnEnum.template_number.name: template_number,
                }
            ]
        )

        if enable_schema_validate:
            return Schema_For_Domain().validate_df_meter(df_meter)
        return df_meter.copy()

    def dump_df_from_dict(self, dict_meter: dict[MeterID, Meter]) -> pd.DataFrame:
        logger.info("dictからDF作成開始")
        list_df_meter = []
        for meter_id, meter in dict_meter.items():
            df_one_meter = self.dump_df(meter, False)
            list_df_meter.append(df_one_meter)
        df_meter = pd.concat(list_df_meter, axis=0, ignore_index=True)
        return Schema_For_Domain().validate_df_meter(cast(pd.DataFrame, df_meter))
