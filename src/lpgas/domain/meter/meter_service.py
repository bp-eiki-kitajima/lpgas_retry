from typing import Optional

from lpgas.domain.identifier import LC_ID, MeterID
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


class MeterService:
    def get_list_meter_id_with_ncu(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_target_meter_id: Optional[list[MeterID]] = None,
    ) -> list[MeterID]:
        """対象メータIDのうち, NCU属性持つメータのIDのリストを返す"""
        dict_meter = cleansed_input_repo.get_dict_meter(list_target_meter_id)
        return [meter_id for meter_id, meter in dict_meter.items() if meter.is_ncu]

    def get_list_lc_id_that_meter_has(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        list_meter_id: list[MeterID],
    ) -> list[LC_ID]:
        list_lc_id: list[LC_ID] = []

        dict_meter = cleansed_input_repo.get_dict_meter(list_meter_id)
        for meter_id, meter in dict_meter.items():
            lc_id = meter.lc_id
            list_lc_id.append(lc_id)

        return list_lc_id
