from typing import Any, Optional, cast

import pandas as pd

from lpgas.domain.feature.feature_util import FeatureUtil
from lpgas.domain.identifier import MeterID
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class NoneNCU_FeatureService:
    # TODO train valにする
    def gene_train_val_feature(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        recent_term: int,
        train_sample_num: int,
        val_sample_num: int,
        end_dt: pd.Timestamp,
        list_target_meter_id: Optional[list[MeterID]] = None,
    ) -> tuple[pd.DataFrame, pd.DataFrame, list[str], str]:

        dict_meter_usage_data = cleansed_input_repo.get_dict_meter_usage_data(list_target_meter_id)
        if len(dict_meter_usage_data) == 0:
            raise ValueError("対象メータの検針データが存在しない")

        dict_meter_usage_data = self._filter_dict_meter_usage_data(dict_meter_usage_data, end_dt)
        if len(dict_meter_usage_data) == 0:
            raise ValueError(f"対象メータの{str(end_dt.date())}以前の検針データが存在しない")

        df_train, df_val = self._make_df_train_val(
            dict_meter_usage_data=dict_meter_usage_data,
            recent_term=recent_term,
            end_dt=end_dt,
            train_sample_num=train_sample_num,
            val_sample_num=val_sample_num,
        )

        list_feature_name = [
            name
            for name in df_train.columns.to_list()
            if name not in [ColumnEnum.str_meter_id.name, ColumnEnum.f_target.name]
        ]
        target_name = ColumnEnum.f_target.name

        return df_train, df_val, list_feature_name, target_name

    def gene_test_feature(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        recent_term: int,
        end_dt: pd.Timestamp,
        list_target_meter_id: Optional[list[MeterID]] = None,
    ) -> tuple[pd.DataFrame, list[str], str]:

        dict_meter_usage_data = cleansed_input_repo.get_dict_meter_usage_data(list_target_meter_id)
        if len(dict_meter_usage_data) == 0:
            raise ValueError("対象メータの検針データが存在しない")

        dict_meter_usage_data = self._filter_dict_meter_usage_data(dict_meter_usage_data, end_dt)
        if len(dict_meter_usage_data) == 0:
            raise ValueError(f"対象メータの{str(end_dt.date())}以前の検針データが存在しない")

        df_test = self._make_df_test(
            dict_meter_usage_data=dict_meter_usage_data,
            recent_term=recent_term,
            end_dt=end_dt,
        )

        list_feature_name = [
            name
            for name in df_test.columns.to_list()
            if name not in [ColumnEnum.str_meter_id.name, ColumnEnum.f_target.name]
        ]
        target_name = ColumnEnum.f_target.name

        return df_test, list_feature_name, target_name

    def _make_df_train_val(
        self,
        dict_meter_usage_data: dict[MeterID, MeterUsageData],
        recent_term: int,
        train_sample_num: int,
        val_sample_num: int,
        end_dt: pd.Timestamp,
    ) -> tuple[pd.DataFrame, pd.DataFrame]:

        dict_df_with_diff = self._make_dict_df_with_diff(dict_meter_usage_data, end_dt)

        list_df_train: list[pd.DataFrame] = []
        list_df_val: list[pd.DataFrame] = []

        for i in range(val_sample_num):
            df_a_val = self._make_df_Xy(
                dict_df_with_diff=dict_df_with_diff, repeat_num=recent_term, end_index=-1 * i
            )
            list_df_val.append(df_a_val)
        df_val = pd.concat(list_df_val, axis=0)

        for i in range(train_sample_num):
            df_a_train = self._make_df_Xy(
                dict_df_with_diff=dict_df_with_diff,
                repeat_num=recent_term,
                end_index=-1 * (i + val_sample_num),
            )
            list_df_train.append(df_a_train)
        df_train = pd.concat(list_df_train, axis=0)
        return df_train, df_val

    def _make_df_test(
        self,
        dict_meter_usage_data: dict[MeterID, MeterUsageData],
        recent_term: int,
        end_dt: pd.Timestamp,
    ) -> pd.DataFrame:
        assert len(dict_meter_usage_data) > 0

        dict_df_with_diff = self._make_dict_df_with_diff(dict_meter_usage_data, end_dt)
        df_test = self._make_df_Xy(
            dict_df_with_diff=dict_df_with_diff, repeat_num=recent_term, end_index=0
        )
        return df_test

    def _make_df_Xy(
        self, dict_df_with_diff: dict[MeterID, pd.DataFrame], repeat_num: int, end_index: int
    ):
        """特徴量と目的変数のセットを生成

        Parameters
        ----------
        dict_df_with_diff
            差分計算したDFのdict
        repeat_num
            特徴量生成の繰り返し回数 (3列/回)
        end_index
            特徴量生成に用いるindexの終点 (終点から数えたindex)

        Returns
        -------
            特徴量のDF
        """

        list_str_meter_id = []
        list_diff_day_feature = []
        list_diff_usage_feature = []
        list_daily_usage_feature = []
        list_target = []
        length = len(dict_df_with_diff)
        for prog_i, (meter_id, df_with_diff) in enumerate(dict_df_with_diff.items()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            df_with_diff_clipped = df_with_diff
            if end_index != 0:
                df_with_diff_clipped = cast(pd.DataFrame, df_with_diff[:end_index])

            if df_with_diff_clipped.shape[0] <= 0:
                logger.warning(f"meter_id:{meter_id.id} DFが空.")
                continue

            try:
                list_a_diff_day_feature = self._make_list_a_diff_day_feature(
                    df_with_diff_clipped=df_with_diff_clipped, repeat_num=repeat_num
                )

                list_a_diff_usage_feature = self._make_list_a_diff_usage_feature(
                    df_with_diff_clipped=df_with_diff_clipped, repeat_num=repeat_num
                )

                list_a_daily_usage_feature = self._make_list_a_daily_usage_feature(
                    df_with_diff_clipped=df_with_diff_clipped, repeat_num=repeat_num
                )

                list_a_target = self._make_list_a_target(df_with_diff_clipped=df_with_diff_clipped)
            except ValueError as e:
                logger.warning(f"meter_id:{meter_id.id}, {e}")
                continue

            list_str_meter_id.append(meter_id.id)
            list_diff_day_feature.append(list_a_diff_day_feature)
            list_diff_usage_feature.append(list_a_diff_usage_feature)
            list_daily_usage_feature.append(list_a_daily_usage_feature)
            list_target.append(list_a_target)

        if len(list_str_meter_id) == 0:
            raise ValueError

        df_Xy = self._convert_list_feature_to_df_Xy(
            list_str_meter_id=list_str_meter_id,
            list_diff_day_feature=list_diff_day_feature,
            list_diff_usage_feature=list_diff_usage_feature,
            list_daily_usage_feature=list_daily_usage_feature,
            list_target=list_target,
            repeat_num=repeat_num,
        )

        return df_Xy

    def _make_dict_df_with_diff(
        self,
        dict_meter_usage_data: dict[MeterID, MeterUsageData],
        end_dt: pd.Timestamp,
    ) -> dict[MeterID, pd.DataFrame]:

        dict_df_with_diff: dict[MeterID, pd.DataFrame] = {}

        length = len(dict_meter_usage_data)
        for prog_i, (meter_id, meter_usage_data) in enumerate(dict_meter_usage_data.items()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            df = MeterUsageDataFactory().dump_df(meter_usage_data, False)
            df = FeatureUtil().mask_df_by_time_range(df, end_dt=end_dt)
            df_with_diff = FeatureUtil().attached_diff_to_df(df)
            dict_df_with_diff[meter_id] = df_with_diff
        return dict_df_with_diff

    def _make_list_a_diff_day_feature(
        self, df_with_diff_clipped: pd.DataFrame, repeat_num: int
    ) -> list[Any]:
        assert repeat_num >= 1

        ser_diff_day_num = df_with_diff_clipped[ColumnEnum.diff_day_num.name][-1 * repeat_num :]
        list_a_diff_day_feature = list(ser_diff_day_num.values)

        if len(list_a_diff_day_feature) != repeat_num:
            raise ValueError("diff_day特徴量生成に必要な検針値が足りない. または重複している.")

        return list_a_diff_day_feature

    def _make_list_a_diff_usage_feature(
        self, df_with_diff_clipped: pd.DataFrame, repeat_num: int
    ) -> list[Any]:
        assert repeat_num >= 1

        ser_diff_usage = df_with_diff_clipped[ColumnEnum.diff_usage.name][-1 * repeat_num :]
        list_a_diff_usage_feature = list(ser_diff_usage.values)

        if len(list_a_diff_usage_feature) != repeat_num:
            raise ValueError("diff_usage特徴量生成に必要な検針値が足りない. または重複している.")

        return list_a_diff_usage_feature

    def _make_list_a_daily_usage_feature(
        self, df_with_diff_clipped: pd.DataFrame, repeat_num: int
    ) -> list[Any]:
        assert repeat_num >= 1

        ser_daily_usage = df_with_diff_clipped[ColumnEnum.daily_usage.name][-1 * repeat_num : -1]
        list_a_daily_usage_feature = list(ser_daily_usage.values)

        if len(list_a_daily_usage_feature) != repeat_num - 1:
            raise ValueError("daily_usage特徴量生成に必要な検針値が足りない. または重複している.")

        return list_a_daily_usage_feature

    def _make_list_a_target(self, df_with_diff_clipped: pd.DataFrame) -> list[Any]:

        ser_target = df_with_diff_clipped[ColumnEnum.daily_usage.name][-1:]
        list_a_target = list(ser_target.values)

        if len(list_a_target) != 1:
            raise ValueError("目的変数に必要な検針値が足りない. または重複している.")
        return list_a_target

    def _convert_list_diff_day_feature_to_df(
        self, list_diff_day_feature: list[Any], repeat_num: int
    ) -> pd.DataFrame:
        _columns = [f"{ColumnEnum.f_nonencu_diff_day.name}_{num}" for num in range(repeat_num)]

        df_diff_day_feature = pd.DataFrame(
            list_diff_day_feature,
            columns=_columns,
        )
        return df_diff_day_feature

    def _convert_list_diff_usage_feature_to_df(
        self, list_diff_usage_feature: list[Any], repeat_num: int
    ) -> pd.DataFrame:
        _columns = [f"{ColumnEnum.f_nonencu_diff_usage.name}_{num}" for num in range(repeat_num)]

        df_diff_usage_feature = pd.DataFrame(
            list_diff_usage_feature,
            columns=_columns,
        )
        return df_diff_usage_feature

    def _convert_list_daily_usage_feature_to_df(
        self, list_daily_usage_feature: list[Any], repeat_num: int
    ) -> pd.DataFrame:
        _columns = [
            f"{ColumnEnum.f_nonencu_daily_usage.name}_{num}" for num in range(repeat_num - 1)
        ]

        df_daily_usage_feature = pd.DataFrame(
            list_daily_usage_feature,
            columns=_columns,
        )
        return df_daily_usage_feature

    def _convert_list_target_to_df(self, list_target: list[Any]) -> pd.DataFrame:
        _columns = [ColumnEnum.f_target.name]

        df_target = pd.DataFrame(
            list_target,
            columns=_columns,
        )
        return df_target

    def _convert_list_feature_to_df_Xy(
        self,
        list_str_meter_id: list[str],
        list_diff_day_feature: list[Any],
        list_diff_usage_feature: list[Any],
        list_daily_usage_feature: list[Any],
        list_target: list[Any],
        repeat_num: int,
    ) -> pd.DataFrame:

        df_diff_day_feature = self._convert_list_diff_day_feature_to_df(
            list_diff_day_feature=list_diff_day_feature, repeat_num=repeat_num
        )
        df_diff_usage_feature = self._convert_list_diff_usage_feature_to_df(
            list_diff_usage_feature=list_diff_usage_feature, repeat_num=repeat_num
        )
        df_daily_usage_feature = self._convert_list_daily_usage_feature_to_df(
            list_daily_usage_feature=list_daily_usage_feature, repeat_num=repeat_num
        )
        df_y = self._convert_list_target_to_df(list_target=list_target)

        df_Xy = pd.concat(
            [df_diff_day_feature, df_diff_usage_feature, df_daily_usage_feature, df_y], axis=1
        )
        df_Xy[ColumnEnum.str_meter_id.name] = list_str_meter_id

        return df_Xy

    def _filter_dict_meter_usage_data(
        self, dict_meter_usage_data: dict[MeterID, MeterUsageData], end_dt: pd.Timestamp
    ) -> dict[MeterID, MeterUsageData]:
        dict_meter_usage_data_filtered = {}
        for meter_id, meter_usage_data in dict_meter_usage_data.items():
            if end_dt < meter_usage_data.start_dt:
                continue
            dict_meter_usage_data_filtered[meter_id] = meter_usage_data
        return dict_meter_usage_data_filtered
