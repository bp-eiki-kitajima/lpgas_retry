from typing import Optional, cast

import pandas as pd

from lpgas.utils.column_enum import ColumnEnum


class FeatureUtil:
    def attached_diff_to_df(self, df: pd.DataFrame) -> pd.DataFrame:
        """差分を計算してdfに追加する
        | -      | -               | 1   | 2    | 3              |
        | ------ | --------------- | --- | ---- | -------------- |
        | 条件   | 検針値の数 == 0 | x   | x    | x              |
        | -      | 検針値の数 == 1 | o   | x    | -              |
        | -      | 同日の重複      | -   | x    | o              |
        | 期待値 | diff_usage      | 0   | 通常 | 通常           |
        | -      | diff_day_num    | 0   | 通常 | 0              |
        | -      | daily_usage     | 0   | 通常 | diff_usage / 1 |
        """

        df[ColumnEnum.diff_usage.name] = (
            df[ColumnEnum.accum_usage.name].diff().round(3).fillna(0.0)
        )
        df[ColumnEnum.diff_day_num.name] = (
            df[ColumnEnum.datetime.name]
            .dt.date.diff()
            .apply(lambda x: float(x.days) if type(x) is pd.Timedelta else x)
            .fillna(0.0)
        )

        df[ColumnEnum.daily_usage.name] = (
            df[ColumnEnum.diff_usage.name]
            / df[ColumnEnum.diff_day_num.name].apply(lambda x: max(x, 1))
        ).round(3)

        return df

    def make_interpolate_to_df(self, df: pd.DataFrame, end_dt: pd.Timestamp) -> pd.DataFrame:
        """
        end_dtまで時系列を補間する.
        下記の基準で検針値を補間する
        | -      | -                  | -   | 1   | 2        | 3   | 4   | 5        | 6   |
        | ------ | ------------------ | --- | --- | -------- | --- | --- | -------- | --- |
        | 条件   | start_dt >= end_dt | -   | o   | x        | x   | x   | x        | x   |
        | -      | 単数欠損           | 中  | -   | o        | -   | x   | -        | -   |
        | -      | -                  | 後  | -   | -        | o   | -   | -        | -   |
        | -      | 連続欠損           | 中  | -   | -        | -   | -   | o        | -   |
        | -      | -                  | 後  | -   | -        | -   | -   | -        | o   |
        | 期待値 | 単数欠損           | 中  | x   | 単数補完 | -   | x   | -        | -   |
        | -      | -                  | 後  | -   | -        | x   | x   | x        | x   |
        | -      | 連続欠損           | 中  | -   | -        | -   | -   | 単数補完 | -   |
        | -      | -                  | 後  | x   | x        | x   | x   | x        | x   |
        """
        min_dt = df[ColumnEnum.datetime.name].min()
        if min_dt >= end_dt:
            return df

        df_date = pd.DataFrame()
        df_date[ColumnEnum.datetime.name] = pd.date_range(min_dt, end_dt, freq="1D")
        df_date[ColumnEnum.str_meter_id.name] = df[ColumnEnum.str_meter_id.name].values[0]
        df = pd.merge(df_date, df, how="left", on=list(df_date.columns))

        df[ColumnEnum.accum_usage.name] = cast(
            pd.Series,
            df[ColumnEnum.accum_usage.name].interpolate(limit_area="inside", limit=1),
        ).round(1)

        return df

    def mask_df_by_time_range(
        self,
        df: pd.DataFrame,
        start_dt: Optional[pd.Timestamp] = None,
        end_dt: Optional[pd.Timestamp] = None,
    ) -> pd.DataFrame:

        if (start_dt is None) and (end_dt is not None):
            return df.loc[df[ColumnEnum.datetime.name] <= end_dt, :]
        elif (start_dt is not None) and (end_dt is None):
            return df.loc[start_dt <= df[ColumnEnum.datetime.name], :]
        elif (start_dt is not None) and (end_dt is not None):
            return df.loc[
                (start_dt <= df[ColumnEnum.datetime.name])
                & (df[ColumnEnum.datetime.name] <= end_dt),
                :,
            ]
        else:
            return df

    def mask_df_by_time_list(
        self, df: pd.DataFrame, list_timestamp: list[pd.Timestamp]
    ) -> pd.DataFrame:
        assert len(list_timestamp) > 0
        return df.loc[df[ColumnEnum.datetime.name].isin(set(list_timestamp)), :]
