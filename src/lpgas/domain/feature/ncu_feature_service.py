from typing import Any, Optional, cast

import numpy as np
import pandas as pd

from lpgas.domain.feature.feature_util import FeatureUtil
from lpgas.domain.identifier import MeterID
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.column_enum import ColumnEnum
from lpgas.utils.logging import get_lpgas_logger, get_str_progress, is_progress_index

logger = get_lpgas_logger(__name__)


class NCU_FeatureService:
    def gene_train_val_feature(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
        train_sample_num: int,
        val_sample_num: int,
        end_dt: pd.Timestamp,
        list_target_meter_id: Optional[list[MeterID]] = None,
    ) -> tuple[pd.DataFrame, pd.DataFrame, list[str], str]:

        dict_ncu_meter_usage_data = cleansed_input_repo.get_dict_ncu_meter_usage_data(
            list_target_meter_id
        )
        if len(dict_ncu_meter_usage_data) == 0:
            raise ValueError("対象メータの検針データが存在しない")

        dict_ncu_meter_usage_data = self._filter_dict_ncu_meter_usage_data(
            dict_ncu_meter_usage_data, end_dt
        )
        if len(dict_ncu_meter_usage_data) == 0:
            raise ValueError(f"対象メータの{str(end_dt.date())}以前の検針データが存在しない")

        df_train, df_val = self._make_df_train_val(
            dict_ncu_meter_usage_data=dict_ncu_meter_usage_data,
            recent_term=recent_term,
            week_term=week_term,
            dayofweek_term=dayofweek_term,
            end_dt=end_dt,
            train_sample_num=train_sample_num,
            val_sample_num=val_sample_num,
        )

        list_feature_name = [
            name
            for name in df_train.columns.to_list()
            if name not in [ColumnEnum.str_meter_id.name, ColumnEnum.f_target.name]
        ]
        target_name = ColumnEnum.f_target.name

        return df_train, df_val, list_feature_name, target_name

    def gene_test_feature(
        self,
        cleansed_input_repo: IF_CleansedInputRepo,
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
        end_dt: pd.Timestamp,
        list_target_meter_id: Optional[list[MeterID]] = None,
    ) -> tuple[pd.DataFrame, list[str], str]:

        dict_ncu_meter_usage_data = cleansed_input_repo.get_dict_ncu_meter_usage_data(
            list_target_meter_id
        )
        if len(dict_ncu_meter_usage_data) == 0:
            raise ValueError("対象メータの検針データが存在しない")

        dict_ncu_meter_usage_data = self._filter_dict_ncu_meter_usage_data(
            dict_ncu_meter_usage_data, end_dt
        )
        if len(dict_ncu_meter_usage_data) == 0:
            raise ValueError(f"対象メータの{str(end_dt.date())}以前の検針データが存在しない")

        df_test = self._make_df_test(
            dict_ncu_meter_usage_data=dict_ncu_meter_usage_data,
            recent_term=recent_term,
            week_term=week_term,
            dayofweek_term=dayofweek_term,
            end_dt=end_dt,
        )

        list_feature_name = [
            name
            for name in df_test.columns.to_list()
            if name not in [ColumnEnum.str_meter_id.name, ColumnEnum.f_target.name]
        ]
        target_name = ColumnEnum.f_target.name

        return df_test, list_feature_name, target_name

    def _make_df_train_val(
        self,
        dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData],
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
        end_dt: pd.Timestamp,
        train_sample_num: int,
        val_sample_num: int,
    ) -> tuple[pd.DataFrame, pd.DataFrame]:

        dict_df_with_diff = self._make_dict_df_with_diff(dict_ncu_meter_usage_data, end_dt)

        list_df_val: list[pd.DataFrame] = []
        list_df_train: list[pd.DataFrame] = []
        for i in range(val_sample_num):
            val_sample_end_dt = cast(pd.Timestamp, end_dt - pd.Timedelta(days=i))

            df_a_val = self._make_df_Xy(
                dict_df_with_diff=dict_df_with_diff,
                recent_term=recent_term,
                week_term=week_term,
                dayofweek_term=dayofweek_term,
                end_dt=val_sample_end_dt,
            )
            list_df_val.append(df_a_val)
        df_val = pd.concat(list_df_val, axis=0)

        for i in range(train_sample_num):
            train_sample_end_dt = cast(
                pd.Timestamp, end_dt - pd.Timedelta(days=i + val_sample_num)
            )

            df_a_train = self._make_df_Xy(
                dict_df_with_diff=dict_df_with_diff,
                recent_term=recent_term,
                week_term=week_term,
                dayofweek_term=dayofweek_term,
                end_dt=train_sample_end_dt,
            )
            list_df_train.append(df_a_train)
        df_train = pd.concat(list_df_train, axis=0)
        return df_train, df_val

    def _make_df_test(
        self,
        dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData],
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
        end_dt: pd.Timestamp,
    ) -> pd.DataFrame:

        dict_df_with_diff = self._make_dict_df_with_diff(dict_ncu_meter_usage_data, end_dt)
        df_test = self._make_df_Xy(
            dict_df_with_diff=dict_df_with_diff,
            recent_term=recent_term,
            week_term=week_term,
            dayofweek_term=dayofweek_term,
            end_dt=end_dt,
        )
        return df_test

    def _make_df_Xy(
        self,
        dict_df_with_diff: dict[MeterID, pd.DataFrame],
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
        end_dt: pd.Timestamp,
    ):

        list_str_meter_id = []
        list_recent_feature = []
        list_dayofweek_feature = []
        list_week_feature = []
        list_target = []

        length = len(dict_df_with_diff)
        for prog_i, (meter_id, df_with_diff) in enumerate(dict_df_with_diff.items()):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            if df_with_diff.shape[0] <= 0:
                logger.warning(f"meter_id:{meter_id.id}, DFが空.")
                continue

            try:
                list_a_recent_feature = self._make_list_a_recent_feature(
                    df_with_diff=df_with_diff,
                    end_dt=end_dt,
                    num_recent_days=recent_term,
                )
                list_a_week_feature = self._make_list_a_week_feature(
                    df_with_diff=df_with_diff,
                    end_dt=end_dt,
                    num_week=week_term,
                )
                list_a_dayofweek_feature = self._make_list_a_dayofweek_feature(
                    df_with_diff=df_with_diff,
                    end_dt=end_dt,
                    num_week=dayofweek_term,
                )
                list_a_target = self._make_list_a_target(
                    df_with_diff=df_with_diff,
                    end_dt=end_dt,
                )
                # TODO 残りの特徴量を追加
            except ValueError as e:
                logger.warning(f"meter_id:{meter_id.id}, {e}")
                continue

            list_str_meter_id.append(meter_id.id)
            list_recent_feature.append(list_a_recent_feature)
            list_week_feature.append(list_a_week_feature)
            list_dayofweek_feature.append(list_a_dayofweek_feature)
            list_target.append(list_a_target)

        if len(list_str_meter_id) == 0:
            raise ValueError

        df_Xy = self._convert_list_feature_to_df_Xy(
            list_str_meter_id=list_str_meter_id,
            list_recent_feature=list_recent_feature,
            list_dayofweek_feature=list_dayofweek_feature,
            list_week_feature=list_week_feature,
            list_target=list_target,
            recent_term=recent_term,
            week_term=week_term,
            dayofweek_term=dayofweek_term,
        )
        return df_Xy

    def _make_dict_df_with_diff(
        self,
        dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData],
        end_dt: pd.Timestamp,
    ) -> dict[MeterID, pd.DataFrame]:

        dict_df_with_diff: dict[MeterID, pd.DataFrame] = {}
        length = len(dict_ncu_meter_usage_data)
        for prog_i, (meter_id, ncu_meter_usage_data) in enumerate(
            dict_ncu_meter_usage_data.items()
        ):
            if is_progress_index(prog_i, length):
                logger.info(f"{get_str_progress(prog_i, length)}")

            df = NcuMeterUsageDataFactory().dump_df(ncu_meter_usage_data, False)
            df = FeatureUtil().make_interpolate_to_df(df, end_dt)
            df = FeatureUtil().mask_df_by_time_range(df, end_dt=end_dt)
            df_with_diff = FeatureUtil().attached_diff_to_df(df)
            dict_df_with_diff[meter_id] = df_with_diff
        return dict_df_with_diff

    def _make_list_a_recent_feature(
        self,
        df_with_diff: pd.DataFrame,
        end_dt: pd.Timestamp,
        num_recent_days: int,
    ) -> list[Any]:

        recent_end_dt = cast(pd.Timestamp, end_dt - pd.Timedelta(days=1))
        recent_start_dt = cast(
            pd.Timestamp, recent_end_dt - pd.Timedelta(days=num_recent_days - 1)
        )
        df = FeatureUtil().mask_df_by_time_range(
            df_with_diff, start_dt=recent_start_dt, end_dt=recent_end_dt
        )

        if df.shape[0] != num_recent_days:
            raise ValueError("recent特徴量生成に必要な検針値が足りない. または重複している.")

        list_a_recent_feature = list(df[ColumnEnum.diff_usage.name].values)

        return list_a_recent_feature

    def _make_list_a_week_feature(
        self,
        df_with_diff: pd.DataFrame,
        end_dt: pd.Timestamp,
        num_week: int,
    ) -> list[Any]:
        assert num_week > 0

        list_a_week_feature = []
        for num_week_ago in range(1, num_week + 1):
            start_in_week = end_dt - cast(pd.Timedelta, pd.Timedelta(days=7 * num_week_ago))
            list_each_week = list(pd.date_range(start=start_in_week, freq="D", periods=7))
            df_each_week = FeatureUtil().mask_df_by_time_list(df_with_diff, list_each_week)
            if df_each_week.shape[0] != 7:
                raise ValueError("week特徴量生成に必要な検針値が足りない. または重複している.")
            ave_each_week = df_each_week[ColumnEnum.diff_usage.name].mean()
            list_a_week_feature.append(ave_each_week)

        list_a_week_feature.append(np.round(np.mean(list_a_week_feature), 2))

        assert len(list_a_week_feature) == num_week + 1
        return list_a_week_feature

    def _make_list_a_dayofweek_feature(
        self,
        df_with_diff: pd.DataFrame,
        end_dt: pd.Timestamp,
        num_week: int,
    ) -> list[Any]:
        assert num_week > 0

        list_num_week_ago = [
            cast(pd.Timestamp, end_dt - pd.Timedelta(days=7 * i)) for i in range(1, num_week + 1)
        ]

        df = FeatureUtil().mask_df_by_time_list(df_with_diff, list_num_week_ago)
        if df.shape[0] != num_week:
            raise ValueError("dayofweek特徴量生成に必要な検針値が足りない. または重複している.")

        list_a_dayofweek_feature = list(df[ColumnEnum.diff_usage.name].values)
        list_a_dayofweek_feature.append(np.round(np.mean(list_a_dayofweek_feature), 2))
        return list_a_dayofweek_feature

    def _make_list_a_target(
        self,
        df_with_diff: pd.DataFrame,
        end_dt: pd.Timestamp,
    ) -> list[Any]:

        df = FeatureUtil().mask_df_by_time_range(df_with_diff, start_dt=end_dt, end_dt=end_dt)
        if df.shape[0] != 1:
            raise ValueError("目的変数に必要な検針値が足りない. または重複している.")

        list_a_target = list(df[ColumnEnum.diff_usage.name].values)
        return list_a_target

    def _convert_list_recent_feature_to_df(
        self, list_recent_feature: list[Any], recent_term: int
    ) -> pd.DataFrame:
        _columns = [f"{ColumnEnum.f_ncu_recent.name}_{num}" for num in range(recent_term)]

        df_recent_feature = pd.DataFrame(
            list_recent_feature,
            columns=_columns,
        )
        return df_recent_feature

    def _convert_list_week_feature_to_df(
        self, list_week_feature: list[Any], week_term: int
    ) -> pd.DataFrame:
        _columns = [f"{ColumnEnum.f_ncu_week.name}_{num}" for num in range(week_term)]
        _columns.append(ColumnEnum.f_ncu_ave_week.name)

        df_dayofweek_feature = pd.DataFrame(
            list_week_feature,
            columns=_columns,
        )
        return df_dayofweek_feature

    def _convert_list_dayofweek_feature_to_df(
        self, list_dayofweek_feature: list[Any], dayofweek_term: int
    ) -> pd.DataFrame:
        _columns = [f"{ColumnEnum.f_ncu_dayofweek.name}_{num}" for num in range(dayofweek_term)]
        _columns.append(ColumnEnum.f_ncu_ave_dayofweek.name)

        df_dayofweek_feature = pd.DataFrame(
            list_dayofweek_feature,
            columns=_columns,
        )
        return df_dayofweek_feature

    def _convert_list_target_to_df(self, list_target: list[Any]) -> pd.DataFrame:
        _columns = [ColumnEnum.f_target.name]

        df_target = pd.DataFrame(
            list_target,
            columns=_columns,
        )
        return df_target

    def _convert_list_feature_to_df_Xy(
        self,
        list_str_meter_id: list[str],
        list_recent_feature: list[Any],
        list_week_feature: list[Any],
        list_dayofweek_feature: list[Any],
        list_target: list[Any],
        recent_term: int,
        week_term: int,
        dayofweek_term: int,
    ):
        df_recent_feature = self._convert_list_recent_feature_to_df(
            list_recent_feature=list_recent_feature, recent_term=recent_term
        )
        df_week_feature = self._convert_list_week_feature_to_df(
            list_week_feature=list_week_feature, week_term=week_term
        )
        df_dayofweek_feature = self._convert_list_dayofweek_feature_to_df(
            list_dayofweek_feature=list_dayofweek_feature, dayofweek_term=dayofweek_term
        )

        df_y = self._convert_list_target_to_df(list_target=list_target)

        df_Xy = pd.concat([df_recent_feature, df_week_feature, df_dayofweek_feature, df_y], axis=1)
        # df_Xy = pd.concat([df_recent_feature, df_y], axis=1)
        df_Xy[ColumnEnum.str_meter_id.name] = list_str_meter_id

        return df_Xy

    def _filter_dict_ncu_meter_usage_data(
        self, dict_ncu_meter_usage_data: dict[MeterID, NcuMeterUsageData], end_dt: pd.Timestamp
    ) -> dict[MeterID, NcuMeterUsageData]:
        dict_ncu_meter_usage_data_filtered = {}
        for meter_id, ncu_meter_usage_data in dict_ncu_meter_usage_data.items():
            if end_dt < ncu_meter_usage_data.start_dt:
                continue
            dict_ncu_meter_usage_data_filtered[meter_id] = ncu_meter_usage_data
        return dict_ncu_meter_usage_data_filtered
