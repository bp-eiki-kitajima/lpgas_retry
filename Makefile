run:
	poetry run python my_lpgas/main.py -cfg ./configs/config.yaml
check: 
	poetry run isort .
	poetry run black .
	poetry run pflake8 .
	poetry run mypy .
	poetry run pytest --cov=src/lpgas --cov-branch --cov-report=html --html=./htmlreport/report.html --self-contained-html -v -vv
test:
	poetry run pytest --cov=src/lpgas --cov-branch --cov-report=html --html=./htmlreport/report.html --self-contained-html -v -vv
html:
	PYTHONPATH=src mkdocs build --no-directory-urls
vprof:
	snakeviz prof/combined.prof -s -p 8080 -H 0.0.0.0