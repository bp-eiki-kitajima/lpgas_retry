from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase


def test_predict_and_store_results(predict_usecase: IF_PredictUsecase) -> None:
    """予測と結果の保存ができるか"""
    predict_usecase.predict_and_store_results()
    assert True
