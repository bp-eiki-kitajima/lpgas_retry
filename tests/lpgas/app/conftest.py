import pytest

from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase
from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase
from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def raw_input_usecase(domain_registry: DomainRegistry):
    raw_input_usecase = domain_registry.raw_input_usecase()
    assert isinstance(raw_input_usecase, IF_RawInputUsecase)

    raw_input_usecase.init_output()
    yield raw_input_usecase
    raw_input_usecase.init_output()


@pytest.fixture
def train_usecase(domain_registry: DomainRegistry, raw_input_usecase: IF_RawInputUsecase):
    train_usecase = domain_registry.train_usecase()
    assert isinstance(train_usecase, IF_TrainUsecase)

    raw_input_usecase.gene_and_store_all()
    train_usecase.init_output()
    yield train_usecase
    train_usecase.init_output()


@pytest.fixture
def predict_usecase(domain_registry: DomainRegistry, train_usecase: IF_TrainUsecase):
    predict_usecase = domain_registry.predict_usecase()
    assert isinstance(predict_usecase, IF_PredictUsecase)

    train_usecase.train_and_store_model()
    predict_usecase.init_output()
    yield predict_usecase
    predict_usecase.init_output()


@pytest.fixture
def evaluate_usecase(domain_registry: DomainRegistry, predict_usecase: IF_PredictUsecase):
    evaluate_usecase = domain_registry.evaluate_usecase()
    assert isinstance(evaluate_usecase, IF_EvaluateUsecase)

    predict_usecase.predict_and_store_results()
    evaluate_usecase.init_output()
    yield evaluate_usecase
    evaluate_usecase.init_output()
