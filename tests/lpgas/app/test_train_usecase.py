from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase


def test_train_and_store_model(train_usecase: IF_TrainUsecase) -> None:
    """学習と学習済みモデルの保存ができるか"""
    train_usecase.train_and_store_model()
    assert True
