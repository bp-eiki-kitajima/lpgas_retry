from typing import cast

from lpgas.app.raw_input_usecase import RawInputUsecase
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase


def test_gene_and_store_dict_meter(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_meter()
    assert True


def test_gene_and_store_dict_lc(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_lc()
    assert True


def test_gene_and_store_dict_meter_usage_data(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_meter_usage_data()
    assert True


def test_gene_and_store_dict_ncu_meter_usage(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_ncu_meter_usage_data()
    assert True


def test_gene_and_store_dict_delivery_result_data(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_delivery_result_data()
    assert True


def test_gene_and_store_dict_weather_data(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_dict_weather_data()
    assert True


def test_gene_and_store_all(raw_input_usecase: IF_RawInputUsecase) -> None:
    raw_input_usecase.gene_and_store_all()
    assert True


def test_gene_and_store_dict_delivery_result_data_for_merge(
    raw_input_usecase: IF_RawInputUsecase,
) -> None:
    """一時的に追加しているテスト"""
    raw_input_usecase = cast(RawInputUsecase, raw_input_usecase)

    raw_input_usecase.gene_and_store_dict_lc(enable_preprocess=False)
    raw_input_usecase.gene_and_store_dict_meter(enable_preprocess=False)
    raw_input_usecase.gene_and_store_dict_delivery_result_data(enable_preprocess=False)
    raw_input_usecase.gene_and_store_dict_meter_usage_data(enable_preprocess=False)
    raw_input_usecase._preprocess_service.apply_and_store(raw_input_usecase._cleansed_input_repo)
    assert True
