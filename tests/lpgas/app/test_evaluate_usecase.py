from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase


def test_evaluate_and_store_evaluation(evaluate_usecase: IF_EvaluateUsecase) -> None:
    """予測の評価と保存ができるか"""
    evaluate_usecase.evaluate_and_store_evaluation()
    assert True
