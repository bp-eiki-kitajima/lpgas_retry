import pathlib

import pytest
from pytest_mock import MockerFixture

from lpgas.adapter.dp_model_repo import DPModelRepo
from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.utils.boudary.if_dp_model import IF_DPModel
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def dp_model_repo(domain_registry: DomainRegistry):
    dp_model_repo = domain_registry.dp_model_repo()
    if not isinstance(dp_model_repo, DPModelRepo):
        pytest.skip("this is not target repo for test")

    dp_model_repo.init_output()
    yield dp_model_repo
    dp_model_repo.init_output()


def test_store(tmp_path: pathlib.Path, mocker: MockerFixture, dp_model_repo: DPModelRepo) -> None:
    """DPModelの保存ができるか"""

    p_model = tmp_path / "dp_model.pickle"
    p_tag = tmp_path / "dp_model_tag.csv"
    p_result_dir = tmp_path / "result"

    mocker.patch.object(
        dp_model_repo, dp_model_repo._get_latest_model_path.__name__, return_value=p_model
    )
    mocker.patch.object(
        dp_model_repo, dp_model_repo._get_latest_model_tag_path.__name__, return_value=p_tag
    )
    mocker.patch.object(
        dp_model_repo,
        dp_model_repo._get_latest_model_result_dir_path.__name__,
        return_value=p_result_dir,
    )

    isg_dp_model = DPModelFactory().gene_dummy_isg_dp_model()
    dp_model_repo.store(isg_dp_model)

    assert p_model.exists()
    assert p_tag.exists()
    assert p_result_dir.exists()


def test_load_latest(
    tmp_path: pathlib.Path, mocker: MockerFixture, dp_model_repo: DPModelRepo
) -> None:
    """最新のモデルを読込できるか"""

    p_dir = tmp_path
    p_model = tmp_path / "dp_model.pickle"
    p_tag = tmp_path / "dp_model_tag.csv"
    mocker.patch.object(
        dp_model_repo, dp_model_repo._get_dp_model_dir_path.__name__, return_value=p_dir
    )
    mocker.patch.object(
        dp_model_repo, dp_model_repo._get_latest_model_path.__name__, return_value=p_model
    )
    mocker.patch.object(
        dp_model_repo, dp_model_repo._get_latest_model_tag_path.__name__, return_value=p_tag
    )

    isg_dp_model = DPModelFactory().gene_dummy_isg_dp_model()
    dp_model_repo.store(isg_dp_model)

    dp_model = dp_model_repo.load_latest()
    assert isinstance(dp_model, IF_DPModel)
