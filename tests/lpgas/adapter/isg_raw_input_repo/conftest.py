import pytest

from lpgas.adapter.isg_raw_input_repo.isg_raw_input_repo import ISG_RawInputRepo
from lpgas.utils.domain_registry import DomainRegistry

from .fixtures import (
    fixture_gene_dict_delivery_result_data,
    fixture_gene_dict_lc,
    fixture_gene_dict_meter,
    fixture_gene_dict_meter_usage_data,
    fixture_gene_dict_ncu_meter_usage_data,
    fixture_gene_dict_weather_data,
    fixture_get_delivery_result,
    fixture_get_lc_and_meter,
    fixture_get_lc_attr,
    fixture_get_meter_attr,
    fixture_get_meter_usage_value,
    fixture_get_ncu_meter_usage_value,
    fixture_get_weather,
)


@pytest.fixture
def isg_raw_input_repo(domain_registry: DomainRegistry):
    isg_raw_input_repo = domain_registry.raw_input_repo()
    if not isinstance(isg_raw_input_repo, ISG_RawInputRepo):
        pytest.skip("this is not target repo for test")
    return isg_raw_input_repo


fixture_get_df_lc_and_meter_ok = fixture_get_lc_and_meter.fixture_ok
fixture_get_df_meter_attr_ok = fixture_get_meter_attr.fixture_ok
fixture_get_df_lc_attr_ok = fixture_get_lc_attr.fixture_ok
fixture_get_df_meter_usage_value_ok = fixture_get_meter_usage_value.fixture_ok
fixture_get_df_ncu_meter_usage_value_ok = fixture_get_ncu_meter_usage_value.fixture_ok
fixture_get_df_delivery_result_ok = fixture_get_delivery_result.fixture_ok
fixture_get_df_weather_ok = fixture_get_weather.fixture_ok
fixture_gene_dict_meter_ok = fixture_gene_dict_meter.fixture_ok
fixture_gene_dict_meter_ng = fixture_gene_dict_meter.fixture_ng
fixture_gene_dict_lc_ok = fixture_gene_dict_lc.fixture_ok
fixture_gene_dict_lc_ng = fixture_gene_dict_lc.fixture_ng
fixture_gene_dict_meter_usage_data_ok = fixture_gene_dict_meter_usage_data.fixture_ok
fixture_gene_dict_ncu_meter_usage_data_ok = fixture_gene_dict_ncu_meter_usage_data.fixture_ok
fixture_gene_dict_delivery_result_data_ok = fixture_gene_dict_delivery_result_data.fixture_ok
fixture_gene_dict_weather_data_ok = fixture_gene_dict_weather_data.fixture_ok
