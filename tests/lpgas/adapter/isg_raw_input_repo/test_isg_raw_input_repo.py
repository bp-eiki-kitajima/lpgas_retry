from typing import Any, cast

import pandas as pd
import pytest
from pandas.testing import assert_frame_equal
from pytest_mock import MockerFixture

from lpgas.adapter.isg_raw_input_repo.isg_raw_input_repo import ISG_RawInputRepo
from lpgas.domain.delivery_result_data import DeliveryResultData
from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC
from lpgas.domain.meter.meter import Meter
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.weather.weather_data import WeatherData


def test_load_data(isg_raw_input_repo: ISG_RawInputRepo) -> None:
    """全てのload関数が正常なデータを読み込むことを確認するか"""

    assert isinstance(isg_raw_input_repo._load_joined_meter_raw(), pd.DataFrame)
    assert isinstance(isg_raw_input_repo._load_lc_list_raw(), pd.DataFrame)
    assert isinstance(isg_raw_input_repo._load_meter_read_raw(), pd.DataFrame)
    assert isinstance(isg_raw_input_repo._load_meter_ncu_raw(), pd.DataFrame)
    assert isinstance(isg_raw_input_repo._load_delivery_result_raw(), pd.DataFrame)
    assert isinstance(isg_raw_input_repo._load_weather_raw(), pd.DataFrame)

    assert isg_raw_input_repo._load_joined_meter_raw().shape[0] > 0
    assert isg_raw_input_repo._load_lc_list_raw().shape[0] > 0
    assert isg_raw_input_repo._load_meter_read_raw().shape[0] > 0
    assert isg_raw_input_repo._load_meter_ncu_raw().shape[0] > 0
    assert isg_raw_input_repo._load_delivery_result_raw().shape[0] > 0
    assert isg_raw_input_repo._load_weather_raw().shape[0] > 0


def test_get_df_lc_and_meter_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_lc_and_meter_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なload_joined_meter.csvを入力して, 想定のlcとmeter関係を表すdfが生成されるか"""

    df_joined_meter, df_lc_and_meter = fixture_get_df_lc_and_meter_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_joined_meter_raw.__name__,
        return_value=df_joined_meter,
    )

    df_result = isg_raw_input_repo._get_df_lc_and_meter()
    assert_frame_equal(
        df_result, df_lc_and_meter, check_index_type=cast(str, False), check_like=True
    )


def test_get_df_meter_attr_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_meter_attr_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なload_joined_meter.csvを入力して, 想定のmeterの属性を表すdfが生成されるか"""

    df_joined_meter, df_meter_attr = fixture_get_df_meter_attr_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_joined_meter_raw.__name__,
        return_value=df_joined_meter,
    )

    df_result = isg_raw_input_repo._get_df_meter_attr()
    assert_frame_equal(df_result, df_meter_attr, check_index_type=cast(str, False))


def test_get_df_lc_attr_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_lc_attr_ok: tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なdf_lc_listから, df_lc_attrが生成できるか"""

    df_lc_list, df_lc_and_meter, df_lc_attr = fixture_get_df_lc_attr_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_lc_list_raw.__name__,
        return_value=df_lc_list,
    )

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_lc_and_meter.__name__,
        return_value=df_lc_and_meter,
    )

    df_result = isg_raw_input_repo._get_df_lc_attr()
    assert_frame_equal(df_result, df_lc_attr, check_index_type=cast(str, False), check_like=True)


def test_get_df_meter_usage_value_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_meter_usage_value_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なdf_meter_readから, 想定の手動検針値のdfが生成されるか"""

    df_meter_read, df_meter_usage_value = fixture_get_df_meter_usage_value_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_meter_read_raw.__name__,
        return_value=df_meter_read,
    )
    df_result = cast(pd.DataFrame, isg_raw_input_repo._get_df_meter_usage_value())

    assert_frame_equal(df_result, df_meter_usage_value, check_index_type=cast(str, False))


def test_get_df_ncu_meter_usage_value_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_ncu_meter_usage_value_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なload_meter_ncu.csvを入力して, 想定のNCU検針値のdfが生成されるか"""

    df_meter_ncu, df_ncu_meter_usage_value = fixture_get_df_ncu_meter_usage_value_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_meter_ncu_raw.__name__,
        return_value=df_meter_ncu,
    )
    df_result = cast(pd.DataFrame, isg_raw_input_repo._get_df_ncu_meter_usage_value())

    assert_frame_equal(df_result, df_ncu_meter_usage_value, check_index_type=cast(str, False))


def test_get_df_delivery_result_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_delivery_result_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なdelivery_result.csvを入力して, 想定のdf_delivery_resultが生成されるか"""

    df_delivery_result, df_delivery_result_value = fixture_get_df_delivery_result_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_delivery_result_raw.__name__,
        return_value=df_delivery_result,
    )
    df_result = cast(pd.DataFrame, isg_raw_input_repo._get_df_delivery_result())

    assert_frame_equal(
        df_result, df_delivery_result_value, check_index_type=cast(str, False), check_like=True
    )


def test_get_df_weather_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_get_df_weather_ok: tuple[pd.DataFrame, pd.DataFrame],
) -> None:
    """正常なdf_weather_rawから, 想定のdf_weatherが生成されるか"""

    df_weather_raw, df_weather = fixture_get_df_weather_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._load_weather_raw.__name__,
        return_value=df_weather_raw,
    )
    df_result = cast(pd.DataFrame, isg_raw_input_repo._get_df_weather())

    assert_frame_equal(df_result, df_weather, check_index_type=cast(str, False), check_like=True)


def test_gene_dict_meter_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_meter_ok: tuple[pd.DataFrame, dict[MeterID, Meter]],
) -> None:
    """正常にmeterオブジェクトが生成されるか"""

    df_meter_attr, dict_meter = fixture_gene_dict_meter_ok
    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_meter_attr.__name__,
        return_value=df_meter_attr,
    )

    dict_meter_result = isg_raw_input_repo.gene_dict_meter()
    assert dict_meter_result == dict_meter


def test_gene_dict_meter_ng(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_meter_ng: tuple[pd.DataFrame, Any],
) -> None:
    """異常なdf_meter_attrを入力した時に想定のエラーが発生するか"""

    df_meter_attr, error = fixture_gene_dict_meter_ng
    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_meter_attr.__name__,
        return_value=df_meter_attr,
    )

    with pytest.raises(error):
        isg_raw_input_repo.gene_dict_meter()


def test_gene_dict_lc_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_lc_ok: tuple[pd.DataFrame, list[LC]],
) -> None:
    """正常にlcオブジェクトが生成されるか"""

    df_lc_attr, dict_lc = fixture_gene_dict_lc_ok

    mocker.patch.object(
        isg_raw_input_repo, isg_raw_input_repo._get_df_lc_attr.__name__, return_value=df_lc_attr
    )

    dict_lc_result = isg_raw_input_repo.gene_dict_lc()
    assert dict_lc_result == dict_lc


def test_gene_dict_lc_ng(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_lc_ng: tuple[pd.DataFrame, Any],
) -> None:
    """異常なdf_lc_attrを入力した時に想定のエラーが発生するか"""

    df_lc_attr, error = fixture_gene_dict_lc_ng
    mocker.patch.object(
        isg_raw_input_repo, isg_raw_input_repo._get_df_lc_attr.__name__, return_value=df_lc_attr
    )

    with pytest.raises(error):
        isg_raw_input_repo.gene_dict_lc()


def test_gene_dict_meter_usage_data_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_meter_usage_data_ok: tuple[pd.DataFrame, dict[MeterID, MeterUsageData]],
) -> None:
    """正常にMeterUsageDataオブジェクトが生成されるか"""

    df_meter_usage_value, dict_meter_usage_data = fixture_gene_dict_meter_usage_data_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_meter_usage_value.__name__,
        return_value=df_meter_usage_value,
    )

    dict_meter_usage_data_result = isg_raw_input_repo.gene_dict_meter_usage_data()

    assert dict_meter_usage_data_result == dict_meter_usage_data


def test_gene_dict_ncu_meter_usage_data_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_ncu_meter_usage_data_ok: tuple[pd.DataFrame, list[NcuMeterUsageData]],
) -> None:
    """正常にNcuMeterUsageDataオブジェクトが生成されるか"""

    df_ncu_meter_usage_value, dict_ncu_meter_usage_data = fixture_gene_dict_ncu_meter_usage_data_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_ncu_meter_usage_value.__name__,
        return_value=df_ncu_meter_usage_value,
    )

    dict_ncu_meter_usage_data_result = isg_raw_input_repo.gene_dict_ncu_meter_usage_data()

    assert dict_ncu_meter_usage_data_result == dict_ncu_meter_usage_data


def test_gene_dict_delivery_result_data_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_delivery_result_data_ok: tuple[
        pd.DataFrame, dict[LC_ID, DeliveryResultData]
    ],
) -> None:
    """正常にdelivery_result_dataオブジェクトが生成されるか"""

    df_delivery_result_value, dict_delivery_result_data = fixture_gene_dict_delivery_result_data_ok

    mocker.patch.object(
        isg_raw_input_repo,
        isg_raw_input_repo._get_df_delivery_result.__name__,
        return_value=df_delivery_result_value,
    )

    dict_delivery_result_date_result = isg_raw_input_repo.gene_dict_delivery_result_data()

    assert dict_delivery_result_date_result == dict_delivery_result_data


def test_gene_dict_weather_data_ok(
    mocker: MockerFixture,
    isg_raw_input_repo: ISG_RawInputRepo,
    fixture_gene_dict_weather_data_ok: tuple[pd.DataFrame, dict[WeatherCityID, WeatherData]],
) -> None:
    """正常にWeatherDataオブジェクトが生成されるか"""

    df_weather, dict_weather_data = fixture_gene_dict_weather_data_ok

    mocker.patch.object(
        isg_raw_input_repo, isg_raw_input_repo._get_df_weather.__name__, return_value=df_weather
    )

    dict_weather_data_result = isg_raw_input_repo.gene_dict_weather_data()

    assert dict_weather_data_result == dict_weather_data
