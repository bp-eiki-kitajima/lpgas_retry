from typing import Any

import pandas as pd
import pytest
from _pytest.fixtures import SubRequest
from pandera.errors import SchemaError

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.identifier import LC_ID
from lpgas.domain.lc import LC, LCFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
    ),
    "main_cylinder_size is 0": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [0, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.2, 35.3],
            ColumnEnum.lon.name: [140.2, 140.3],
            ColumnEnum.addr.name: ["addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 20],
            ColumnEnum.main_cylinder_num.name: [2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 20],
            ColumnEnum.sub_cylinder_num.name: [2, 1],
            ColumnEnum.str_staff_id.name: ["1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[LC_ID, LC]]:
    df_lc_attr = pd.DataFrame(data=request.param[0])
    df_lc_attr = Schema_For_ISG_RawInputRepo().validate_df_lc_attr(df_lc_attr)

    df_lc = pd.DataFrame(data=request.param[1])
    dict_lc = LCFactory().gene_dict_from_df(df_lc)

    return df_lc_attr, dict_lc


params_ng = {
    "len is 0": (
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [],
                ColumnEnum.str_lc_id.name: [],
                ColumnEnum.is_ncu.name: [],
                ColumnEnum.template_number.name: [],
                ColumnEnum.enable_prediction.name: [],
            }
        ),
        SchemaError,
    ),
}


@pytest.fixture(
    params=params_ng.values(),
    ids=params_ng.keys(),
)
def fixture_ng(
    request: SubRequest,
) -> tuple[pd.DataFrame, Any]:
    df_lc_attr = request.param[0]
    error = request.param[1]

    return df_lc_attr, error
