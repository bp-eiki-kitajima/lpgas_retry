from typing import Any

import pandas as pd
import pytest
from _pytest.fixtures import SubRequest
from pandera.errors import SchemaError

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.identifier import MeterID
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.enable_prediction.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.enable_prediction.name: [True, True, True],
        },
    ),
    "template number is wrong": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["", "101", "102"],
            ColumnEnum.enable_prediction.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["1", "2"],
            ColumnEnum.str_lc_id.name: ["1", "2"],
            ColumnEnum.is_ncu.name: [True, True],
            ColumnEnum.template_number.name: ["101", "102"],
            ColumnEnum.enable_prediction.name: [True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[MeterID, Meter]]:
    df_meter_attr = pd.DataFrame(data=request.param[0])
    df_meter_attr = Schema_For_ISG_RawInputRepo().validate_df_meter_attr(df_meter_attr)

    df_meter = pd.DataFrame(data=request.param[0])
    dict_meter = MeterFactory().gene_dict_from_df(df_meter)

    return df_meter_attr, dict_meter


params_ng = {
    "len is 0": (
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [],
                ColumnEnum.str_lc_id.name: [],
                ColumnEnum.is_ncu.name: [],
                ColumnEnum.template_number.name: [],
                ColumnEnum.enable_prediction.name: [],
            }
        ),
        SchemaError,
    ),
}


@pytest.fixture(
    params=params_ng.values(),
    ids=params_ng.keys(),
)
def fixture_ng(
    request: SubRequest,
) -> tuple[pd.DataFrame, Any]:
    df_meter_attr = request.param[0]
    error = request.param[1]

    return df_meter_attr, error
