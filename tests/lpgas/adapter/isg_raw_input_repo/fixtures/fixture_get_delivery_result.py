import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
    ),
    "lc_id is wrong": (
        {
            ColumnEnum.str_lc_id.name: [np.nan, "lc_1", "lc_2", "lc_2"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
        },
    ),
    "meter_usage_value is wrong": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.accum_usage.name: ["tmp", 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [np.nan, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [True, True, True, True],
            ColumnEnum.is_read.name: [False, True, True, True],
        },
    ),
    "delivery_input_amount is wrong": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [np.nan, 50.0, 50.0, 50.0],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [np.nan, 50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [False, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    df_delivery_result = pd.DataFrame(data=request.param[0])

    df_delivery_result_value = pd.DataFrame(data=request.param[1])
    df_delivery_result_value = Schema_For_ISG_RawInputRepo().validate_df_delivery_result_value(
        df_delivery_result_value
    )

    return df_delivery_result, df_delivery_result_value
