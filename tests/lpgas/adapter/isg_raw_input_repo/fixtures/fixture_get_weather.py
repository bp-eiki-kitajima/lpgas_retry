import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.date.name: ["2019-1-1", "2019-1-2", "2019-1-1", "2019-1-2"],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
    ),
    "date missing": (
        {
            ColumnEnum.date.name: [np.nan, "2019-1-2", "2019-1-1", "2019-1-2"],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [0.2, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
    ),
    "date skip": (
        {
            ColumnEnum.date.name: ["2019-1-1", "2019-1-3", "2019-1-1", "2019-1-2"],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=3),
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=3),
            ],
            ColumnEnum.ave_temp.name: [10.1, 5.1, 10.2, 0.1, 0.2, 5.1],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1", "w_2", "w_2", "w_2"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    # TODO 気が向いたらケース追加する
    df_weather_raw = pd.DataFrame(data=request.param[0])
    df_weather_raw = Schema_For_ISG_RawInputRepo().validate_df_weather_raw(df_weather_raw)

    df_weather = pd.DataFrame(data=request.param[1])
    df_weather = Schema_For_ISG_RawInputRepo().validate_df_weather(df_weather)

    return df_weather_raw, df_weather
