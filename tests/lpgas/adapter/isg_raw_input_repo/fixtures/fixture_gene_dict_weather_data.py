import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.identifier import WeatherCityID
from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2, 0.1, 0.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_2", "w_2"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[WeatherCityID, WeatherData]]:
    df_weather = pd.DataFrame(data=request.param[0])
    df_weather = Schema_For_ISG_RawInputRepo().validate_df_weather(df_weather)

    df_weather_data = pd.DataFrame(data=request.param[1])
    dict_weather_data = WeatherDataFactory().gene_dict_from_df(df_weather_data)

    return df_weather, dict_weather_data
