import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.identifier import MeterID
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[MeterID, MeterUsageData]]:
    df_meter_usage_value = pd.DataFrame(data=request.param[0])
    df_meter_usage_value = Schema_For_ISG_RawInputRepo().validate_df_meter_usage_value(
        df_meter_usage_value
    )

    df_meter_usage_data = pd.DataFrame(data=request.param[1])
    dict_meter_usage_data = MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_data)

    return df_meter_usage_value, dict_meter_usage_data
