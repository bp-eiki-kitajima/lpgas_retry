import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.identifier import MeterID
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data import NcuMeterUsageData
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[MeterID, NcuMeterUsageData]]:
    df_ncu_meter_usage_value = pd.DataFrame(data=request.param[0])
    df_ncu_meter_usage_value = Schema_For_ISG_RawInputRepo().validate_df_ncu_meter_usage_value(
        df_ncu_meter_usage_value
    )

    df_ncu_meter_usage_data = pd.DataFrame(data=request.param[1])
    dict_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dict_from_df(
        df_ncu_meter_usage_data
    )

    return df_ncu_meter_usage_value, dict_ncu_meter_usage_data
