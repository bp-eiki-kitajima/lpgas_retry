import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["1", "1", "1", "2", "2", "2"],
            ColumnEnum.str_meter_id.name: ["10", "11", "12", "20", "21", "22"],
        },
        {
            ColumnEnum.str_lc_id.name: ["1", "2"],
            ColumnEnum.list_str_meter_id.name: ["10 11 12", "20 21 22"],
        },
    ),
    "str_lc_id is not str": (
        {
            ColumnEnum.str_lc_id.name: [np.nan, "1", "1", np.nan, "2", "2"],
            ColumnEnum.str_meter_id.name: ["10", "11", "12", "20", "21", "22"],
        },
        {
            ColumnEnum.str_lc_id.name: ["1", "2"],
            ColumnEnum.list_str_meter_id.name: ["11 12", "21 22"],
        },
    ),
    "str_meter_id is not str": (
        {
            ColumnEnum.str_lc_id.name: ["1", "1", "1", "2", "2", "2"],
            ColumnEnum.str_meter_id.name: [np.nan, "11", "12", np.nan, "21", np.nan],
        },
        {
            ColumnEnum.str_lc_id.name: ["1", "2"],
            ColumnEnum.list_str_meter_id.name: ["11 12", "21"],
        },
    ),
    "all str_lc_id is nan": (
        {
            ColumnEnum.str_lc_id.name: [np.nan, np.nan, np.nan, "2", "2", "2"],
            ColumnEnum.str_meter_id.name: ["10", "11", "12", "20", "21", "22"],
        },
        {
            ColumnEnum.str_lc_id.name: ["2"],
            ColumnEnum.list_str_meter_id.name: ["20 21 22"],
        },
    ),
    "all str_meter_id is nan": (
        {
            ColumnEnum.str_lc_id.name: ["1", "1", "1", "2", "2", "2"],
            ColumnEnum.str_meter_id.name: [np.nan, np.nan, np.nan, "20", "21", "22"],
        },
        {
            ColumnEnum.str_lc_id.name: ["2"],
            ColumnEnum.list_str_meter_id.name: ["20 21 22"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:

    df_joined_meter = pd.DataFrame(data=request.param[0])
    df_joined_meter[ColumnEnum.customer_code.name] = "999"
    df_joined_meter[ColumnEnum.atach_status.name] = "設置完了"
    df_joined_meter[ColumnEnum.ncu_id.name] = "AAAA"
    df_joined_meter[ColumnEnum.template_number.name] = "2"
    df_joined_meter[ColumnEnum.open_status.name] = ""
    df_joined_meter[ColumnEnum.predict_target_flag.name] = True

    df_joined_meter = Schema_For_ISG_RawInputRepo().validate_df_joined_meter_raw(df_joined_meter)
    df_lc_and_meter = pd.DataFrame(data=request.param[1])
    df_lc_and_meter = Schema_For_ISG_RawInputRepo().validate_df_lc_and_meter(df_lc_and_meter)

    return df_joined_meter, df_lc_and_meter
