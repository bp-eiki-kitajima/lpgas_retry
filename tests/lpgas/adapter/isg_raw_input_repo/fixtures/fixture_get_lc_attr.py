import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
    ),
    "lc_id is wrong": (
        {
            ColumnEnum.str_lc_id.name: [np.nan, "lc_2", "lc_3"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.2, 35.3],
            ColumnEnum.lon.name: [140.2, 140.3],
            ColumnEnum.addr.name: ["addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 20],
            ColumnEnum.main_cylinder_num.name: [2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 20],
            ColumnEnum.sub_cylinder_num.name: [2, 1],
            ColumnEnum.str_staff_id.name: ["1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
    ),
    "lat is wrong": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.lat.name: [np.nan, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.2, 35.3],
            ColumnEnum.lon.name: [140.2, 140.3],
            ColumnEnum.addr.name: ["addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [50, 20],
            ColumnEnum.main_cylinder_num.name: [2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 20],
            ColumnEnum.sub_cylinder_num.name: [2, 1],
            ColumnEnum.str_staff_id.name: ["1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
    ),
    "cylinder_size is 0": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [0, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_2", "lc_3"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13", "m_21 m_22", "m_31"],
            ColumnEnum.lat.name: [35.1, 35.2, 35.3],
            ColumnEnum.lon.name: [140.1, 140.2, 140.3],
            ColumnEnum.addr.name: ["addr_1", "addr_2", "add_3"],
            ColumnEnum.main_cylinder_size.name: [0, 50, 20],
            ColumnEnum.main_cylinder_num.name: [1, 2, 1],
            ColumnEnum.sub_cylinder_size.name: [50, 50, 20],
            ColumnEnum.sub_cylinder_num.name: [1, 2, 1],
            ColumnEnum.str_staff_id.name: ["1", "1", "2"],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1", "w_1"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    df_lc_list = pd.DataFrame(data=request.param[0])
    df_lc_list = Schema_For_ISG_RawInputRepo().validate_df_lc_list_raw(df_lc_list)

    df_lc_and_meter = pd.DataFrame(data=request.param[1])
    df_lc_and_meter = Schema_For_ISG_RawInputRepo().validate_df_lc_and_meter(df_lc_and_meter)

    df_lc_attr = pd.DataFrame(data=request.param[2])
    df_lc_attr = Schema_For_ISG_RawInputRepo().validate_df_lc_attr(df_lc_attr)

    return df_lc_list, df_lc_and_meter, df_lc_attr
