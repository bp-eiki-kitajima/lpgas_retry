import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.ncu_id.name: ["10", "11", "12"],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.open_status.name: [np.nan, np.nan, np.nan],
            ColumnEnum.predict_target_flag.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.enable_prediction.name: [True, True, True],
        },
    ),
    "lc is not str": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: [np.nan, "1", np.nan],
            ColumnEnum.ncu_id.name: ["10", "11", "12"],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.open_status.name: [np.nan, np.nan, np.nan],
            ColumnEnum.predict_target_flag.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["1"],
            ColumnEnum.str_lc_id.name: ["1"],
            ColumnEnum.is_ncu.name: [True],
            ColumnEnum.template_number.name: ["101"],
            ColumnEnum.enable_prediction.name: [True],
        },
    ),
    "meter is not str": (
        {
            ColumnEnum.str_meter_id.name: [np.nan, "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.ncu_id.name: ["10", "11", "12"],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.open_status.name: [np.nan, np.nan, np.nan],
            ColumnEnum.predict_target_flag.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["1", "2"],
            ColumnEnum.str_lc_id.name: ["1", "2"],
            ColumnEnum.is_ncu.name: [True, True],
            ColumnEnum.template_number.name: ["101", "102"],
            ColumnEnum.enable_prediction.name: [True, True],
        },
    ),
    "template_number is not str": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.ncu_id.name: ["10", "11", "12"],
            ColumnEnum.template_number.name: [np.nan, "101", np.nan],
            ColumnEnum.open_status.name: [np.nan, np.nan, np.nan],
            ColumnEnum.predict_target_flag.name: [True, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["0", "101", "0"],
            ColumnEnum.enable_prediction.name: [True, True, True],
        },
    ),
    "predict_target_flag is not True": (
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.ncu_id.name: ["10", "11", "12"],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.open_status.name: [np.nan, np.nan, np.nan],
            ColumnEnum.predict_target_flag.name: [False, True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["0", "1", "2"],
            ColumnEnum.str_lc_id.name: ["0", "1", "2"],
            ColumnEnum.is_ncu.name: [True, True, True],
            ColumnEnum.template_number.name: ["100", "101", "102"],
            ColumnEnum.enable_prediction.name: [False, True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    df_joined_meter = pd.DataFrame(data=request.param[0])
    df_joined_meter[ColumnEnum.customer_code.name] = "999"
    df_joined_meter[ColumnEnum.atach_status.name] = "設置完了"

    df_joined_meter = Schema_For_ISG_RawInputRepo().validate_df_joined_meter_raw(df_joined_meter)
    df_meter_attr = pd.DataFrame(data=request.param[1])
    df_meter_attr = Schema_For_ISG_RawInputRepo().validate_df_meter_attr(df_meter_attr)

    return df_joined_meter, df_meter_attr
