import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.ncu_id.name: ["aaa", "aaa", "bbb", "bbb"],
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    df_meter_ncu = pd.DataFrame(data=request.param[0])
    df_ncu_meter_usage_value = pd.DataFrame(data=request.param[1])
    df_ncu_meter_usage_value = Schema_For_ISG_RawInputRepo().validate_df_ncu_meter_usage_value(
        df_ncu_meter_usage_value
    )

    return df_meter_ncu, df_ncu_meter_usage_value
