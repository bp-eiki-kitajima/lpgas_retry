import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1", "lc_2", "lc_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 45.0, 50.0, 45.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0, 50.0, 50.0],
            ColumnEnum.is_change.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, dict[LC_ID, DeliveryResultData]]:
    df_delivery_result_value = pd.DataFrame(data=request.param[0])
    df_delivery_result_value = Schema_For_ISG_RawInputRepo().validate_df_delivery_result_value(
        df_delivery_result_value
    )

    df_delivery_result_data = pd.DataFrame(data=request.param[1])
    dict_delivery_result_data = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data
    )

    return df_delivery_result_value, dict_delivery_result_data
