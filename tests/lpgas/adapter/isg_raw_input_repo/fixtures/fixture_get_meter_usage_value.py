import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.adapter.isg_raw_input_repo.schema import Schema_For_ISG_RawInputRepo
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
            ColumnEnum.meter_usage_type.name: ["4", "5", "4", "5"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.plug_type.name: ["1", "1", "1", "1"],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True, True],
            ColumnEnum.is_change.name: [False, True, False, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
    ),
    "meter is not str": (
        {
            ColumnEnum.str_meter_id.name: [np.nan, "100", "101", "101"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
            ColumnEnum.meter_usage_type.name: ["4", "5", "4", "5"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.plug_type.name: ["1", "1", "1", "1"],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [101.0, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_change.name: [True, False, True],
            ColumnEnum.is_read.name: [True, True, True],
        },
    ),
    "meter_reading_date cannot convert timestamp": (
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.date.name: ["2019/100/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
            ColumnEnum.meter_usage_type.name: ["4", "5", "4", "5"],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 10.0, 11.0],
            ColumnEnum.plug_type.name: ["1", "1", "1", "1"],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [101.0, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_change.name: [True, False, True],
            ColumnEnum.is_read.name: [True, True, True],
        },
    ),
    "meter_usage_value is not float": (
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.date.name: ["2019/1/1", "2019/1/2", "2019/1/1", "2019/1/2"],
            ColumnEnum.time.name: ["900", "1000", "900", "1000"],
            ColumnEnum.meter_usage_type.name: ["4", "5", "4", "5"],
            ColumnEnum.accum_usage.name: [np.nan, np.nan, 10.0, 11.0],
            ColumnEnum.plug_type.name: ["1", "1", "1", "1"],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["100", "100", "101", "101"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [np.nan, np.nan, 10.0, 11.0],
            ColumnEnum.is_open.name: [True, True, True, True],
            ColumnEnum.is_change.name: [False, True, False, True],
            ColumnEnum.is_read.name: [True, True, True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    df_meter_read = pd.DataFrame(data=request.param[0])
    df_meter_read[ColumnEnum.enterprise_id.name] = "10001"
    df_meter_read[ColumnEnum.str_lc_id.name] = "lc_1"
    df_meter_read = Schema_For_ISG_RawInputRepo().validate_df_meter_read_raw(df_meter_read)

    df_meter_usage_value = pd.DataFrame(data=request.param[1])
    df_meter_usage_value = Schema_For_ISG_RawInputRepo().validate_df_meter_usage_value(
        df_meter_usage_value
    )

    return df_meter_read, df_meter_usage_value
