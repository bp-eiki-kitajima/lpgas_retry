import pathlib

import pytest
from pytest_mock import MockerFixture

from lpgas.adapter.csv_cleansed_input_repo import Csv_CleansedInputRepo
from lpgas.domain.delivery_result_data import DeliveryResultDataFactory
from lpgas.domain.lc import LCFactory
from lpgas.domain.meter.meter import MeterFactory
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.domain.weather.weather_data import WeatherDataFactory
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def csv_cleansed_input_repo(domain_registry: DomainRegistry):
    cleansed_input_repo = domain_registry.cleansed_input_repo()
    if not isinstance(cleansed_input_repo, Csv_CleansedInputRepo):
        pytest.skip("this is not target repo for test")

    cleansed_input_repo.init_output()
    yield cleansed_input_repo
    cleansed_input_repo.init_output()


def test_store_dict_meter(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """Meterの保存ができるか"""

    p = tmp_path / "meter.csv"
    mocker.patch.object(
        csv_cleansed_input_repo, csv_cleansed_input_repo._get_meter_path.__name__, return_value=p
    )

    dict_meter = MeterFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_meter(dict_meter)
    assert p.exists()


def test_store_dict_lc(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """LCの保存ができるか"""

    p = tmp_path / "lc.csv"
    mocker.patch.object(
        csv_cleansed_input_repo, csv_cleansed_input_repo._get_lc_path.__name__, return_value=p
    )

    dict_lc = LCFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_lc(dict_lc)
    assert p.exists()


def test_store_dict_meter_usage_data(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """MeterUsageDataの保存ができるか"""

    p = tmp_path / "meter_usage_data.csv"
    mocker.patch.object(
        csv_cleansed_input_repo,
        csv_cleansed_input_repo._get_meter_usage_data_path.__name__,
        return_value=p,
    )

    dict_meter_usage_data = MeterUsageDataFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_meter_usage_data(dict_meter_usage_data)
    assert p.exists()


def test_store_dict_ncu_meter_usage_data(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """NcuMeterUsageDataの保存ができるか"""

    p = tmp_path / "ncu_meter_usage_data.csv"
    mocker.patch.object(
        csv_cleansed_input_repo,
        csv_cleansed_input_repo._get_ncu_meter_usage_data_path.__name__,
        return_value=p,
    )

    dict_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_ncu_meter_usage_data(dict_ncu_meter_usage_data)
    assert p.exists()


def test_store_delivery_result_data(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """DeliveryResultDataの保存ができるか"""

    p = tmp_path / "delivery_result_data.csv"
    mocker.patch.object(
        csv_cleansed_input_repo,
        csv_cleansed_input_repo._get_delivery_result_data_path.__name__,
        return_value=p,
    )

    dict_delivery_result_data = DeliveryResultDataFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_delivery_result_data(dict_delivery_result_data)
    assert p.exists()


def test_store_weather_data(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    csv_cleansed_input_repo: Csv_CleansedInputRepo,
) -> None:
    """WeatherDataの保存ができるか"""

    p = tmp_path / "weather_data.csv"
    mocker.patch.object(
        csv_cleansed_input_repo,
        csv_cleansed_input_repo._get_weather_data_path.__name__,
        return_value=p,
    )

    dict_weather_data = WeatherDataFactory().gene_dummy_dict()
    csv_cleansed_input_repo.store_dict_weather_data(dict_weather_data)
    assert p.exists()
