import pathlib

import pytest
from pytest_mock import MockerFixture

from lpgas.adapter.dp_results_repo import DPResultsRepo
from lpgas.domain.dp_results import DPResultsFactory
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def dp_results_repo(domain_registry: DomainRegistry):
    dp_results_repo = domain_registry.dp_results_repo()
    if not isinstance(dp_results_repo, DPResultsRepo):
        pytest.skip("this is not target repo for test")

    dp_results_repo.init_output()
    yield dp_results_repo
    dp_results_repo.init_output()


def test_store(tmp_path: pathlib.Path, mocker: MockerFixture, dp_results_repo: DPResultsRepo):
    p_meter_results = tmp_path / "dp_meter_results.csv"
    p_lc_results = tmp_path / "dp_lc_results.csv"
    mocker.patch.object(
        dp_results_repo,
        dp_results_repo._get_latest_dp_meter_results_path.__name__,
        return_value=p_meter_results,
    )
    mocker.patch.object(
        dp_results_repo,
        dp_results_repo._get_latest_dp_lc_results_path.__name__,
        return_value=p_lc_results,
    )

    dp_results = DPResultsFactory().gene_dummy_dp_results()
    dp_results_repo.store(dp_results)

    assert p_meter_results.exists()
    assert p_lc_results.exists()
