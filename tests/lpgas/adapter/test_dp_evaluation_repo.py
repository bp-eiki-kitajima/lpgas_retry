import pathlib

import pytest
from pytest_mock import MockerFixture

from lpgas.adapter.dp_evaluation_repo import DPEvaluationRepo
from lpgas.domain.dp_results import DPResultsFactory
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def dp_evaluation_repo(domain_registry: DomainRegistry):
    dp_evaluation_repo = domain_registry.dp_evaluation_repo()
    if not isinstance(dp_evaluation_repo, DPEvaluationRepo):
        pytest.skip("this is not target repo for test")

    dp_evaluation_repo.init_output()
    yield dp_evaluation_repo
    dp_evaluation_repo.init_output()


def test_store(
    tmp_path: pathlib.Path,
    mocker: MockerFixture,
    domain_registry: DomainRegistry,
    dp_evaluation_repo: DPEvaluationRepo,
):
    p_meter_results = tmp_path / "dp_meter_results_with_gt.csv"
    p_lc_results = tmp_path / "dp_lc_results_with_gt.csv"
    mocker.patch.object(
        dp_evaluation_repo,
        dp_evaluation_repo._get_latest_dp_meter_results_path.__name__,
        return_value=p_meter_results,
    )
    mocker.patch.object(
        dp_evaluation_repo,
        dp_evaluation_repo._get_latest_dp_lc_results_path.__name__,
        return_value=p_lc_results,
    )

    dp_results = DPResultsFactory().gene_dummy_dp_results()
    dp_results.attach_groundtruth(domain_registry.cleansed_input_repo())
    dp_evaluation_repo.store(dp_results)

    assert p_meter_results.exists()
    assert p_lc_results.exists()
