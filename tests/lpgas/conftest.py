import pytest
from _pytest.fixtures import SubRequest
from py.xml import html

from lpgas.utils.config_manager import (
    ConfigManager,
    ConfigManagerRepoCommand,
    ConfigManagerUsecaseCommand,
)
from lpgas.utils.domain_registry import DomainRegistry

params_cfg = {
    "isg": (
        ConfigManagerUsecaseCommand(
            raw_input_usecase_cfg_path="configs/usecase/RawInputUsecase.yaml",
            train_usecase_cfg_path="configs/usecase/TrainUsecase.yaml",
            predict_usecase_cfg_path="configs/usecase/PredictUsecase.yaml",
            evaluate_usecase_cfg_path="configs/usecase/EvaluateUsecase.yaml",
        ),
        ConfigManagerRepoCommand(
            raw_input_repo_cfg_path="configs/repo/RawInputRepo_simple.yaml",
            # raw_input_repo_cfg_path="configs/repo/RawInputRepo_simple2.yaml",
            # raw_input_repo_cfg_path="configs/repo/RawInputRepo_ishikawa.yaml",
            # raw_input_repo_cfg_path="configs/repo/RawInputRepo.yaml",
            cleansed_input_repo_cfg_path="configs/repo/CleansedInputRepo.yaml",
            dp_model_repo_cfg_path="configs/repo/DPModelRepo.yaml",
            dp_results_repo_cfg_path="configs/repo/DPResultsRepo.yaml",
            dp_evaluation_repo_cfg_path="configs/repo/DPEvaluationRepo.yaml",
        ),
    ),
}


@pytest.fixture(
    params=list(params_cfg.values()),
    ids=list(params_cfg.keys()),
)
def config_manager(request: SubRequest) -> ConfigManager:
    app_command = request.param[0]
    repo_command = request.param[1]
    config_manager = ConfigManager(usecase_command=app_command, repo_command=repo_command)
    return config_manager


@pytest.fixture
def domain_registry(config_manager: ConfigManager) -> DomainRegistry:
    return DomainRegistry(config_manager=config_manager)


def pytest_html_report_title(report):
    report.title = "テスト仕様書"


def pytest_html_results_table_header(cells):
    del cells[1]
    cells.insert(1, html.td("TestFolder"))
    cells.insert(2, html.td("TestScript"))
    cells.insert(3, html.td("TestFunction"))
    cells.insert(4, html.th("Description"))
    cells.insert(5, html.th("Parameter"))
    cells.pop()


def pytest_html_results_table_row(report, cells):
    del cells[1]
    cells.insert(1, html.td(report.test_folder))
    cells.insert(2, html.td(report.test_script))
    cells.insert(3, html.td(report.test_func))
    cells.insert(4, html.td(report.description))
    cells.insert(5, html.td(report.param))
    cells.pop()


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    report.test_folder = item.fspath.dirname
    report.test_script = item.fspath.basename
    report.test_func = item.originalname
    report.description = str(item.function.__doc__)
    report.param = item.name.removeprefix(item.originalname)
