from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo
from lpgas.utils.boudary.if_dp_evaluation_repo import IF_DPEvaluationRepo
from lpgas.utils.boudary.if_dp_model_repo import IF_DPModelRepo
from lpgas.utils.boudary.if_dp_results_repo import IF_DPResultsRepo
from lpgas.utils.boudary.if_evaluation_usecase import IF_EvaluateUsecase
from lpgas.utils.boudary.if_predict_usecase import IF_PredictUsecase
from lpgas.utils.boudary.if_raw_input_repo import IF_RawInputRepo
from lpgas.utils.boudary.if_raw_input_usecase import IF_RawInputUsecase
from lpgas.utils.boudary.if_train_usecase import IF_TrainUsecase
from lpgas.utils.config_manager import ConfigManager
from lpgas.utils.domain_registry import DomainRegistry


def test_domain_registry_initialize(config_manager: ConfigManager) -> None:
    """ConfigManagerを読み込んで, DomainRegistryの初期化できるか"""
    domain_registry = DomainRegistry(config_manager)
    assert isinstance(domain_registry, DomainRegistry)


# usecase
def test_usecase(domain_registry: DomainRegistry) -> None:
    """DomainRegistryがusecaseを正常に生成できるか"""

    raw_input_usecase = domain_registry.raw_input_usecase()
    assert isinstance(raw_input_usecase, IF_RawInputUsecase)

    train_usecase = domain_registry.train_usecase()
    assert isinstance(train_usecase, IF_TrainUsecase)

    predict_case = domain_registry.predict_usecase()
    assert isinstance(predict_case, IF_PredictUsecase)

    evaluate_case = domain_registry.evaluate_usecase()
    assert isinstance(evaluate_case, IF_EvaluateUsecase)


# repo
def test_repo(domain_registry: DomainRegistry) -> None:
    """DomainRegistryがrepoを正常に生成できるか"""

    raw_input_repo = domain_registry.raw_input_repo()
    assert isinstance(raw_input_repo, IF_RawInputRepo)

    dp_model_repo = domain_registry.dp_model_repo()
    assert isinstance(dp_model_repo, IF_DPModelRepo)

    cleansing_repo = domain_registry.cleansed_input_repo()
    assert isinstance(cleansing_repo, IF_CleansedInputRepo)

    dp_results_repo = domain_registry.dp_results_repo()
    assert isinstance(dp_results_repo, IF_DPResultsRepo)

    dp_evaluation_repo = domain_registry.dp_evaluation_repo()
    assert isinstance(dp_evaluation_repo, IF_DPEvaluationRepo)
