from lpgas.utils.config_manager import ConfigManager


def test_config_manager_initialize(config_manager: ConfigManager) -> None:
    """Configを読み込んで, ConfigManagerを初期化できるか"""
    assert type(config_manager) is ConfigManager


# usecase
def test_params_for_usecase(config_manager: ConfigManager) -> None:
    """usecase関連のパラメータが正しく読み込めているか"""

    classname, params = config_manager.params_for_raw_input_usecase()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_train_usecase()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_predict_usecase()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_evaluate_usecase()
    assert type(classname) is str and type(params) is dict


# repo
def test_params_for_repo(config_manager: ConfigManager) -> None:
    """repo関連のパラメータが正しく読み込めているか"""

    classname, params = config_manager.params_for_raw_input_repo()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_dp_model_repo()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_cleansed_input_repo()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_dp_results_repo()
    assert type(classname) is str and type(params) is dict

    classname, params = config_manager.params_for_dp_evaluation_repo()
    assert type(classname) is str and type(params) is dict
