import pandas as pd
import pytest
from pandas.testing import assert_frame_equal

from lpgas.domain.dp_results import DPResults, DPResultsFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_gene_from_args_ok = {
    "normal": (
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [
                    "100",
                    "100",
                    "101",
                    "101",
                    "200",
                    "200",
                    "201",
                    "201",
                ],
                ColumnEnum.str_lc_id.name: ["10", "10", "10", "10", "20", "20", "20", "20"],
                ColumnEnum.str_predict_start_dt.name: [
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                ],
                ColumnEnum.train_output_term.name: [0, 1, 0, 1, 0, 1, 0, 1],
                ColumnEnum.c_predict_kg.name: [10, 20, 100, 200, 10, 20, 100, 200],
                ColumnEnum.error_var.name: [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                ColumnEnum.str_last_change_dt.name: [
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                ],
            }
        ),
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [
                    "100",
                    "100",
                    "101",
                    "101",
                    "200",
                    "200",
                    "201",
                    "201",
                ],
                ColumnEnum.str_lc_id.name: ["10", "10", "10", "10", "20", "20", "20", "20"],
                ColumnEnum.str_predict_start_dt.name: [
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                ],
                ColumnEnum.train_output_term.name: [0, 1, 0, 1, 0, 1, 0, 1],
                ColumnEnum.c_predict_kg.name: [10, 20, 100, 200, 10, 20, 100, 200],
                ColumnEnum.error_var.name: [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                ColumnEnum.str_last_change_dt.name: [
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                ],
            }
        ),
        pd.DataFrame(
            {
                ColumnEnum.str_lc_id.name: ["10", "10", "20", "20"],
                ColumnEnum.list_str_meter_id.name: ["100 101", "100 101", "200 201", "200 201"],
                ColumnEnum.str_predict_start_dt.name: [
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                    "2021-1-1",
                ],
                ColumnEnum.train_output_term.name: [0, 1, 0, 1],
                ColumnEnum.c_predict_kg.name: [110, 220, 110, 220],
                ColumnEnum.error_var.name: [1.0, 1.0, 1.0, 1.0],
                ColumnEnum.str_last_change_dt.name: [
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                    "1900-1-1",
                ],
            }
        ),
    ),
}


@pytest.mark.parametrize(
    "df_dp_meter_results, expected_df_dp_meter_results, expected_df_dp_lc_results",
    list(params_gene_from_args_ok.values()),
    ids=list(params_gene_from_args_ok.keys()),
)
def test_gene_from_args_ok(
    df_dp_meter_results: pd.DataFrame,
    expected_df_dp_meter_results: pd.DataFrame,
    expected_df_dp_lc_results: pd.DataFrame,
) -> None:
    """正常な引数を渡してLCを生成できるか"""
    dp_results = DPResultsFactory().gene_dp_results(df_dp_meter_results)
    expected_df_dp_meter_results = Schema_For_Domain().validate_df_dp_meter_results(
        expected_df_dp_meter_results
    )
    expected_df_dp_lc_results = Schema_For_Domain().validate_df_dp_lc_results(
        expected_df_dp_lc_results
    )

    assert isinstance(dp_results, DPResults)
    assert_frame_equal(dp_results.dump_df_dp_meter_results(), expected_df_dp_meter_results)
    assert_frame_equal(dp_results.dump_df_dp_lc_results_results(), expected_df_dp_lc_results)


# TODO 異常系のテストかく
