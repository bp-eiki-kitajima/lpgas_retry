import pandas as pd
import pytest
from pydantic import ValidationError

from lpgas.domain.identifier import LC_ID, MeterID
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.utils.column_enum import ColumnEnum

params_gene_from_args = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: "100",
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.enable_prediction.name: True,
            ColumnEnum.is_ncu.name: True,
            ColumnEnum.template_number.name: "5",
        },
        Meter(
            meter_id=MeterID(id="100"),
            lc_id=LC_ID(id="100"),
            enable_prediction=True,
            is_ncu=True,
            template_number="5",
        ),
    ),
}


@pytest.mark.parametrize(
    "kwargs, expected_meter",
    list(params_gene_from_args.values()),
    ids=list(params_gene_from_args.keys()),
)
def test_gene_from_args_ok(kwargs, expected_meter: Meter) -> None:
    meter = MeterFactory().gene_from_args(**kwargs)
    assert meter == expected_meter


params_gene_list_from_df = {
    "normal": (
        pd.DataFrame(
            [
                {
                    ColumnEnum.str_meter_id.name: "100",
                    ColumnEnum.str_lc_id.name: "100",
                    ColumnEnum.enable_prediction.name: True,
                    ColumnEnum.is_ncu.name: True,
                    ColumnEnum.template_number.name: "5",
                }
            ]
        ),
        Meter(
            meter_id=MeterID(id="100"),
            lc_id=LC_ID(id="100"),
            enable_prediction=True,
            is_ncu=True,
            template_number="5",
        ),
    ),
}


@pytest.mark.parametrize(
    "df_meter, expected_meter",
    list(params_gene_list_from_df.values()),
    ids=list(params_gene_list_from_df.keys()),
)
def test_gene_list_from_df_ok(df_meter: pd.DataFrame, expected_meter: Meter) -> None:
    dict_meter = MeterFactory().gene_dict_from_df(df_meter)
    assert dict_meter == {expected_meter.meter_id: expected_meter}


params_gene_from_args_ng = {
    "mete_id is not meter_id": (
        {
            ColumnEnum.str_meter_id.name: MeterID(id="100"),
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.enable_prediction.name: True,
            ColumnEnum.is_ncu.name: True,
            ColumnEnum.template_number.name: "5",
        },
        ValidationError,
    ),
    "lc_id is not lc_id": (
        {
            ColumnEnum.str_meter_id.name: "100",
            ColumnEnum.str_lc_id.name: LC_ID(id="100"),
            ColumnEnum.enable_prediction.name: True,
            ColumnEnum.is_ncu.name: True,
            ColumnEnum.template_number.name: "5",
        },
        ValidationError,
    ),
    "enable_prediction is not bool": (
        {
            ColumnEnum.str_meter_id.name: "100",
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.enable_prediction.name: "TrueFalse",
            ColumnEnum.is_ncu.name: True,
            ColumnEnum.template_number.name: "5",
        },
        ValidationError,
    ),
    "is_ncu is not bool": (
        {
            ColumnEnum.str_meter_id.name: "100",
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.enable_prediction.name: True,
            ColumnEnum.is_ncu.name: "Trueaaa",
            ColumnEnum.template_number.name: "5",
        },
        ValidationError,
    ),
    "template number is not str": (
        {
            ColumnEnum.str_meter_id.name: "100",
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.enable_prediction.name: True,
            ColumnEnum.is_ncu.name: True,
            ColumnEnum.template_number.name: [5],
        },
        ValidationError,
    ),
}


@pytest.mark.parametrize(
    "kwargs, expected",
    list(params_gene_from_args_ng.values()),
    ids=list(params_gene_from_args_ng.keys()),
)
def test_gene_from_args_ng(kwargs, expected) -> None:
    with pytest.raises(expected):
        MeterFactory().gene_from_args(**kwargs)
