from typing import cast

import numpy as np
import pandas as pd
import pytest
from pandas.testing import assert_frame_equal
from pandera.errors import SchemaError
from pydantic import ValidationError

from lpgas.domain.identifier import MeterID
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_gene_from_args_ok = {
    "normal": (
        "100",
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [
                    "100",
                    "100",
                    "100",
                ],
                ColumnEnum.datetime.name: [
                    pd.Timestamp(year=2020, month=1, day=1, hour=9),
                    pd.Timestamp(year=2021, month=1, day=1, hour=9),
                    pd.Timestamp(year=2022, month=1, day=1, hour=9),
                ],
                ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0],
                ColumnEnum.is_open.name: [True, True, True],
                ColumnEnum.is_change.name: [True, True, True],
                ColumnEnum.is_read.name: [True, True, True],
            }
        ),
        MeterUsageData(
            meter_id=MeterID(id="100"),
            df_a_meter_usage_data=pd.DataFrame(
                {
                    ColumnEnum.str_meter_id.name: ["100", "100", "100"],
                    ColumnEnum.datetime.name: [
                        pd.Timestamp(year=2020, month=1, day=1, hour=9),
                        pd.Timestamp(year=2021, month=1, day=1, hour=9),
                        pd.Timestamp(year=2022, month=1, day=1, hour=9),
                    ],
                    ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0],
                    ColumnEnum.is_open.name: [True, True, True],
                    ColumnEnum.is_change.name: [True, True, True],
                    ColumnEnum.is_read.name: [True, True, True],
                }
            ),
            start_dt=cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=1, hour=9)),
            end_dt=cast(pd.Timestamp, pd.Timestamp(year=2022, month=1, day=1, hour=9)),
        ),
    ),
    "datetime has nan": (
        "100",
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: ["100", "100", "100"],
                ColumnEnum.datetime.name: [
                    pd.Timestamp(year=2020, month=1, day=1, hour=9),
                    np.nan,
                    pd.Timestamp(year=2022, month=1, day=1, hour=9),
                ],
                ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0],
                ColumnEnum.is_open.name: [True, True, True],
                ColumnEnum.is_change.name: [True, True, True],
                ColumnEnum.is_read.name: [True, True, True],
            }
        ),
        MeterUsageData(
            meter_id=MeterID(id="100"),
            df_a_meter_usage_data=pd.DataFrame(
                {
                    ColumnEnum.str_meter_id.name: ["100", "100"],
                    ColumnEnum.datetime.name: [
                        pd.Timestamp(year=2020, month=1, day=1, hour=9),
                        pd.Timestamp(year=2022, month=1, day=1, hour=9),
                    ],
                    ColumnEnum.accum_usage.name: [10.0, 12.0],
                    ColumnEnum.is_open.name: [True, True],
                    ColumnEnum.is_change.name: [True, True],
                    ColumnEnum.is_read.name: [True, True],
                }
            ),
            start_dt=cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=1, hour=9)),
            end_dt=cast(pd.Timestamp, pd.Timestamp(year=2022, month=1, day=1, hour=9)),
        ),
    ),
}


@pytest.mark.parametrize(
    "str_meter_id, df, expected_meter_usage_data",
    list(params_gene_from_args_ok.values()),
    ids=list(params_gene_from_args_ok.keys()),
)
def test_gene_from_args_ok(
    str_meter_id,
    df,
    expected_meter_usage_data: MeterUsageData,
) -> None:
    """正常な引数を渡した時, MeterUsageDataが生成されるか"""

    meter_usage_data = MeterUsageDataFactory().gene_from_args(
        str_meter_id=str_meter_id, df_a_meter_usage_data=df
    )

    assert meter_usage_data == expected_meter_usage_data


params_meter_usage_data_initialize_ng = {
    "len is 0": (
        "100",
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: [],
                ColumnEnum.datetime.name: [],
                ColumnEnum.accum_usage.name: [],
                ColumnEnum.is_open.name: [],
                ColumnEnum.is_change.name: [],
                ColumnEnum.is_read.name: [],
            }
        ),
        SchemaError,
    ),
    "meter_id is wrong": (
        "",
        pd.DataFrame(
            {
                ColumnEnum.str_meter_id.name: ["", "", ""],
                ColumnEnum.datetime.name: [
                    pd.Timestamp(year=2020, month=1, day=1, hour=9),
                    pd.Timestamp(year=2021, month=1, day=1, hour=9),
                    pd.Timestamp(year=2022, month=1, day=1, hour=9),
                ],
                ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0],
                ColumnEnum.is_open.name: [True, True, True],
                ColumnEnum.is_change.name: [True, True, True],
                ColumnEnum.is_read.name: [True, True, True],
            }
        ),
        ValidationError,
    ),
}


@pytest.mark.parametrize(
    "str_meter_id, df, expected_exception",
    list(params_meter_usage_data_initialize_ng.values()),
    ids=list(params_meter_usage_data_initialize_ng.keys()),
)
def test_meter_usage_data_initialize_ng(
    str_meter_id: str,
    df,
    expected_exception,
) -> None:
    """異常な引数を渡した時, MeterUsageDataの生成で想定のエラーが発生するか"""

    with pytest.raises(expected_exception):
        MeterUsageDataFactory().gene_from_args(str_meter_id=str_meter_id, df_a_meter_usage_data=df)


def test_dump_df() -> None:
    """MeterUsageDataをdfに変換できるか"""
    meter_usage_data = MeterUsageDataFactory().gene_dummy(reading_num=3)
    df_meter_usage_data = MeterUsageDataFactory().dump_df(meter_usage_data)
    expected_df = pd.DataFrame(
        {
            ColumnEnum.str_meter_id.name: ["dummy_m1", "dummy_m1", "dummy_m1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=1, day=1, hour=9),
                pd.Timestamp(year=2020, month=1, day=2, hour=9),
                pd.Timestamp(year=2020, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0],
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_change.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
        }
    )

    assert_frame_equal(df_meter_usage_data, expected_df)


def test_dump_df_from_dict() -> None:
    """MeterUsageDataのlistをdfに変換できるか"""
    dict_meter_usage_data = MeterUsageDataFactory().gene_dummy_dict(meter_num=3, reading_num=3)
    df_meter_usage_data = MeterUsageDataFactory().dump_df_from_dict(dict_meter_usage_data)
    expected_df = pd.DataFrame(
        {
            ColumnEnum.str_meter_id.name: [
                "dummy_m1",
                "dummy_m1",
                "dummy_m1",
                "dummy_m2",
                "dummy_m2",
                "dummy_m2",
                "dummy_m3",
                "dummy_m3",
                "dummy_m3",
            ],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=1, day=1, hour=9),
                pd.Timestamp(year=2020, month=1, day=2, hour=9),
                pd.Timestamp(year=2020, month=1, day=3, hour=9),
                pd.Timestamp(year=2020, month=1, day=1, hour=9),
                pd.Timestamp(year=2020, month=1, day=2, hour=9),
                pd.Timestamp(year=2020, month=1, day=3, hour=9),
                pd.Timestamp(year=2020, month=1, day=1, hour=9),
                pd.Timestamp(year=2020, month=1, day=2, hour=9),
                pd.Timestamp(year=2020, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [
                10.0,
                11.0,
                12.0,
                10.0,
                11.0,
                12.0,
                10.0,
                11.0,
                12.0,
            ],
            ColumnEnum.is_open.name: [
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
            ],
            ColumnEnum.is_change.name: [
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
            ],
            ColumnEnum.is_read.name: [
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
                True,
            ],
        }
    )

    assert_frame_equal(df_meter_usage_data, expected_df)
