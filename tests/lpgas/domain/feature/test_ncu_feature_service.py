from typing import cast

import pandas as pd
import pytest
from pytest_mock import MockerFixture

from lpgas.domain.feature.ncu_feature_service import NCU_FeatureService
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.domain_registry import DomainRegistry


def test_gene_train_feature(mocker: MockerFixture, domain_registry: DomainRegistry):

    train_end_dt = cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=8, hour=0))
    dummy_start_dt = cast(pd.Timestamp, train_end_dt - pd.Timedelta(days=90))

    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dummy_dict(
        meter_num=5, reading_num=90, start_dt=dummy_start_dt
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_ncu_meter_usage_data.__name__,
        return_value=dict_dummy_ncu_meter_usage_data,
    )

    df_train, df_val, list_feature_name, target_name = NCU_FeatureService().gene_train_val_feature(
        cleansed_input_repo=cleansed_input_repo,
        recent_term=6,
        week_term=4,
        dayofweek_term=4,
        train_sample_num=12,
        val_sample_num=3,
        end_dt=train_end_dt,
    )


def test_make_list_a_recent_feature_ok(fixture_make_list_a_recent_feature_ok):
    (
        df_with_diff,
        end_dt,
        num_recent_days,
        list_a_recent_feature_expected,
    ) = fixture_make_list_a_recent_feature_ok

    list_a_recent_feature_result = NCU_FeatureService()._make_list_a_recent_feature(
        df_with_diff=df_with_diff, end_dt=end_dt, num_recent_days=num_recent_days
    )

    assert list_a_recent_feature_result == list_a_recent_feature_expected


def test_make_list_a_recent_feature_ng(fixture_make_list_a_recent_feature_ng):
    (df_with_diff, end_dt, num_recent_days) = fixture_make_list_a_recent_feature_ng

    with pytest.raises(ValueError):
        NCU_FeatureService()._make_list_a_recent_feature(
            df_with_diff=df_with_diff, end_dt=end_dt, num_recent_days=num_recent_days
        )


def test_make_list_a_week_feature_ok(fixture_make_list_a_week_feature_ok):
    (
        df_with_diff,
        end_dt,
        num_week,
        list_a_week_feature_expected,
    ) = fixture_make_list_a_week_feature_ok

    list_a_week_feature_result = NCU_FeatureService()._make_list_a_week_feature(
        df_with_diff=df_with_diff, end_dt=end_dt, num_week=num_week
    )

    assert list_a_week_feature_result == list_a_week_feature_expected


def test_make_list_a_dayofweek_feature_ok(fixture_make_list_a_dayofweek_feature_ok):
    (
        df_with_diff,
        end_dt,
        num_week,
        list_a_dayofweek_feature_expected,
    ) = fixture_make_list_a_dayofweek_feature_ok

    list_a_dayofweek_feature_result = NCU_FeatureService()._make_list_a_dayofweek_feature(
        df_with_diff=df_with_diff, end_dt=end_dt, num_week=num_week
    )

    assert list_a_dayofweek_feature_result == list_a_dayofweek_feature_expected


# TODO 異常系のテストかく
