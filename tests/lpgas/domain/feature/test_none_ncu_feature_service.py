from typing import cast

import pandas as pd
from pytest_mock import MockerFixture

from lpgas.domain.feature.none_ncu_feature_service import NoneNCU_FeatureService
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.utils.domain_registry import DomainRegistry


def test_gene_train_feature(mocker: MockerFixture, domain_registry: DomainRegistry):

    test_end_dt = cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=8, hour=0))
    dummy_start_dt = cast(pd.Timestamp, test_end_dt - pd.Timedelta(days=90))

    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_meter_usage_data = MeterUsageDataFactory().gene_dummy_dict(
        meter_num=10, reading_num=90, start_dt=dummy_start_dt
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_meter_usage_data.__name__,
        return_value=dict_dummy_meter_usage_data,
    )

    (
        df_train,
        df_val,
        list_feature_name,
        target_name,
    ) = NoneNCU_FeatureService().gene_train_val_feature(
        cleansed_input_repo=cleansed_input_repo,
        recent_term=1,
        train_sample_num=1,
        val_sample_num=1,
        end_dt=test_end_dt,
    )


def test_gene_test_feature(mocker: MockerFixture, domain_registry: DomainRegistry):

    test_end_dt = cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=8, hour=0))
    dummy_start_dt = cast(pd.Timestamp, test_end_dt - pd.Timedelta(days=90))

    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_meter_usage_data = MeterUsageDataFactory().gene_dummy_dict(
        meter_num=10, reading_num=90, start_dt=dummy_start_dt
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_meter_usage_data.__name__,
        return_value=dict_dummy_meter_usage_data,
    )

    df_test, list_feature_name, target_name = NoneNCU_FeatureService().gene_test_feature(
        cleansed_input_repo=cleansed_input_repo,
        recent_term=3,
        end_dt=test_end_dt,
    )
