from pandas.testing import assert_frame_equal

from lpgas.domain.feature.feature_util import FeatureUtil


def test_make_single_interpolation_to_df_ok(
    fixture_make_single_interpolation_to_df_ok,
):
    (
        df_a_ncu_meter_usage_data,
        end_dt,
        df_a_ncu_meter_usage_data_expected,
    ) = fixture_make_single_interpolation_to_df_ok

    df_a_ncu_meter_usage_data_result = FeatureUtil().make_interpolate_to_df(
        df_a_ncu_meter_usage_data, end_dt
    )

    assert_frame_equal(
        df_a_ncu_meter_usage_data_result, df_a_ncu_meter_usage_data_expected, check_like=True
    )


def test_attached_diff_to_df_for_ncu_ok(
    fixture_attached_diff_to_df_for_ncu_ok,
):
    (
        df_a_ncu_meter_usage_data,
        df_a_ncu_meter_usage_data_expected,
    ) = fixture_attached_diff_to_df_for_ncu_ok

    df_a_ncu_meter_usage_data_result = FeatureUtil().attached_diff_to_df(df_a_ncu_meter_usage_data)

    assert_frame_equal(
        df_a_ncu_meter_usage_data_result, df_a_ncu_meter_usage_data_expected, check_like=True
    )


def test_attached_diff_to_df_for_none_ncu_ok(
    fixture_attached_diff_to_df_for_none_ncu_ok,
):
    (
        df_a_meter_usage_data,
        df_a_meter_usage_data_expected,
    ) = fixture_attached_diff_to_df_for_none_ncu_ok

    df_a_meter_usage_data_result = FeatureUtil().attached_diff_to_df(df_a_meter_usage_data)

    assert_frame_equal(
        df_a_meter_usage_data_result, df_a_meter_usage_data_expected, check_like=True
    )
