import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, normal": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0],
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, False],
        },
        {
            ColumnEnum.diff_usage.name: [0.0, 2.0],
            ColumnEnum.diff_day_num.name: [0, 1.0],
            ColumnEnum.daily_usage.name: [0.0, 2.0],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
):

    df_a_meter_usage_data = pd.DataFrame(data=request.param[0])
    df_a_meter_usage_data = Schema_For_Domain().validate_df_a_meter_usage_data(
        df_a_meter_usage_data
    )

    df_diff_only = pd.DataFrame(data=request.param[1])
    df_a_meter_usage_data_expected = pd.concat([df_a_meter_usage_data, df_diff_only], axis=1)

    return df_a_meter_usage_data, df_a_meter_usage_data_expected
