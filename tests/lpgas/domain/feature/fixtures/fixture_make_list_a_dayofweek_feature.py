import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, normal": (
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=12, day=16, hour=0),
                pd.Timestamp(year=2020, month=12, day=17, hour=0),  # 2 week ago
                pd.Timestamp(year=2020, month=12, day=18, hour=0),
                pd.Timestamp(year=2020, month=12, day=19, hour=0),
                pd.Timestamp(year=2020, month=12, day=20, hour=0),
                pd.Timestamp(year=2020, month=12, day=21, hour=0),
                pd.Timestamp(year=2020, month=12, day=22, hour=0),
                pd.Timestamp(year=2020, month=12, day=23, hour=0),
                pd.Timestamp(year=2020, month=12, day=24, hour=0),  # 1 week ago
                pd.Timestamp(year=2020, month=12, day=25, hour=0),
                pd.Timestamp(year=2020, month=12, day=26, hour=0),
                pd.Timestamp(year=2020, month=12, day=27, hour=0),
                pd.Timestamp(year=2020, month=12, day=28, hour=0),
                pd.Timestamp(year=2020, month=12, day=29, hour=0),
                pd.Timestamp(year=2020, month=12, day=30, hour=0),
                pd.Timestamp(year=2020, month=12, day=31, hour=0),  # target
            ],
            ColumnEnum.str_meter_id.name: ["meter_id" for i in range(16)],
            ColumnEnum.accum_usage.name: [
                10.0,
                11.1,  # 2 week ago
                12.3,
                13.6,
                15.0,
                16.5,
                18.1,
                19.8,
                21.6,  # 1 week ago
                23.5,
                25.5,
                27.6,
                29.8,
                32.1,
                34.5,
                37.0,  # target
            ],
            ColumnEnum.diff_usage.name: [
                1.0,
                1.1,  # 2 week ago
                1.2,
                1.3,
                1.4,
                1.5,
                1.6,
                1.7,
                1.8,  # 1 week ago
                1.9,
                2.0,
                2.1,
                2.2,
                2.3,
                2.4,
                2.5,  # target
            ],
            ColumnEnum.diff_day_num.name: [
                1.0,
                1.0,  # 2 week ago
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,  # 1 week ago
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,
                1.0,  # target
            ],
            ColumnEnum.daily_usage.name: [
                1.0,
                1.1,  # 2 week ago
                1.2,
                1.3,
                1.4,
                1.5,
                1.6,
                1.7,
                1.8,  # 1 week ago
                1.9,
                2.0,
                2.1,
                2.2,
                2.3,
                2.4,
                2.5,  # target
            ],
        },
        pd.Timestamp(year=2020, month=12, day=31, hour=0),
        2,
        [1.1, 1.8, 1.45],
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
):

    df_with_diff = pd.DataFrame(data=request.param[0])
    end_dt = request.param[1]
    num_week = request.param[2]
    list_a_dayofweek_feature_expected = request.param[3]

    return df_with_diff, end_dt, num_week, list_a_dayofweek_feature_expected
