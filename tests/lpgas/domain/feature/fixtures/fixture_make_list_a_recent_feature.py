import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, normal": (
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=12, day=28, hour=0),
                pd.Timestamp(year=2020, month=12, day=29, hour=0),
                pd.Timestamp(year=2020, month=12, day=30, hour=0),
                pd.Timestamp(year=2020, month=12, day=31, hour=0),
            ],
            ColumnEnum.str_meter_id.name: ["meter_id", "meter_id", "meter_id", "meter_id"],
            ColumnEnum.accum_usage.name: [100.0, 100.2, 100.5, 100.9],
            ColumnEnum.diff_usage.name: [0.1, 0.2, 0.3, 0.4],
            ColumnEnum.diff_day_num.name: [1.0, 1.0, 1.0, 1.0],
            ColumnEnum.daily_usage.name: [0.1, 0.2, 0.3, 0.4],
        },
        pd.Timestamp(year=2020, month=12, day=31, hour=0),
        2,
        [0.2, 0.3],
    ),
    "2, normal": (
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=12, day=28, hour=0),
                pd.Timestamp(year=2020, month=12, day=29, hour=0),
                pd.Timestamp(year=2020, month=12, day=30, hour=0),
                pd.Timestamp(year=2020, month=12, day=31, hour=0),
            ],
            ColumnEnum.str_meter_id.name: ["meter_id", "meter_id", "meter_id", "meter_id"],
            ColumnEnum.accum_usage.name: [100.0, 100.2, 100.5, 100.9],
            ColumnEnum.diff_usage.name: [0.1, 0.2, 0.3, 0.4],
            ColumnEnum.diff_day_num.name: [1.0, 1.0, 1.0, 1.0],
            ColumnEnum.daily_usage.name: [0.1, 0.2, 0.3, 0.4],
        },
        pd.Timestamp(year=2020, month=12, day=30, hour=0),
        2,
        [0.1, 0.2],
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
):

    df_with_diff = pd.DataFrame(data=request.param[0])
    end_dt = request.param[1]
    num_recent_days = request.param[2]
    list_a_recent_feature_expected = request.param[3]

    return df_with_diff, end_dt, num_recent_days, list_a_recent_feature_expected


params_ng = {
    "1, not enough": (
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2020, month=12, day=28, hour=0),
                pd.Timestamp(year=2020, month=12, day=29, hour=0),
                pd.Timestamp(year=2020, month=12, day=30, hour=0),
                pd.Timestamp(year=2020, month=12, day=31, hour=0),
            ],
            ColumnEnum.str_meter_id.name: ["meter_id", "meter_id", "meter_id", "meter_id"],
            ColumnEnum.accum_usage.name: [100.0, 100.2, 100.5, 100.9],
            ColumnEnum.diff_usage.name: [0.1, 0.2, 0.3, 0.4],
            ColumnEnum.diff_day_num.name: [1.0, 1.0, 1.0, 1.0],
            ColumnEnum.daily_usage.name: [0.1, 0.2, 0.3, 0.4],
        },
        pd.Timestamp(year=2020, month=12, day=31, hour=0),
        4,
    ),
}


@pytest.fixture(
    params=params_ng.values(),
    ids=params_ng.keys(),
)
def fixture_ng(
    request: SubRequest,
):

    df_with_diff = pd.DataFrame(data=request.param[0])
    end_dt = request.param[1]
    num_recent_days = request.param[2]

    return df_with_diff, end_dt, num_recent_days
