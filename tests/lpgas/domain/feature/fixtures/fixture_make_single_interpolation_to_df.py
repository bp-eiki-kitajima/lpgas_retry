import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, start_dt>=end_dt": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=4, hour=0),
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0],
        },
        pd.Timestamp(2018, 1, 1),  # start_dt>=end_dt
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=4, hour=0),
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0],
        },
    ),
    "2, single missing, middle": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=3, hour=0),  # 単数欠損, 中
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0],
        },
        pd.Timestamp(2019, 1, 4),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),  # 時系列補間
                pd.Timestamp(year=2019, month=1, day=3, hour=0),
                pd.Timestamp(year=2019, month=1, day=4, hour=0),  # 時系列補間
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0, 12.0, np.nan],  # 単数補完, 中
        },
    ),
    "3, single missing, after": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),  # 単数欠損, 後
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0],
        },
        pd.Timestamp(2019, 1, 3),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),
                pd.Timestamp(year=2019, month=1, day=3, hour=0),  # 時系列補間
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0, np.nan],  # 補間なし, 後
        },
    ),
    "4, not single missing, middle": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),  # 単数欠損なし, 中
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0],
        },
        pd.Timestamp(2019, 1, 4),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),
                pd.Timestamp(year=2019, month=1, day=3, hour=0),  # 時系列補間
                pd.Timestamp(year=2019, month=1, day=4, hour=0),  # 時系列補間
            ],
            ColumnEnum.accum_usage.name: [10.0, 12.0, np.nan, np.nan],  # 補完なし, 中
        },
    ),
    "5, series missing, middle": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=4, hour=0),  # 連続欠損, 中
            ],
            ColumnEnum.accum_usage.name: [10.0, 13.0],
        },
        pd.Timestamp(2019, 1, 4),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),  # 時系列補間
                pd.Timestamp(year=2019, month=1, day=3, hour=0),  # 時系列補間
                pd.Timestamp(year=2019, month=1, day=4, hour=0),
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0, np.nan, 13.0],  # 単数補間, 中
        },
    ),
    "6, series missing, after": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),  # 連続欠損, 後
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0],
        },
        pd.Timestamp(2019, 1, 4),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=0),
                pd.Timestamp(year=2019, month=1, day=2, hour=0),
                pd.Timestamp(year=2019, month=1, day=3, hour=0),  # 時系列補間
                pd.Timestamp(year=2019, month=1, day=4, hour=0),  # 時系列補間
            ],
            ColumnEnum.accum_usage.name: [10.0, 11.0, np.nan, np.nan],  # 補間なし
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
):

    df_a_ncu_meter_usage_data = pd.DataFrame(data=request.param[0])
    df_a_ncu_meter_usage_data = Schema_For_Domain().validate_df_a_ncu_meter_usage_data(
        df_a_ncu_meter_usage_data
    )

    end_dt = request.param[1]

    df_a_ncu_meter_usage_data_expected = pd.DataFrame(data=request.param[2])
    df_a_ncu_meter_usage_data_expected = Schema_For_Domain().validate_df_a_ncu_meter_usage_data(
        df_a_ncu_meter_usage_data_expected
    )

    return df_a_ncu_meter_usage_data, end_dt, df_a_ncu_meter_usage_data_expected
