import numpy as np
import pandas as pd
import pytest
from pandas.testing import assert_frame_equal
from pydantic import ValidationError

from lpgas.domain.lc import LCFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_gene_from_args_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.list_str_meter_id.name: ["10", "11"],
            ColumnEnum.lat.name: 30.0,
            ColumnEnum.lon.name: 130.0,
            ColumnEnum.addr.name: "addr",
            ColumnEnum.main_cylinder_size.name: 30.0,
            ColumnEnum.main_cylinder_num.name: 2,
            ColumnEnum.sub_cylinder_size.name: 30.0,
            ColumnEnum.sub_cylinder_num.name: 2,
            ColumnEnum.str_staff_id.name: "1",
            ColumnEnum.str_weather_city_id.name: "1",
        },
        pd.DataFrame(
            [
                {
                    ColumnEnum.str_lc_id.name: "100",
                    ColumnEnum.list_str_meter_id.name: "10 11",
                    ColumnEnum.lat.name: 30.0,
                    ColumnEnum.lon.name: 130.0,
                    ColumnEnum.addr.name: "addr",
                    ColumnEnum.main_cylinder_size.name: 30.0,
                    ColumnEnum.main_cylinder_num.name: 2,
                    ColumnEnum.sub_cylinder_size.name: 30.0,
                    ColumnEnum.sub_cylinder_num.name: 2,
                    ColumnEnum.str_staff_id.name: "1",
                    ColumnEnum.str_weather_city_id.name: "1",
                }
            ]
        ),
    ),
}


@pytest.mark.parametrize(
    "kwargs, df_expected",
    list(params_gene_from_args_ok.values()),
    ids=list(params_gene_from_args_ok.keys()),
)
def test_gene_from_args_ok(kwargs, df_expected) -> None:
    """正常な引数を渡してLCを生成できるか"""
    df_expected = Schema_For_Domain().validate_df_lc(df_expected)

    lc = LCFactory().gene_from_args(**kwargs)
    assert_frame_equal(LCFactory().dump_df(lc), df_expected)


params_gene_from_args_ng = {
    "str_lc_id is empty": (
        {
            ColumnEnum.str_lc_id.name: "",
            ColumnEnum.list_str_meter_id.name: "10 11",
            ColumnEnum.lat.name: 30.0,
            ColumnEnum.lon.name: 130.0,
            ColumnEnum.addr.name: "addr",
            ColumnEnum.main_cylinder_size.name: 30.0,
            ColumnEnum.main_cylinder_num.name: 2,
            ColumnEnum.sub_cylinder_size.name: 30.0,
            ColumnEnum.sub_cylinder_num.name: 2,
            ColumnEnum.str_staff_id.name: "1",
            ColumnEnum.str_weather_city_id.name: "1",
        },
        ValidationError,
    ),
    "list_meter_id is empty": (
        {
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.list_str_meter_id.name: "",
            ColumnEnum.lat.name: 30.0,
            ColumnEnum.lon.name: 130.0,
            ColumnEnum.addr.name: "addr",
            ColumnEnum.main_cylinder_size.name: 30.0,
            ColumnEnum.main_cylinder_num.name: 2,
            ColumnEnum.sub_cylinder_size.name: 30.0,
            ColumnEnum.sub_cylinder_num.name: 2,
            ColumnEnum.str_staff_id.name: "1",
            ColumnEnum.str_weather_city_id.name: "1",
        },
        ValidationError,
    ),
    "lat is wrong": (
        {
            ColumnEnum.str_lc_id.name: "100",
            ColumnEnum.list_str_meter_id.name: "10 11",
            ColumnEnum.lat.name: np.nan,
            ColumnEnum.lon.name: 130.0,
            ColumnEnum.addr.name: "addr",
            ColumnEnum.main_cylinder_size.name: 30.0,
            ColumnEnum.main_cylinder_num.name: 2,
            ColumnEnum.sub_cylinder_size.name: 30.0,
            ColumnEnum.sub_cylinder_num.name: 2,
            ColumnEnum.str_staff_id.name: "1",
            ColumnEnum.str_weather_city_id.name: "1",
        },
        ValidationError,
    ),
}


@pytest.mark.parametrize(
    "kwargs, expected_error",
    list(params_gene_from_args_ng.values()),
    ids=list(params_gene_from_args_ng.keys()),
)
def test_gene_from_args_ng(kwargs, expected_error) -> None:
    """異常な引数を渡した時, LCの生成でエラーが発生するか"""

    with pytest.raises(expected_error):
        LCFactory().gene_from_args(**kwargs)
