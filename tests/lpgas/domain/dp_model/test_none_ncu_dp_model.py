import pytest
from pytest_mock import MockerFixture

from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.domain.dp_model.none_ncu_dp_model import NoneNCU_DPModel
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.utils.boudary.if_dp_model import IF_DPModel
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def none_ncu_dp_model() -> IF_DPModel:
    none_ncu_dp_model = DPModelFactory().gene_dummy_none_ncu_dpmodel()
    if not isinstance(none_ncu_dp_model, NoneNCU_DPModel):
        pytest.skip("this is not target object for test")
    return none_ncu_dp_model


def test_train(
    mocker: MockerFixture, domain_registry: DomainRegistry, none_ncu_dp_model: NoneNCU_DPModel
):
    """NoneNCU_DPModelで学習できるか"""
    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_meter_usage_data = MeterUsageDataFactory().gene_dummy_dict()
    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_meter_usage_data.__name__,
        return_value=dict_dummy_meter_usage_data,
    )

    none_ncu_dp_model.train(cleansed_input_repo=cleansed_input_repo)
    assert isinstance(none_ncu_dp_model, NoneNCU_DPModel)
