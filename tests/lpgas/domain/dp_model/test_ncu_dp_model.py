from typing import cast

import pandas as pd
import pytest
from pytest_mock import MockerFixture

from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.domain.dp_model.ncu_dp_model import NCU_DPModel
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def ncu_dp_model() -> NCU_DPModel:
    ncu_dp_model = DPModelFactory().gene_dummy_ncu_dpmodel()
    if not isinstance(ncu_dp_model, NCU_DPModel):
        pytest.skip("this is not target object for test")
    return ncu_dp_model


def test_train(mocker: MockerFixture, domain_registry: DomainRegistry, ncu_dp_model: NCU_DPModel):
    """NCU_DPModelで学習できるか"""
    train_end_dt = cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=8, hour=0))
    dummy_start_dt = cast(pd.Timestamp, train_end_dt - pd.Timedelta(days=90))

    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dummy_dict(
        meter_num=3, reading_num=90, start_dt=dummy_start_dt
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_ncu_meter_usage_data.__name__,
        return_value=dict_dummy_ncu_meter_usage_data,
    )

    mocker.patch.object(
        ncu_dp_model.command,
        ncu_dp_model.command.get_train_end_dt.__name__,
        return_value=train_end_dt,
    )

    ncu_dp_model.train(cleansed_input_repo=cleansed_input_repo)
    assert isinstance(ncu_dp_model, NCU_DPModel)
    assert ncu_dp_model.command.ncu_raw_model is not None
