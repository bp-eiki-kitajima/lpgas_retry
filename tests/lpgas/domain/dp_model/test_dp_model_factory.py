import pytest

from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.domain.dp_model.isg_dp_model import ISG_DPModel
from lpgas.domain.dp_model.ncu_dp_model import NCU_DPModel
from lpgas.domain.dp_model.none_ncu_dp_model import NoneNCU_DPModel
from lpgas.utils.column_enum import ColumnEnum

params_gene_from_args_ok = {
    ISG_DPModel.__name__: (
        ISG_DPModel,
        {
            ColumnEnum.str_train_end_dt.name: "2021-1-1",
            ColumnEnum.train_output_term.name: 15,
            ColumnEnum.ncu_model_recent_term.name: 6,
            ColumnEnum.ncu_model_week_term.name: 4,
            ColumnEnum.ncu_model_dayofweek_term.name: 4,
            ColumnEnum.ncu_model_train_sample_num.name: 3,
            ColumnEnum.ncu_model_val_sample_num.name: 2,
            ColumnEnum.none_ncu_model_recent_term.name: 6,
            ColumnEnum.none_ncu_model_train_sample_num.name: 3,
            ColumnEnum.none_ncu_model_val_sample_num.name: 2,
            ColumnEnum.ncu_raw_model.name: None,
            ColumnEnum.none_ncu_raw_model.name: None,
        },
    ),
    NCU_DPModel.__name__: (
        NCU_DPModel,
        {
            ColumnEnum.str_train_end_dt.name: "2021-1-1",
            ColumnEnum.train_output_term.name: 15,
            ColumnEnum.ncu_model_recent_term.name: 6,
            ColumnEnum.ncu_model_week_term.name: 4,
            ColumnEnum.ncu_model_dayofweek_term.name: 4,
            ColumnEnum.ncu_model_train_sample_num.name: 3,
            ColumnEnum.ncu_model_val_sample_num.name: 2,
            ColumnEnum.ncu_raw_model.name: None,
        },
    ),
    NoneNCU_DPModel.__name__: (
        NoneNCU_DPModel,
        {
            ColumnEnum.str_train_end_dt.name: "2021-1-1",
            ColumnEnum.train_output_term.name: 15,
            ColumnEnum.none_ncu_model_recent_term.name: 6,
            ColumnEnum.none_ncu_model_train_sample_num.name: 3,
            ColumnEnum.none_ncu_model_val_sample_num.name: 2,
            ColumnEnum.none_ncu_raw_model.name: None,
        },
    ),
}


@pytest.mark.parametrize(
    "dp_model_class, kwargs",
    list(params_gene_from_args_ok.values()),
    ids=list(params_gene_from_args_ok.keys()),
)
def test_gene_from_args_ok(dp_model_class: type, kwargs) -> None:
    """正常な引数を渡した時, DPModelの生成されるか"""
    dp_model = DPModelFactory().gene_from_args(
        dp_model_classname=dp_model_class.__name__, **kwargs
    )
    assert isinstance(dp_model, dp_model_class)


def test_gene_from_args_ng() -> None:
    """異常な引数を渡した時, DPModelの生成されるか"""
    kwargs = {"tmp": 100}

    class Unknown_DPModel:
        pass

    with pytest.raises(ValueError):
        DPModelFactory().gene_from_args(dp_model_classname=Unknown_DPModel.__name__, **kwargs)
