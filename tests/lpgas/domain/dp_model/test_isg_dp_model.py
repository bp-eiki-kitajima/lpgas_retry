from typing import cast

import pandas as pd
import pytest
from pytest_mock import MockerFixture

from lpgas.domain.dp_model.dp_model_factory import DPModelFactory
from lpgas.domain.dp_model.isg_dp_model import ISG_DPModel
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.ncu_meter_usage_data.ncu_meter_usage_data_factory import NcuMeterUsageDataFactory
from lpgas.utils.domain_registry import DomainRegistry


@pytest.fixture
def isg_dp_model() -> ISG_DPModel:
    isg_dp_model = DPModelFactory().gene_dummy_isg_dp_model()
    if not isinstance(isg_dp_model, ISG_DPModel):
        pytest.skip("this is not target object for test")
    return isg_dp_model


def test_train(mocker: MockerFixture, domain_registry: DomainRegistry, isg_dp_model: ISG_DPModel):
    """ISG_DPModelで学習できるか"""
    train_end_dt = cast(pd.Timestamp, pd.Timestamp(year=2020, month=1, day=8, hour=0))
    dummy_start_dt = cast(pd.Timestamp, train_end_dt - pd.Timedelta(days=90))

    cleansed_input_repo = domain_registry.cleansed_input_repo()
    dict_dummy_meter_usage_date = MeterUsageDataFactory().gene_dummy_dict(
        meter_num=10, reading_num=90, start_dt=dummy_start_dt
    )
    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_meter_usage_data.__name__,
        return_value=dict_dummy_meter_usage_date,
    )

    dict_dummy_ncu_meter_usage_data = NcuMeterUsageDataFactory().gene_dummy_dict(
        meter_num=10, reading_num=90, start_dt=dummy_start_dt
    )
    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_ncu_meter_usage_data.__name__,
        return_value=dict_dummy_ncu_meter_usage_data,
    )

    mocker.patch.object(
        isg_dp_model.ncu_dpmodel.command,
        isg_dp_model.ncu_dpmodel.command.get_train_end_dt.__name__,
        return_value=train_end_dt,
    )

    isg_dp_model.train(cleansed_input_repo=cleansed_input_repo)
    assert isinstance(isg_dp_model, ISG_DPModel)
