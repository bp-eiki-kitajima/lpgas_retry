from pytest_mock import MockerFixture

from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC
from lpgas.domain.meter.meter import Meter
from lpgas.domain.weather.weather_data import WeatherData
from lpgas.domain.weather.weather_data_service import WeatherDataService
from lpgas.utils.boudary.if_cleansed_input_repo import IF_CleansedInputRepo


def test_get_weather_data_from_lc_id_ok(
    mocker: MockerFixture,
    cleansed_input_repo: IF_CleansedInputRepo,
    fixture_get_weather_data_from_lc_id_ok: tuple[
        LC_ID,
        dict[LC_ID, LC],
        dict[WeatherCityID, WeatherData],
        WeatherData,
    ],
):
    """正常な入力とlc_id指定からWeatherDataオブジェクトを生成できるか"""

    (
        lc_id,
        dict_lc,
        dict_weather_data,
        weather_data_expected,
    ) = fixture_get_weather_data_from_lc_id_ok

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_lc.__name__,
        return_value=dict_lc,
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_weather_data.__name__,
        return_value=dict_weather_data,
    )

    weather_data_result = WeatherDataService().get_weather_data_from_lc_id(
        cleansed_input_repo=cleansed_input_repo, lc_id=lc_id
    )

    assert weather_data_result == weather_data_expected


def test_get_weather_data_from_meter_id_ok(
    mocker: MockerFixture,
    cleansed_input_repo: IF_CleansedInputRepo,
    fixture_get_weather_data_from_meter_id_ok: tuple[
        MeterID,
        dict[MeterID, Meter],
        dict[LC_ID, LC],
        dict[WeatherCityID, WeatherData],
        WeatherData,
    ],
):
    """正常な入力とmeter_id指定からWeatherDataオブジェクトを生成できるか"""

    (
        meter_id,
        dict_meter,
        dict_lc,
        dict_weather_data,
        weather_data_expected,
    ) = fixture_get_weather_data_from_meter_id_ok

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_meter.__name__,
        return_value=dict_meter,
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_lc.__name__,
        return_value=dict_lc,
    )

    mocker.patch.object(
        cleansed_input_repo,
        cleansed_input_repo.get_dict_weather_data.__name__,
        return_value=dict_weather_data,
    )

    weather_data_result = WeatherDataService().get_weather_data_from_meter_id(
        cleansed_input_repo=cleansed_input_repo, meter_id=meter_id
    )

    assert weather_data_result == weather_data_expected
