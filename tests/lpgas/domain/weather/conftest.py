import pytest

from lpgas.utils.domain_registry import DomainRegistry

from .fixtures import (
    fixture_gene_from_args,
    fixture_get_weather_data_from_lc_id,
    fixture_get_weather_data_from_meter_id,
)


@pytest.fixture
def cleansed_input_repo(domain_registry: DomainRegistry):
    cleansed_input_repo = domain_registry.cleansed_input_repo()
    cleansed_input_repo.init_output()
    yield cleansed_input_repo
    cleansed_input_repo.init_output()


fixture_get_weather_data_from_lc_id_ok = fixture_get_weather_data_from_lc_id.fixture_ok
fixture_get_weather_data_from_meter_id_ok = fixture_get_weather_data_from_meter_id.fixture_ok
fixture_gene_from_args_ok = fixture_gene_from_args.fixture_ok
fixture_gene_from_args_ng = fixture_gene_from_args.fixture_ng
