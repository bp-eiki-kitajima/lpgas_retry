from typing import Any

import pandas as pd
import pytest
from _pytest.fixtures import SubRequest
from pydantic import ValidationError

from lpgas.domain.identifier import WeatherCityID
from lpgas.domain.weather.weather_data import WeatherData
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        "w_1",
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
        WeatherData(
            weather_city_id=WeatherCityID(id="w_1"),
            df_a_weather_data=pd.DataFrame(
                {
                    ColumnEnum.datetime.name: [
                        pd.Timestamp(year=2019, month=1, day=1),
                        pd.Timestamp(year=2019, month=1, day=2),
                    ],
                    ColumnEnum.ave_temp.name: [10.1, 10.2],
                    ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
                }
            ),
        ),
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[str, pd.DataFrame, WeatherData]:

    str_weather_city_id = request.param[0]
    df_a_weather_data = pd.DataFrame(data=request.param[1])
    weather_data_expected = request.param[2]

    return str_weather_city_id, df_a_weather_data, weather_data_expected


params_ng = {
    "date missing": (
        "w_1",
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=3),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
        ValidationError,
    ),
}


@pytest.fixture(
    params=params_ng.values(),
    ids=params_ng.keys(),
)
def fixture_ng(
    request: SubRequest,
) -> tuple[str, pd.DataFrame, Any]:

    str_weather_city_id = request.param[0]
    df_a_weather_data = pd.DataFrame(data=request.param[1])
    error = request.param[2]

    return str_weather_city_id, df_a_weather_data, error
