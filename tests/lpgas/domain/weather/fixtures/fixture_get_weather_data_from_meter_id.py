import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.identifier import LC_ID, MeterID, WeatherCityID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        MeterID(id="m_1"),
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_2"],
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.is_ncu.name: [True, True],
            ColumnEnum.template_number.name: ["101", "102"],
            ColumnEnum.enable_prediction.name: [True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_1 m_2"],
            ColumnEnum.lat.name: [35.1],
            ColumnEnum.lon.name: [140.1],
            ColumnEnum.addr.name: ["addr_1"],
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [50],
            ColumnEnum.sub_cylinder_num.name: [1],
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
        "w_1",
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[
    MeterID,
    dict[MeterID, Meter],
    dict[LC_ID, LC],
    dict[WeatherCityID, WeatherData],
    WeatherData,
]:

    meter_id = request.param[0]

    df_meter = pd.DataFrame(data=request.param[1])
    dict_meter = MeterFactory().gene_dict_from_df(df_meter)

    df_lc = pd.DataFrame(data=request.param[2])
    dict_lc = LCFactory().gene_dict_from_df(df_lc)

    df_weather_data = pd.DataFrame(data=request.param[3])
    dict_weather_data = WeatherDataFactory().gene_dict_from_df(df_weather_data)

    str_weather_city_id = request.param[4]
    df_a_weather_data = pd.DataFrame(data=request.param[5])
    weather_data_expected = WeatherDataFactory().gene_from_args(
        str_weather_city_id, df_a_weather_data
    )

    return meter_id, dict_meter, dict_lc, dict_weather_data, weather_data_expected
