import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.identifier import LC_ID, WeatherCityID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        LC_ID(id="lc_1"),
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_11 m_12 m_13"],
            ColumnEnum.lat.name: [35.1],
            ColumnEnum.lon.name: [140.1],
            ColumnEnum.addr.name: ["addr_1"],
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [50],
            ColumnEnum.sub_cylinder_num.name: [1],
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
        "w_1",
        {
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1),
                pd.Timestamp(year=2019, month=1, day=2),
            ],
            ColumnEnum.ave_temp.name: [10.1, 10.2],
            ColumnEnum.str_weather_city_id.name: ["w_1", "w_1"],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[LC_ID, dict[LC_ID, LC], dict[WeatherCityID, WeatherData], WeatherData]:

    lc_id = request.param[0]

    df_lc = pd.DataFrame(data=request.param[1])
    df_lc = Schema_For_Domain().validate_df_lc(df_lc)
    dict_lc = LCFactory().gene_dict_from_df(df_lc)

    df_weather_data = pd.DataFrame(data=request.param[2])
    dict_weather_data = WeatherDataFactory().gene_dict_from_df(df_weather_data)

    str_weather_city_id = request.param[3]
    df_a_weather_data = pd.DataFrame(data=request.param[4])
    weather_data_expected = WeatherDataFactory().gene_from_args(
        str_weather_city_id, df_a_weather_data
    )

    return lc_id, dict_lc, dict_weather_data, weather_data_expected
