from typing import Any

import pandas as pd
import pytest

from lpgas.domain.weather.weather_data import WeatherData, WeatherDataFactory


def test_gene_from_args_ok(fixture_gene_from_args_ok: tuple[str, pd.DataFrame, WeatherData]):
    """正常なdf_a_weather_dataからWeatherDataオブジェクトの生成ができるか"""
    str_weather_city_id, df_a_weather_data, weather_data_expected = fixture_gene_from_args_ok
    weather_data_result = WeatherDataFactory().gene_from_args(
        str_weather_city_id=str_weather_city_id, df_a_weather_data=df_a_weather_data
    )

    assert weather_data_result == weather_data_expected


def test_gene_from_args_ng(fixture_gene_from_args_ng: tuple[str, pd.DataFrame, Any]):
    """異常なdf_a_weather_dataから想定のエラーが発生されるか"""
    str_weather_city_id, df_a_weather_data, error = fixture_gene_from_args_ng

    with pytest.raises(error):
        WeatherDataFactory().gene_from_args(
            str_weather_city_id=str_weather_city_id, df_a_weather_data=df_a_weather_data
        )
