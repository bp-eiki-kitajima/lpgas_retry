import pytest

from lpgas.domain.preprocess.isg_preprocess_service import ISG_PreprocessService
from lpgas.domain.preprocess.preprocess_service_factory import PreprocessServiceFactory

params_gene_from_args_ok = {
    "isg": (ISG_PreprocessService.__name__, {"tmp_param": 0}, ISG_PreprocessService),
}


@pytest.mark.parametrize(
    "classname, kwargs, expected_type",
    list(params_gene_from_args_ok.values()),
    ids=list(params_gene_from_args_ok.keys()),
)
def test_gene_from_args_ok_case(classname: str, kwargs: dict, expected_type: type):
    preprocess_service = PreprocessServiceFactory().gene_from_args(
        preprocess_service_classname=classname, **kwargs
    )
    assert isinstance(preprocess_service, expected_type)
