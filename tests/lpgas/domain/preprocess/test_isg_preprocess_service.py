from lpgas.domain.preprocess.isg_preprocess_service import ISG_PreprocessService


def test_replace_main_cylinder_remain_amount_ok(
    fixture_replace_main_cylinder_remain_amount_ok,
):
    """正常に予備系0のLCの主容系残ガスを容量に置換できるか"""
    (
        dict_delivery_result_data,
        dict_lc,
        dict_delivery_result_data_expected,
    ) = fixture_replace_main_cylinder_remain_amount_ok

    dict_delivery_result_data_result = (
        ISG_PreprocessService()._replace_main_cylinder_remain_amount(
            dict_delivery_result_data=dict_delivery_result_data, dict_lc=dict_lc
        )
    )

    for lc_id, delivery_result_data_result in dict_delivery_result_data_result.items():
        delivery_result_data_expected = dict_delivery_result_data_expected[lc_id]
        assert delivery_result_data_result == delivery_result_data_expected


def test_round_delivery_result_ok(
    fixture_round_delivery_result_ok,
):
    """正常に配送時指針値の丸め込みができるか"""
    (
        dict_delivery_result_data,
        dict_delivery_result_data_expected,
    ) = fixture_round_delivery_result_ok

    dict_delivery_result_data_result = ISG_PreprocessService()._round_delivery_result(
        dict_delivery_result_data=dict_delivery_result_data
    )

    for lc_id, delivery_result_data_result in dict_delivery_result_data_result.items():
        delivery_result_data_expected = dict_delivery_result_data_expected[lc_id]
        assert delivery_result_data_result == delivery_result_data_expected


def test_round_meter_usage_data_ok(
    fixture_round_meter_usage_data_ok,
):
    """正常に検針時指針値の丸め込みができるか"""
    (
        dict_meter_usage_data,
        dict_meter_usage_data_expected,
    ) = fixture_round_meter_usage_data_ok

    dict_meter_usage_data_result = ISG_PreprocessService()._round_meter_usage_data(
        dict_meter_usage_data
    )

    for meter_id, meter_usage_data_result in dict_meter_usage_data_result.items():
        meter_usage_data_expected = dict_meter_usage_data_expected[meter_id]
        assert meter_usage_data_result == meter_usage_data_expected


def test_round_ncu_meter_usage_data_ok(
    fixture_round_ncu_meter_usage_data_ok,
):
    """正常にNCU検針時指針値の丸め込みができるか"""
    (
        dict_ncu_meter_usage_data,
        dict_ncu_meter_usage_data_expected,
    ) = fixture_round_ncu_meter_usage_data_ok

    dict_ncu_meter_usage_data_result = ISG_PreprocessService()._round_ncu_meter_usage_data(
        dict_ncu_meter_usage_data
    )

    for meter_id, ncu_meter_usage_data_result in dict_ncu_meter_usage_data_result.items():
        ncu_meter_usage_data_expected = dict_ncu_meter_usage_data_expected[meter_id]
        assert ncu_meter_usage_data_result == ncu_meter_usage_data_expected


def test_merge_from_delivery_result_data_ok(
    fixture_merge_from_delivery_result_data_ok,
):
    """正常に配送時指針値を検針実績にマージできるか"""
    (
        dict_meter_usage_data,
        dict_delivery_result_data,
        dict_lc,
        dict_meter,
        dict_meter_usage_data_expected,
    ) = fixture_merge_from_delivery_result_data_ok

    dict_meter_usage_data_result = ISG_PreprocessService()._merge_from_delivery_result_data(
        dict_meter_usage_data, dict_delivery_result_data, dict_lc, dict_meter
    )

    for meter_id, meter_usage_data_result in dict_meter_usage_data_result.items():
        meter_usage_data_expected = dict_meter_usage_data_expected[meter_id]
        assert meter_usage_data_result == meter_usage_data_expected


def test_interpolate_meter_usage_data_ok(fixture_interpolate_meter_usage_data_ok):
    """正常に検針実績の線形補完処理ができるか"""
    dict_meter_usage_data, dict_meter_usage_data_expected = fixture_interpolate_meter_usage_data_ok

    dict_meter_usage_data_result = ISG_PreprocessService()._interpolate_meter_usage_data(
        dict_meter_usage_data
    )

    for meter_id, meter_usage_data_result in dict_meter_usage_data_result.items():
        meter_usage_data_expected = dict_meter_usage_data_expected[meter_id]
        assert meter_usage_data_result == meter_usage_data_expected
