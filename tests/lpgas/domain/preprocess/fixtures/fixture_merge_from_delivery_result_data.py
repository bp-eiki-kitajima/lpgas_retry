import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID, MeterID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.meter.meter import Meter, MeterFactory
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "no apartment, no duplicate": (
        {
            ColumnEnum.str_meter_id.name: [
                "m_1",
                "m_2",
            ],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [
                100.0,
                100.0,
            ],
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, False],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=2, hour=9),  # 重複なし
            ],
            ColumnEnum.accum_usage.name: [101.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0],
            ColumnEnum.delivery_input_amount.name: [50.0],
            ColumnEnum.is_read.name: [True],
            ColumnEnum.is_change.name: [True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_1"],  # 個宅
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1"],
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.is_ncu.name: [True],
            ColumnEnum.template_number.name: ["100"],
            ColumnEnum.enable_prediction.name: [True],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0, 100.0],
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, True, False],
        },
    ),
    "no apartment, same duplicate": (
        {
            ColumnEnum.str_meter_id.name: [
                "m_1",
                "m_2",
            ],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [
                100.0,
                100.0,
            ],
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, False],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),  # 重複
            ],
            ColumnEnum.accum_usage.name: [100.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0],
            ColumnEnum.delivery_input_amount.name: [50.0],
            ColumnEnum.is_read.name: [True],
            ColumnEnum.is_change.name: [True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_1"],  # 個宅
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1"],
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.is_ncu.name: [True],
            ColumnEnum.template_number.name: ["100"],
            ColumnEnum.enable_prediction.name: [True],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [100.0, 100.0, 100.0],
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, True, False],
        },
    ),
    "apartment, no duplicate": (
        {
            ColumnEnum.str_meter_id.name: [
                "m_1",
                "m_2",
            ],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [
                100.0,
                100.0,
            ],
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, False],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=2, hour=9),  # 重複なし
            ],
            ColumnEnum.accum_usage.name: [101.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0],
            ColumnEnum.delivery_input_amount.name: [50.0],
            ColumnEnum.is_read.name: [True],
            ColumnEnum.is_change.name: [True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_1 m_2"],  # 集合住宅
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_2"],
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.is_ncu.name: [True, True],
            ColumnEnum.template_number.name: ["100", "100"],
            ColumnEnum.enable_prediction.name: [True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_2", "m_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
            ],
            ColumnEnum.accum_usage.name: [100.0, np.nan, 100.0, np.nan],
            ColumnEnum.is_open.name: [True, True, True, True],
            ColumnEnum.is_read.name: [True, True, True, True],
            ColumnEnum.is_change.name: [False, True, False, True],
        },
    ),
    "apartment, same duplicate": (
        {
            ColumnEnum.str_meter_id.name: [
                "m_1",
                "m_2",
            ],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [
                100.0,
                100.0,
            ],
            ColumnEnum.is_open.name: [True, False],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),  # 重複
            ],
            ColumnEnum.accum_usage.name: [100.0],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0],
            ColumnEnum.delivery_input_amount.name: [50.0],
            ColumnEnum.is_read.name: [True],
            ColumnEnum.is_change.name: [True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],
            ColumnEnum.list_str_meter_id.name: ["m_1 m_2"],  # 集合住宅
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_2"],
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.is_ncu.name: [True, True],
            ColumnEnum.template_number.name: ["100", "100"],
            ColumnEnum.enable_prediction.name: [True, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_2", "m_2"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
            ],
            ColumnEnum.accum_usage.name: [100.0, np.nan, 100.0, np.nan],
            ColumnEnum.is_open.name: [True, True, False, False],
            ColumnEnum.is_read.name: [True, True, True, True],
            ColumnEnum.is_change.name: [True, True, True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[
    dict[MeterID, MeterUsageData],
    dict[LC_ID, DeliveryResultData],
    dict[LC_ID, LC],
    dict[MeterID, Meter],
    dict[MeterID, MeterUsageData],
]:

    df_meter_usage_data = pd.DataFrame(data=request.param[0])
    df_meter_usage_data = Schema_For_Domain().validate_df_meter_usage_data(df_meter_usage_data)
    dict_meter_usage_data = MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_data)

    df_delivery_result_data = pd.DataFrame(data=request.param[1])
    df_delivery_result_data = Schema_For_Domain().validate_df_delivery_result_data(
        df_delivery_result_data
    )
    dict_delivery_result_data = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data
    )

    df_lc = pd.DataFrame(data=request.param[2])
    df_lc[ColumnEnum.main_cylinder_size.name] = 50
    df_lc[ColumnEnum.main_cylinder_num.name] = 1
    df_lc[ColumnEnum.sub_cylinder_size.name] = 50
    df_lc[ColumnEnum.sub_cylinder_num.name] = 1
    df_lc[ColumnEnum.str_staff_id.name] = "1"
    df_lc[ColumnEnum.str_weather_city_id.name] = "w_1"
    df_lc[ColumnEnum.lat.name] = 35.1
    df_lc[ColumnEnum.lon.name] = 140.1
    df_lc[ColumnEnum.addr.name] = "addr_1"
    df_lc = Schema_For_Domain().validate_df_lc(df_lc)
    dict_lc = LCFactory().gene_dict_from_df(df_lc)

    df_meter = pd.DataFrame(data=request.param[3])
    df_meter = Schema_For_Domain().validate_df_meter(df_meter)
    dict_meter = MeterFactory().gene_dict_from_df(df_meter)

    df_meter_usage_data_expected = pd.DataFrame(data=request.param[4])
    df_meter_usage_data_expected = Schema_For_Domain().validate_df_meter_usage_data(
        df_meter_usage_data_expected
    )
    dict_meter_usage_data_expected = MeterUsageDataFactory().gene_dict_from_df(
        df_meter_usage_data_expected
    )

    return (
        dict_meter_usage_data,
        dict_delivery_result_data,
        dict_lc,
        dict_meter,
        dict_meter_usage_data_expected,
    )
