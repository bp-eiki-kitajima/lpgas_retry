import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID
from lpgas.domain.lc import LC, LCFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],  # 配送時に交換
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],  # LC集約に存在
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [0],  # 予備系0
            ColumnEnum.sub_cylinder_num.name: [0],  # 予備系0
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 50.0],  # 置換
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
    ),
    "2, no change": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],
            ColumnEnum.delivery_input_amount.name: [0.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, True],  # 配送時に交換なし
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],  # LC集約に存在
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [0],
            ColumnEnum.sub_cylinder_num.name: [0],
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 50.0],  # 置換なし
            ColumnEnum.delivery_input_amount.name: [0.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, True],
        },
    ),
    "3, sub is not 0": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1"],  # LC集約に存在
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [50],  # 予備系 not 0
            ColumnEnum.sub_cylinder_num.name: [1],  # 予備系 not 0
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],  # 置換なし
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
    ),
    "4, lc not in lc agg": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_3"],  # LC集約に存在なし
            ColumnEnum.main_cylinder_size.name: [50],
            ColumnEnum.main_cylinder_num.name: [1],
            ColumnEnum.sub_cylinder_size.name: [0],
            ColumnEnum.sub_cylinder_num.name: [0],
            ColumnEnum.str_staff_id.name: ["1"],
            ColumnEnum.str_weather_city_id.name: ["w_1"],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.main_cylinder_remain_amount.name: [35.0, 35.0],  # 置換なし
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[dict[LC_ID, DeliveryResultData], dict[LC_ID, LC], dict[LC_ID, DeliveryResultData]]:

    df_delivery_result_data = pd.DataFrame(data=request.param[0])
    df_delivery_result_data[ColumnEnum.datetime.name] = [
        pd.Timestamp(year=2019, month=1, day=1, hour=9),
        pd.Timestamp(year=2019, month=1, day=2, hour=10),
    ]
    df_delivery_result_data[ColumnEnum.accum_usage.name] = [100.0, 101.0]
    df_delivery_result_data = Schema_For_Domain().validate_df_delivery_result_data(
        df_delivery_result_data
    )
    dict_delivery_result_data = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data
    )

    df_lc = pd.DataFrame(data=request.param[1])
    df_lc[ColumnEnum.list_str_meter_id.name] = ["m_11 m_12 m_13"]
    df_lc[ColumnEnum.lat.name] = [35.1]
    df_lc[ColumnEnum.lon.name] = [140.1]
    df_lc[ColumnEnum.addr.name] = ["addr_1"]
    df_lc = Schema_For_Domain().validate_df_lc(df_lc)
    dict_lc = LCFactory().gene_dict_from_df(df_lc)

    df_delivery_result_data_expected = pd.DataFrame(data=request.param[2])
    df_delivery_result_data_expected[ColumnEnum.datetime.name] = [
        pd.Timestamp(year=2019, month=1, day=1, hour=9),
        pd.Timestamp(year=2019, month=1, day=2, hour=10),
    ]
    df_delivery_result_data_expected[ColumnEnum.accum_usage.name] = [100.0, 101.0]
    df_delivery_result_data_expected = Schema_For_Domain().validate_df_delivery_result_data(
        df_delivery_result_data_expected
    )
    dict_delivery_result_data_expected = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data_expected
    )

    return (dict_delivery_result_data, dict_lc, dict_delivery_result_data_expected)
