import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.delivery_result_data import DeliveryResultData, DeliveryResultDataFactory
from lpgas.domain.identifier import LC_ID
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.01234, 101.01234],  # 小数点2位以下
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 50.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
        {
            ColumnEnum.str_lc_id.name: ["lc_1", "lc_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0],  # 丸め込み
            ColumnEnum.main_cylinder_remain_amount.name: [50.0, 50.0],
            ColumnEnum.delivery_input_amount.name: [50.0, 50.0],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [True, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[dict[LC_ID, DeliveryResultData], dict[LC_ID, DeliveryResultData]]:

    df_delivery_result_data = pd.DataFrame(data=request.param[0])
    df_delivery_result_data = Schema_For_Domain().validate_df_delivery_result_data(
        df_delivery_result_data
    )
    dict_delivery_result_data = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data
    )

    df_delivery_result_data_expected = pd.DataFrame(data=request.param[1])
    df_delivery_result_data_expected = Schema_For_Domain().validate_df_delivery_result_data(
        df_delivery_result_data_expected
    )
    dict_delivery_result_data_expected = DeliveryResultDataFactory().gene_dict_from_df(
        df_delivery_result_data_expected
    )

    return (dict_delivery_result_data, dict_delivery_result_data_expected)
