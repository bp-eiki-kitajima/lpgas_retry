import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.identifier import MeterID
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "normal": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.01234, 101.01234],  # 小数点1位以下
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, True],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=10),
            ],
            ColumnEnum.accum_usage.name: [100.0, 101.0],  # 小数点1位
            ColumnEnum.is_open.name: [True, True],
            ColumnEnum.is_read.name: [True, True],
            ColumnEnum.is_change.name: [False, True],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[dict[MeterID, MeterUsageData], dict[MeterID, MeterUsageData]]:

    df_meter_usage_data = pd.DataFrame(data=request.param[0])
    df_meter_usage_data = Schema_For_Domain().validate_df_meter_usage_data(df_meter_usage_data)
    dict_meter_usage_data = MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_data)

    df_meter_usage_data_expected = pd.DataFrame(data=request.param[1])
    df_meter_usage_data_expected = Schema_For_Domain().validate_df_meter_usage_data(
        df_meter_usage_data_expected
    )
    dict_meter_usage_data_expected = MeterUsageDataFactory().gene_dict_from_df(
        df_meter_usage_data_expected
    )

    return (dict_meter_usage_data, dict_meter_usage_data_expected)
