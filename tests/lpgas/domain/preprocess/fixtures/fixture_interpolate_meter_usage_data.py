import numpy as np
import pandas as pd
import pytest
from _pytest.fixtures import SubRequest

from lpgas.domain.identifier import MeterID
from lpgas.domain.meter_usage_data.meter_usage_data import MeterUsageData
from lpgas.domain.meter_usage_data.meter_usage_data_factory import MeterUsageDataFactory
from lpgas.domain.schema import Schema_For_Domain
from lpgas.utils.column_enum import ColumnEnum

params_ok = {
    "1, pre_missing": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [np.nan, 2.0, 3.0],  # 前欠損
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [2.0, 2.0, 3.0],  # 前端で置換
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
    ),
    "2, pro_missing": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [1.0, 2.0, np.nan],  # 後欠損
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=3, hour=9),
            ],
            ColumnEnum.accum_usage.name: [1.0, 2.0, 2.0],  # 後端で置換
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
    ),
    "3, middle_missing": (
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=4, hour=9),
            ],
            ColumnEnum.accum_usage.name: [1.0, np.nan, 4.0],  # 中欠損
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
        {
            ColumnEnum.str_meter_id.name: ["m_1", "m_1", "m_1"],
            ColumnEnum.datetime.name: [
                pd.Timestamp(year=2019, month=1, day=1, hour=9),
                pd.Timestamp(year=2019, month=1, day=2, hour=9),
                pd.Timestamp(year=2019, month=1, day=4, hour=9),
            ],
            ColumnEnum.accum_usage.name: [1.0, 2.0, 4.0],  # 時系列置換
            ColumnEnum.is_open.name: [True, True, True],
            ColumnEnum.is_read.name: [True, True, True],
            ColumnEnum.is_change.name: [False, False, False],
        },
    ),
}


@pytest.fixture(
    params=params_ok.values(),
    ids=params_ok.keys(),
)
def fixture_ok(
    request: SubRequest,
) -> tuple[dict[MeterID, MeterUsageData], dict[MeterID, MeterUsageData]]:

    df_meter_usage_data = pd.DataFrame(data=request.param[0])
    df_meter_usage_data = Schema_For_Domain().validate_df_meter_usage_data(df_meter_usage_data)
    dict_meter_usage_data = MeterUsageDataFactory().gene_dict_from_df(df_meter_usage_data)

    df_meter_usage_data_expected = pd.DataFrame(data=request.param[1])
    df_meter_usage_data_expected = Schema_For_Domain().validate_df_meter_usage_data(
        df_meter_usage_data_expected
    )
    dict_meter_usage_data_expected = MeterUsageDataFactory().gene_dict_from_df(
        df_meter_usage_data_expected
    )

    return dict_meter_usage_data, dict_meter_usage_data_expected
