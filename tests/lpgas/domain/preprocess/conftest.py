from .fixtures import (
    fixture_interpolate_meter_usage_data,
    fixture_merge_from_delivery_result_data,
    fixture_replace_main_cylinder_remain_amount,
    fixture_round_delivery_result,
    fixture_round_meter_usage_data,
    fixture_round_ncu_meter_usage_data,
)

fixture_replace_main_cylinder_remain_amount_ok = (
    fixture_replace_main_cylinder_remain_amount.fixture_ok
)
fixture_round_delivery_result_ok = fixture_round_delivery_result.fixture_ok
fixture_round_meter_usage_data_ok = fixture_round_meter_usage_data.fixture_ok
fixture_round_ncu_meter_usage_data_ok = fixture_round_ncu_meter_usage_data.fixture_ok
fixture_merge_from_delivery_result_data_ok = fixture_merge_from_delivery_result_data.fixture_ok
fixture_interpolate_meter_usage_data_ok = fixture_interpolate_meter_usage_data.fixture_ok
