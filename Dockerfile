FROM python:3.9-buster

ENV SHELL /bin/bash
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

# Environment Setting
RUN apt update

# install poetry
COPY pyproject.toml ./
RUN pip install poetry
RUN poetry config virtualenvs.create false && poetry install
RUN rm pyproject.toml

# install packages
RUN apt install -y vim zip unzip tree graphviz

# Clean up
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
