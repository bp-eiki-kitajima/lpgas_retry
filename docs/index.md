# LPガス配送最適化

ソフトバンクLPガス案件で作成したモデルコードの技術負債を反省するため清書する.

---

## RawInputUsecase
![RawInputUsecase](uml/raw_input_usecase.svg)
## TrainUsecase
![TrainUsecase](uml/train_usecase.svg)
## PredictUsecase
![PredictUsecase](uml/predict_usecase.svg)


