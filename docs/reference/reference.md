# リファレンスのルール

    ::: my_package.my_module.MyClass
        handler: python
        selection:
        members:
            - method_a
            - method_b
        rendering:
        show_root_heading: false
        show_source: false

# 参考ページ
[mkdocstringのドキュメンテーション](https://mkdocstrings.github.io/)